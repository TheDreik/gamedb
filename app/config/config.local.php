<?php
/*-------------------------------------------------------
 * @Project: Alto CMS
 * @Project URI: http://altocms.com
 * @Description: Advanced Community Engine
 * @Copyright: Alto CMS Team
 * @License: GNU GPL v2 & MIT
 *-------------------------------------------------------
 */

define('DEBUG', 0);

/**
 * Config for your site
 * You need to rename this file to "config.local.php" and fill your settings
 */

/**
 * Settings for connection to database
 */
$config['db']['params']['host'] = 'localhost';
$config['db']['params']['port'] = '3306';
$config['db']['params']['user'] = 'root';
$config['db']['params']['pass'] = 'kolenko';
$config['db']['params']['type']   = 'mysqli';
$config['db']['params']['dbname'] = 'alto';
$config['db']['table']['prefix'] = 'alto_';
$config['db']['tables']['engine'] = 'InnoDB';

/**
 * "Salt" to enhance the security of hashable data
 */
$config['security']['salt_sess'] = 'X8rEtQX3LqjbBW2mJLb9RJiWdCIHZSE8a7MUYvYvmHWzoYm9vNIMi2uLECepguxG';
$config['security']['salt_pass'] = '2KmaFKV3c22RboaWCutvwoHA2w0iry0tImTZ7zcj2eAdTm_gg4cdsupvrpdindb7';
$config['security']['salt_auth'] = 'P6gW6IG8ySmRFx9x2laugPpYFTG47tAXzr46ALD_o1c4yl20XcvE2lnIE5MMyXuy';

$config['path']['root']['url'] = 'http://188.226.205.51:8080/';

$config['path']['root']['dir'] = '/var/www/kolenka/gamedb/';

$config['path']['offset_request_url'] = '0';

return $config;

// EOF
