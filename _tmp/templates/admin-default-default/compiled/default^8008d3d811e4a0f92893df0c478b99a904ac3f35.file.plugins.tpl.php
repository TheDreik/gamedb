<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:10
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/admin-default/tpls/actions/admin/action.admin.site/plugins.tpl" */ ?>
<?php /*%%SmartyHeaderCode:55906964755940ec611ef68-48440857%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8008d3d811e4a0f92893df0c478b99a904ac3f35' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/admin-default/tpls/actions/admin/action.admin.site/plugins.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
    '2af50dd0cb04e27a1dcf2e56eb03bde0fe28e4b0' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/admin-default/tpls/_index.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
    '8927a76be03e77c5eb2cbe3b43ac1bd329c5cc09' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/admin-default/themes/default/default.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '55906964755940ec611ef68-48440857',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'sHtmlTitle' => 0,
    'sHtmlDescription' => 0,
    'sHtmlKeywords' => 0,
    'aHtmlHeadFiles' => 0,
    'aHtmlRssAlternate' => 0,
    'sHtmlCanonical' => 0,
    'bRefreshToHome' => 0,
    'ALTO_SECURITY_KEY' => 0,
    '_sPhpSessionId' => 0,
    'aRouter' => 0,
    'sPage' => 0,
    'sPath' => 0,
    'aLangJs' => 0,
    'body_classes' => 0,
    'oUserCurrent' => 0,
    'aLang' => 0,
    'iUserCurrentCountTalkNew' => 0,
    'iUserCurrentCountTrack' => 0,
    'sAction' => 0,
    'sEvent' => 0,
    'sPageTitle' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ec65d5564_21922582',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ec65d5564_21922582')) {function content_55940ec65d5564_21922582($_smarty_tpl) {?><?php if (!is_callable('smarty_function_hook')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.hook.php';
if (!is_callable('smarty_function_router')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.router.php';
if (!is_callable('smarty_function_cfg')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.cfg.php';
if (!is_callable('smarty_function_asset')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.asset.php';
if (!is_callable('smarty_function_json')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.json.php';
?><!DOCTYPE html>

<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="ru"> <!--<![endif]-->

<head>

<?php echo smarty_function_hook(array('run'=>'layout_head_begin'),$_smarty_tpl);?>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title><?php echo $_smarty_tpl->tpl_vars['sHtmlTitle']->value;?>
</title>

    <meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['sHtmlDescription']->value;?>
">
    <meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['sHtmlKeywords']->value;?>
">

    <meta name="viewport" content="width=600"/>

    <?php echo $_smarty_tpl->tpl_vars['aHtmlHeadFiles']->value['css'];?>


    <link href="<?php echo Config::Get('path.static.skin');?>
assets/img/favicon.ico?v1" rel="shortcut icon">
    <link rel="search" type="application/opensearchdescription+xml" href="<?php echo smarty_function_router(array('page'=>'search'),$_smarty_tpl);?>
opensearch/"
          title="<?php echo Config::Get('view.name');?>
"/>

<?php if ($_smarty_tpl->tpl_vars['aHtmlRssAlternate']->value) {?>
    <link rel="alternate" type="application/rss+xml" href="<?php echo $_smarty_tpl->tpl_vars['aHtmlRssAlternate']->value['url'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['aHtmlRssAlternate']->value['title'];?>
">
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['sHtmlCanonical']->value) {?>
    <link rel="canonical" href="<?php echo $_smarty_tpl->tpl_vars['sHtmlCanonical']->value;?>
"/>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['bRefreshToHome']->value) {?>
    <meta HTTP-EQUIV="Refresh" CONTENT="3; URL=<?php echo Config::Get('path.root.url');?>
">
<?php }?>


    <script type="text/javascript">
        var DIR_WEB_ROOT = '<?php echo Config::Get("path.root.web");?>
';
        var DIR_STATIC_SKIN = '<?php echo Config::Get("path.static.skin");?>
';
        var DIR_ROOT_ENGINE_LIB = '<?php echo Config::Get("path.root.engine_lib");?>
';
        var ALTO_SECURITY_KEY = '<?php echo $_smarty_tpl->tpl_vars['ALTO_SECURITY_KEY']->value;?>
';
        var SESSION_ID = '<?php echo $_smarty_tpl->tpl_vars['_sPhpSessionId']->value;?>
';
        var WYSIWYG = <?php if (Config::Get("view.wysiwyg")) {?>true<?php } else { ?>false<?php }?>;

        var l10n = {
            'date_format': '<?php echo Config::Get("l10n.date_format");?>
',
            'week_start': <?php echo smarty_function_cfg(array('name'=>"l10n.week_start",'default'=>0),$_smarty_tpl);?>

        };

        var tinymce = false;
        var TINYMCE_LANG = <?php if (Config::Get('lang.current')=='ru') {?>'ru'<?php } else { ?>'en'<?php }?>;

        var aRouter = new Array();
        <?php  $_smarty_tpl->tpl_vars['sPath'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sPath']->_loop = false;
 $_smarty_tpl->tpl_vars['sPage'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['aRouter']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sPath']->key => $_smarty_tpl->tpl_vars['sPath']->value) {
$_smarty_tpl->tpl_vars['sPath']->_loop = true;
 $_smarty_tpl->tpl_vars['sPage']->value = $_smarty_tpl->tpl_vars['sPath']->key;
?>
        aRouter['<?php echo $_smarty_tpl->tpl_vars['sPage']->value;?>
'] = '<?php echo $_smarty_tpl->tpl_vars['sPath']->value;?>
';
        <?php } ?>
    </script>

	<style>
	@font-face {
		font-family:'Icons Halflings';
		src:url('<?php echo smarty_function_asset(array('file'=>"assets/css/fonts/icons-halflings-regular.eot"),$_smarty_tpl);?>
');
		src:url('<?php echo smarty_function_asset(array('file'=>"assets/css/fonts/icons-halflings-regular.eot?#iefix"),$_smarty_tpl);?>
') format('embedded-opentype'),
		url('<?php echo smarty_function_asset(array('file'=>"assets/css/fonts/icons-halflings-regular.woff"),$_smarty_tpl);?>
') format('woff'),
		url('<?php echo smarty_function_asset(array('file'=>"assets/css/fonts/icons-halflings-regular.ttf"),$_smarty_tpl);?>
') format('truetype'),
		url('<?php echo smarty_function_asset(array('file'=>"assets/css/fonts/icons-halflings-regular.svg#icons-halflingsregular"),$_smarty_tpl);?>
') format('svg');
	}
	@font-face {
	font-family: 'Simple-Line-Icons';
	src:url('<?php echo smarty_function_asset(array('file'=>"assets/css/simpleline/fonts/Simple-Line-Icons.eot"),$_smarty_tpl);?>
');
	src:url('<?php echo smarty_function_asset(array('file'=>"assets/css/simpleline/fonts/Simple-Line-Icons.eot?#iefix"),$_smarty_tpl);?>
') format('embedded-opentype'),
		url('<?php echo smarty_function_asset(array('file'=>"assets/css/simpleline/fonts/Simple-Line-Icons.woff"),$_smarty_tpl);?>
') format('woff'),
		url('<?php echo smarty_function_asset(array('file'=>"assets/css/simpleline/fonts/Simple-Line-Icons.ttf"),$_smarty_tpl);?>
') format('truetype'),
		url('<?php echo smarty_function_asset(array('file'=>"assets/css/simpleline/fonts/Simple-Line-Icons.svg#Simple-Line-Icons"),$_smarty_tpl);?>
') format('svg');
	font-weight: normal;
	font-style: normal;
}
	</style>

	<link href='http://fonts.googleapis.com/css?family=Cuprum:400,400italic,700,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,700,400italic,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

<?php echo $_smarty_tpl->tpl_vars['aHtmlHeadFiles']->value['js'];?>

    <script>
        (function ($) {
            $(function () {
                $('input, select').styler();
            });
        })(jQuery);
    </script>
    <script type="text/javascript">
        ls.lang.load(<?php echo smarty_function_json(array('var'=>$_smarty_tpl->tpl_vars['aLangJs']->value),$_smarty_tpl);?>
);
        //ls.registry.set('comment_max_tree', '<?php echo Config::Get("module.comment.max_tree");?>
');
    </script>

<?php echo smarty_function_hook(array('run'=>'layout_head_end'),$_smarty_tpl);?>


</head>

<body class="<?php echo $_smarty_tpl->tpl_vars['body_classes']->value;?>
">

<?php echo smarty_function_hook(array('run'=>'layout_body_begin'),$_smarty_tpl);?>


    <?php echo $_smarty_tpl->getSubTemplate ("modals/modal.empty.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<!-- NAVBAR -->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>

    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">

    <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img src="<?php echo $_smarty_tpl->tpl_vars['oUserCurrent']->value->getAvatarUrl(24);?>
" alt="avatar" class="avatar"/>
                <?php echo $_smarty_tpl->tpl_vars['oUserCurrent']->value->getDisplayName();?>

            </a>

            <ul class="dropdown-menu">
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['oUserCurrent']->value->getUserUrl();?>
"><i class="icon icon-user"></i> <?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_menu_profile'];?>

                    </a></li>
                <li><a href="/settings/profile/"><i class="icon icon-settings"></i> <?php echo $_smarty_tpl->tpl_vars['aLang']->value['settings_menu'];?>
</a></li>
                <li><a href="<?php echo smarty_function_router(array('page'=>'login'),$_smarty_tpl);?>
exit/?security_key=<?php echo $_smarty_tpl->tpl_vars['ALTO_SECURITY_KEY']->value;?>
"><i class="icon icon-lock"></i> <?php echo $_smarty_tpl->tpl_vars['aLang']->value['exit'];?>
</a></li>
            </ul>
        </li>
    </ul>

    <ul class="nav navbar-nav navbar-right">
        <!-- li>
          <a href="#" data-toggle="dropdown">
            <i class="icon icon-send"></i> Онлайн <span class="badge badge-success">178</span>
          </a>
        </li -->

        <li>
            <a href="<?php echo smarty_function_router(array('page'=>'talk'),$_smarty_tpl);?>
">
                <i class="icon icon-envelope"></i>
                <?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_privat_messages'];?>

                <?php if ($_smarty_tpl->tpl_vars['iUserCurrentCountTalkNew']->value) {?>
                    <span class="badge badge-important"><?php echo $_smarty_tpl->tpl_vars['iUserCurrentCountTalkNew']->value;?>
</span>
                <?php }?>
            </a>
        </li>

        <li>
            <a href="<?php echo smarty_function_router(array('page'=>'feed'),$_smarty_tpl);?>
track/">
                <i class="icon icon-bell"></i> <?php echo $_smarty_tpl->tpl_vars['aLang']->value['subscribe_menu'];?>
 <?php if ($_smarty_tpl->tpl_vars['iUserCurrentCountTrack']->value) {?><span
                        class="badge badge-important"><?php echo $_smarty_tpl->tpl_vars['iUserCurrentCountTrack']->value;?>
</span><?php }?>
            </a>
        </li>

    </ul>

    <ul class="nav navbar-nav navbar-right goto-site">
        <li>
            <a href="/" target="_blank"><i class="icon icon-pointer"></i> <?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['goto_site'];?>
</a></li>
        <li>
    </ul>
</nav>
</div>

<div class="container">
<!-- SIDEBAR -->
<div id="sidebar" class="b-sidebar">

    <a class="logo" href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
"><span></span></a>

    <ul class="menu-left">
        <?php echo smarty_function_hook(array('run'=>'admin_menu_top'),$_smarty_tpl);?>

        <li class="menu-header">

            <a data-toggle="collapse" href="#MenuInfo">
                <i class="icon icon-info"></i><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_info'];?>

            </a>

            <ul id="MenuInfo"
                style="<?php if ($_smarty_tpl->tpl_vars['sEvent']->value==''||$_smarty_tpl->tpl_vars['sEvent']->value=='info-dashboard'||$_smarty_tpl->tpl_vars['sEvent']->value=='info-report'||$_smarty_tpl->tpl_vars['sEvent']->value=='info-phpinfo') {?>height: auto;<?php } else { ?>height: 0;<?php }?>">
                <li class="menu-item_dashboard <?php if ($_smarty_tpl->tpl_vars['sEvent']->value==''||$_smarty_tpl->tpl_vars['sEvent']->value=='info-dashboard') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>'admin'),$_smarty_tpl);?>
info-dashboard/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_info_dashboard'];?>
</a>
                </li>
                <li class="menu-item_report <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='info-report') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>'admin'),$_smarty_tpl);?>
info-report/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_info_report'];?>
</a>
                </li>
                <li class="menu-item_phpinfo <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='info-phpinfo') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>'admin'),$_smarty_tpl);?>
info-phpinfo/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_info_phpinfo'];?>
</a>
                </li>
                <?php echo smarty_function_hook(array('run'=>'admin_menu_info'),$_smarty_tpl);?>

            </ul>

        </li>

        <li class="menu-header">

            <a data-toggle="collapse" href="#MenuContent">
                <i class="icon icon-docs"></i><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_content'];?>

            </a>

            <ul id="MenuContent" style="<?php if ($_smarty_tpl->tpl_vars['sMainMenuItem']->value=='content') {?>height: auto;<?php } else { ?>height: 0;<?php }?>">
                <li class="menu-item_pages <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='content-pages') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
content-pages/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_content_pages'];?>
</a>
                </li>
                <li class="menu-item_blogs <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='content-blogs') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
content-blogs/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_content_blogs'];?>
</a>
                </li>
                <li class="menu-item_topics <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='content-topics') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
content-topics/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_content_topics'];?>
</a>
                </li>
                <li class="menu-item_comments <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='content-comments') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
content-comments/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_content_comments'];?>
</a>
                </li>
                <li class="menu-item_mresources <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='content-mresources') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
content-mresources/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_content_mresources'];?>
</a>
                </li>
                <?php echo smarty_function_hook(array('run'=>'admin_menu_content'),$_smarty_tpl);?>

            </ul>

        </li>

        <li class="menu-header">

            <a data-toggle="collapse" href="#MenuUsers">
                <i class="icon icon-users"></i><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_users'];?>

            </a>

            <ul id="MenuUsers" style="<?php if ($_smarty_tpl->tpl_vars['sMainMenuItem']->value=='users') {?>height: auto;<?php } else { ?>height: 0;<?php }?>">
                <li class="menu-item_users <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='users-list') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
users-list/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_users_list'];?>
</a>
                </li>
                <li class="menu-item_banlist <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='users-banlist') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
users-banlist/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_users_banlist'];?>
</a>
                </li>
                <li class="menu-item_invites <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='users-invites') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
users-invites/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_users_invites'];?>
</a>
                </li>
                <?php echo smarty_function_hook(array('run'=>'admin_menu_users'),$_smarty_tpl);?>

            </ul>

        </li>

        <li class="menu-header">

            <a data-toggle="collapse" href="#MenuSettings">
                <i class="icon icon-settings"></i><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_settings'];?>

            </a>

            <ul id="MenuSettings" style="<?php if ($_smarty_tpl->tpl_vars['sMainMenuItem']->value=='settings') {?>height: auto;<?php } else { ?>height: 0;<?php }?>">
                <li class="menu-item_settings <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='settings-site') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
settings-site/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_settings_site'];?>
</a>
                </li>
                <li class="menu-item_lang <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='settings-lang') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
settings-lang/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_settings_lang'];?>
</a>
                </li>
                <li class="menu-item_blogtypes <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='settings-blogtypes') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
settings-blogtypes/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_settings_blogtypes'];?>
</a>
                </li>
                <li class="menu-item_content <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='settings-contenttypes') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
settings-contenttypes/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_settings_contenttypes'];?>
</a>
                </li>
                <li class="menu-item_userrights <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='settings-userrights') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
settings-userrights/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_settings_userrights'];?>
</a>
                </li>
                <li class="menu-item_userfields <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='settings-userfields') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
settings-userfields/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_settings_userfields'];?>
</a>
                </li>
                <?php echo smarty_function_hook(array('run'=>'admin_menu_settings'),$_smarty_tpl);?>

            </ul>

        </li>

        <li class="menu-header">

            <a data-toggle="collapse" href="#MenuSite">
                <i class="icon icon-screen-desktop"></i><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_site'];?>

            </a>

            <ul id="MenuSite" style="<?php if ($_smarty_tpl->tpl_vars['sMainMenuItem']->value=='site') {?>height: auto;<?php } else { ?>height: 0;<?php }?>">
                <li class="menu-item_skins <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='site-skins') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
site-skins/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_site_skins'];?>
</a>
                </li>
                <li class="menu-item_widgets <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='site-widgets') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
site-widgets/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_widgets'];?>
</a>
                </li>
                <li class="menu-item_plugins <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='site-plugins') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
site-plugins/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_plugins'];?>
</a>
                </li>
                <?php echo smarty_function_hook(array('run'=>'admin_menu_site'),$_smarty_tpl);?>

            </ul>

        </li>

        <li class="menu-header">

            <a data-toggle="collapse" href="#MenuLogs">
                <i class="icon icon-book-open"></i><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_logs'];?>

            </a>

            <ul id="MenuLogs" style="<?php if ($_smarty_tpl->tpl_vars['sMainMenuItem']->value=='logs') {?>height: auto;<?php } else { ?>height: 0;<?php }?>">
                <li class="menu-item_logs_errors <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='logs-error') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
logs-error/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_logs_error'];?>
</a>
                </li>
                <li class="menu-item_logs_sqlerrors <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='logs-sqlerror') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
logs-sqlerror/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_logs_sqlerror'];?>
</a>
                </li>
                <li class="menu-item_logs_sql <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='logs-sqllog') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
logs-sqllog/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_logs_sqllog'];?>
</a>
                </li>
                <?php echo smarty_function_hook(array('run'=>'admin_menu_logs'),$_smarty_tpl);?>

            </ul>

        </li>

        <li class="menu-header">

            <a data-toggle="collapse" href="#MenuTools">
                <i class="icon icon-wrench"></i><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_tools'];?>

            </a>

            <ul id="MenuTools" style="<?php if ($_smarty_tpl->tpl_vars['sMainMenuItem']->value=='tools') {?>height: auto;<?php } else { ?>height: 0;<?php }?>">
                <li class="menu-item_reset <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='tools-reset') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
tools-reset/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_tools_reset'];?>
</a>
                </li>
                <?php if (Config::Get('module.comment.use_nested')) {?>
                    <li class="menu-item_commentstree <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='tools-commentstree') {?>active<?php }?>">
                        <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
tools-commentstree/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_tools_commentstree'];?>
</a>
                    </li>
                <?php }?>
                <li class="menu-item_recalcfavourites <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='tools-recalcfavourites') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
tools-recalcfavourites/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_tools_recalcfavourites'];?>
</a>
                </li>
                <li class="menu-item_recalcvotes <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='tools-recalcvotes') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
tools-recalcvotes/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_tools_recalcvotes'];?>
</a>
                </li>
                <li class="menu-item_recalctopics <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='tools-recalctopics') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
tools-recalctopics/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_tools_recalctopics'];?>
</a>
                </li>
                <li class="menu-item_recalcblograting <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='tools-recalcblograting') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
tools-recalcblograting/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_tools_recalcblograting'];?>
</a>
                </li>
                <li class="menu-item_checkdb <?php if ($_smarty_tpl->tpl_vars['sEvent']->value=='tools-checkdb') {?>active<?php }?>">
                    <a href="<?php echo smarty_function_router(array('page'=>"admin"),$_smarty_tpl);?>
tools-checkdb/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_tools_checkdb'];?>
</a>
                </li>

                <?php echo smarty_function_hook(array('run'=>'admin_menu_tools'),$_smarty_tpl);?>

            </ul>

        </li>
        <?php echo smarty_function_hook(array('run'=>'admin_menu_end'),$_smarty_tpl);?>

    </ul>

<div class="site-search">
    <form method="get" action="<?php echo smarty_function_router(array('page'=>'search'),$_smarty_tpl);?>
topics/">
        <input type="text" name="q" class="search-input" placeholder="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['search_submit'];?>
..." style="">
        <button type="submit" style=""><i class="icon icon-magnifier"></i></button>
    </form>
</div>


    <div class="b-sidebar-top">
        <!--Action: [<?php echo $_smarty_tpl->tpl_vars['sAction']->value;?>
], Event: [<?php echo $_smarty_tpl->tpl_vars['sEvent']->value;?>
]-->
        <span id="window-width"></span>
    </div>
</div>

<!-- CONTENT -->
<div id="content" class="b-content">


    <div id="sticknote" class="b-sticknote">wait...</div>
    <div id="content-header" class="b-content-header">
        <h1 class="b-content-header-title"><?php echo $_smarty_tpl->tpl_vars['sPageTitle']->value;?>
</h1>
    </div>
    <div id="breadcrumb" class="b-content-breadcrumb">
        <a href="#" ><i class="icon icon-magic-wand"></i> <?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['title'];?>
</a>
        <a href="#" class="current"><?php echo $_smarty_tpl->tpl_vars['sPageTitle']->value;?>
</a>
    </div>

    
    <?php if (!$_smarty_tpl->tpl_vars['noShowSystemMessage']->value&&($_smarty_tpl->tpl_vars['aMsgError']->value||$_smarty_tpl->tpl_vars['aMsgNotice']->value)) {?>
    <div class="row-fluid">
        <div class="span12">
            <?php if ($_smarty_tpl->tpl_vars['aMsgError']->value) {?>
                <?php  $_smarty_tpl->tpl_vars['aMsg'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['aMsg']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aMsgError']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['aMsg']->key => $_smarty_tpl->tpl_vars['aMsg']->value) {
$_smarty_tpl->tpl_vars['aMsg']->_loop = true;
?>
                    <div class="b-sysmessage_alert alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <?php if ($_smarty_tpl->tpl_vars['aMsg']->value['title']!='') {?>
                            <h4 class="alert-heading"><?php echo $_smarty_tpl->tpl_vars['aMsg']->value['title'];?>
:</h4>
                        <?php }?>
                        <?php echo $_smarty_tpl->tpl_vars['aMsg']->value['msg'];?>

                    </div>
                <?php } ?>
            <?php }?>

            <?php if ($_smarty_tpl->tpl_vars['aMsgNotice']->value) {?>
                <?php  $_smarty_tpl->tpl_vars['aMsg'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['aMsg']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aMsgNotice']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['aMsg']->key => $_smarty_tpl->tpl_vars['aMsg']->value) {
$_smarty_tpl->tpl_vars['aMsg']->_loop = true;
?>
                    <div class="b-sysmessage_success alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <?php if ($_smarty_tpl->tpl_vars['aMsg']->value['title']!='') {?>
                            <h4 class="alert-heading"><?php echo $_smarty_tpl->tpl_vars['aMsg']->value['title'];?>
:</h4>
                        <?php }?>
                        <?php echo $_smarty_tpl->tpl_vars['aMsg']->value['msg'];?>

                    </div>
                <?php } ?>
            <?php }?>
        </div>
    </div>
    <?php }?>


    
    <div class="btn-group">
        <a href="<?php echo smarty_function_router(array('page'=>'admin'),$_smarty_tpl);?>
site-plugins/add/" class="btn btn-primary tip-top"
           title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['plugin_load'];?>
"><i class="icon icon-plus"></i></a>
    </div>
    <div class="btn-group">
        <a class="btn btn-default <?php if ($_smarty_tpl->tpl_vars['sMode']->value=='all'||$_smarty_tpl->tpl_vars['sMode']->value=='') {?>active<?php }?>" href="<?php echo smarty_function_router(array('page'=>'admin'),$_smarty_tpl);?>
site-plugins/list/all/">
            <?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['all_plugins'];?>

        </a>
        <a class="btn btn-default <?php if ($_smarty_tpl->tpl_vars['sMode']->value=='active') {?>active<?php }?>" href="<?php echo smarty_function_router(array('page'=>'admin'),$_smarty_tpl);?>
site-plugins/list/active/">
            <?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['active_plugins'];?>

        </a>
        <a class="btn btn-default <?php if ($_smarty_tpl->tpl_vars['sMode']->value=='inactive') {?>active<?php }?>" href="<?php echo smarty_function_router(array('page'=>'admin'),$_smarty_tpl);?>
site-plugins/list/inactive/">
            <?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['inactive_plugins'];?>

        </a>
    </div>


    
    <form action="<?php echo smarty_function_router(array('page'=>'admin'),$_smarty_tpl);?>
site-plugins/" method="post" id="form_plugins_list" class="uniform">
        <input type="hidden" name="security_key" value="<?php echo $_smarty_tpl->tpl_vars['ALTO_SECURITY_KEY']->value;?>
"/>

        <div class="b-wbox">
            <div class="b-wbox-content nopadding">

                <table class="table plugins-list">
                    <thead>
                    <tr>
                        <th>
                            <input type="checkbox" name="" onclick="admin.selectAllRows(this);"/>
                        </th>
                        <th class="name"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['plugin_name'];?>
</th>
                        <th class="version"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['plugin_version'];?>
</th>
                        <th class="author"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['plugin_author'];?>
</th>
                        <th class="action"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['plugin_action'];?>
</th>
                        <th class=""><?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['menu_settings'];?>
</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php  $_smarty_tpl->tpl_vars['oPlugin'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oPlugin']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aPluginList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oPlugin']->key => $_smarty_tpl->tpl_vars['oPlugin']->value) {
$_smarty_tpl->tpl_vars['oPlugin']->_loop = true;
?>
                        <tr id="plugin-<?php echo $_smarty_tpl->tpl_vars['oPlugin']->value->GetId();?>
"
                            class="<?php if ($_smarty_tpl->tpl_vars['oPlugin']->value->IsActive()) {?>success<?php } else { ?>inactive<?php }?> selectable">
                            <td class="check-row">
                                <input type="checkbox" name="plugin_sel[]" value="<?php echo $_smarty_tpl->tpl_vars['oPlugin']->value->GetId();?>
"
                                       class="form_plugins_checkbox"/>
                            </td>
                            <td class="name">
                                <div class="i-title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oPlugin']->value->GetName(), ENT_QUOTES, 'UTF-8', true);?>
</div>
                                <div class="description">
                                    <b><?php echo $_smarty_tpl->tpl_vars['oPlugin']->value->GetId();?>
</b> - <?php echo $_smarty_tpl->tpl_vars['oPlugin']->value->GetDescription();?>

                                </div>
                                <?php if (($_smarty_tpl->tpl_vars['oPlugin']->value->GetHomepage()>'')) {?>
                                    <div class="url">
                                        Homepage: <?php echo $_smarty_tpl->tpl_vars['oPlugin']->value->GetHomepage();?>

                                    </div>
                                <?php }?>
                            </td>
                            <td class="version"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oPlugin']->value->GetVersion(), ENT_QUOTES, 'UTF-8', true);?>
</td>
                            <td class="author"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oPlugin']->value->GetAuthor(), ENT_QUOTES, 'UTF-8', true);?>
</td>
                            <td class="action">
                                <div class="b-switch"
                                     onclick="admin.plugin.turn('<?php echo $_smarty_tpl->tpl_vars['oPlugin']->value->GetId();?>
', '<?php echo !$_smarty_tpl->tpl_vars['oPlugin']->value->isActive();?>
'); return false;">
                                    <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['oPlugin']->value->isActive()) {?>checked<?php }?>
                                           name="b-switch-<?php echo $_smarty_tpl->tpl_vars['oPlugin']->value->GetId();?>
">
                                    <label><i></i></label>
                                </div>
                            </td>
                            <td class="center">
                                <?php if ($_smarty_tpl->tpl_vars['oPlugin']->value->isActive()&&$_smarty_tpl->tpl_vars['oPlugin']->value->GetProperty('settings')!='') {?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['oPlugin']->value->GetProperty('settings');?>
"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['plugins_plugin_settings'];?>
</a>
                                <?php }?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <!-- <br/> <?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['plugin_priority_notice'];?>
 -->
                <!-- <input type="submit" name="submit_plugins_save" value="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['adm_save'];?>
" onclick="adminPluginSave();" /> -->
            </div>
        </div>

        <input type="hidden" name="plugin_action" value="">

        <div class="navbar navbar-inner">
            <button type="submit" name="submit_plugins_del" class="btn btn-danger pull-right"
                    onclick="admin.confirmDelete(); return false;">
                <?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['plugin_submit_delete'];?>

            </button>
        </div>
    </form>

    <script>
        var admin = admin || { };

        admin.confirmDelete = function () {
            if ($('.form_plugins_checkbox:checked').length) {
                ls.modal.confirm({
                    title: '<?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['plugin_submit_delete'];?>
',
                    message: '<?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['plugin_delete_confirm'];?>
',
                    onConfirm: function () {
                        $('#form_plugins_list [name=plugin_action]').val('delete');
                        $('#form_plugins_list').submit();
                    }
                });
            } else {
                ls.modal.alert({
                    content: '<?php echo $_smarty_tpl->tpl_vars['aLang']->value['action']['admin']['plugin_need_select_for_delete'];?>
'
                });
            }

        }
    </script>



</div>

</div>

<?php echo smarty_function_hook(array('run'=>'layout_body_end'),$_smarty_tpl);?>


</body>
</html><?php }} ?>
