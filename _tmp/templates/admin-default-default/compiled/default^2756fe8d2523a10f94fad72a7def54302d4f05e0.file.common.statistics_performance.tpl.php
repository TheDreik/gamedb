<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:10
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/admin-default/tpls/commons/common.statistics_performance.tpl" */ ?>
<?php /*%%SmartyHeaderCode:141289433955940ec664ed96-80729245%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2756fe8d2523a10f94fad72a7def54302d4f05e0' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/admin-default/tpls/commons/common.statistics_performance.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '141289433955940ec664ed96-80729245',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aStatsPerformance' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ec6699864_12798273',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ec6699864_12798273')) {function content_55940ec6699864_12798273($_smarty_tpl) {?><?php if (!is_callable('smarty_function_hook')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.hook.php';
?><?php if (E::IsAdmin()) {?>
<div class="b-page-stats">
    <?php echo smarty_function_hook(array('run'=>'statistics_performance_begin'),$_smarty_tpl);?>

    <table>
        <tr>
            <td>
                <h4 class="b-page-stats-head">Database</h4>
                query: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['sql']['count'];?>
</strong><br/>
                time: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['sql']['time'];?>
</strong>
            </td>
            <td>
                <h4 class="b-page-stats-head">Cache (<?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['cache']['mode'];?>
)</h4>
                query: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['cache']['count'];?>
</strong><br/>
                &mdash; set: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['cache']['count_set'];?>
</strong><br/>
                &mdash; get: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['cache']['count_get'];?>
</strong><br/>
                time: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['cache']['time'];?>
</strong>
            </td>
            <td>
                <h4 class="b-page-stats-head">Viewer</h4>
                total time: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['viewer']['total'];?>
</strong><br/>
                &mdash; render calls: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['viewer']['count'];?>
</strong><br/>
                &mdash; render time: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['viewer']['time'];?>
</strong><br/>
            </td>
            <td>
                <h4 class="b-page-stats-head">PHP</h4>
                time load modules: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['engine']['time_load_module'];?>
</strong><br/>
                included files: <br/>
                &mdash; count: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['engine']['files_count'];?>
</strong><br/>
                &mdash; time: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['engine']['files_time'];?>
</strong><br/>
                full time: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['engine']['full_time'];?>
</strong>
            </td>
            <td>
                <h4 class="b-page-stats-head">Memory</h4>
                memory limit: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['memory']['limit'];?>
</strong><br/>
                memory usage: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['memory']['usage'];?>
</strong><br/>
                peak usage: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['memory']['peak'];?>
</strong>
            </td>
            <?php echo smarty_function_hook(array('run'=>'statistics_performance_item'),$_smarty_tpl);?>

        </tr>
    </table>
    <?php echo smarty_function_hook(array('run'=>'statistics_performance_end'),$_smarty_tpl);?>

</div>
<?php }?><?php }} ?>
