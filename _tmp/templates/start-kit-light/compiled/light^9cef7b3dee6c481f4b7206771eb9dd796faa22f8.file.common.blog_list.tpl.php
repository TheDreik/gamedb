<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:22
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/commons/common.blog_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:57532115955940ed2a2fba0-60270904%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9cef7b3dee6c481f4b7206771eb9dd796faa22f8' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/commons/common.blog_list.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '57532115955940ed2a2fba0-60270904',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'bBlogsUseOrder' => 0,
    'sBlogsRootPage' => 0,
    'sBlogOrder' => 0,
    'sBlogOrderWayNext' => 0,
    'sBlogOrderWay' => 0,
    'aLang' => 0,
    'aBlogs' => 0,
    'oBlog' => 0,
    'oBlogType' => 0,
    'sBlogsEmptyList' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ed2b276b0_34003434',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ed2b276b0_34003434')) {function content_55940ed2b276b0_34003434($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/var/www/kolenka/gamedb/engine/libs/Smarty/libs/plugins/modifier.truncate.php';
?><table class="table table-blogs">
    <?php if ($_smarty_tpl->tpl_vars['bBlogsUseOrder']->value) {?>
        <thead>
        <tr>
            <th class="cell-name">
                <small>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['sBlogsRootPage']->value;?>
?order=blog_title&order_way=<?php if ($_smarty_tpl->tpl_vars['sBlogOrder']->value=='blog_title') {?><?php echo $_smarty_tpl->tpl_vars['sBlogOrderWayNext']->value;?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['sBlogOrderWay']->value;?>
<?php }?>"
                       <?php if ($_smarty_tpl->tpl_vars['sBlogOrder']->value=='blog_title') {?>class="<?php echo $_smarty_tpl->tpl_vars['sBlogOrderWay']->value;?>
"<?php }?>><span><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blogs_title'];?>
</span></a>
                </small>
            </th>

            <?php if (E::IsUser()) {?>
                <th class="cell-join">
                    <small><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_join_leave'];?>
</small>
                </th>
            <?php }?>

            <th class="small cell-readers">
                <a href="<?php echo $_smarty_tpl->tpl_vars['sBlogsRootPage']->value;?>
?order=blog_count_topic&order_way=<?php if ($_smarty_tpl->tpl_vars['sBlogOrder']->value=='blog_count_topic') {?><?php echo $_smarty_tpl->tpl_vars['sBlogOrderWayNext']->value;?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['sBlogOrderWay']->value;?>
<?php }?>"
                   <?php if ($_smarty_tpl->tpl_vars['sBlogOrder']->value=='blog_count_topic') {?>class="<?php echo $_smarty_tpl->tpl_vars['sBlogOrderWay']->value;?>
"<?php }?>><span><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blogs_topic_count'];?>
</span></a>
            </th>
            <th class="cell-rating align-center">
                <small>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['sBlogsRootPage']->value;?>
?order=blog_rating&order_way=<?php if ($_smarty_tpl->tpl_vars['sBlogOrder']->value=='blog_rating') {?><?php echo $_smarty_tpl->tpl_vars['sBlogOrderWayNext']->value;?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['sBlogOrderWay']->value;?>
<?php }?>"
                       <?php if ($_smarty_tpl->tpl_vars['sBlogOrder']->value=='blog_rating') {?>class="<?php echo $_smarty_tpl->tpl_vars['sBlogOrderWay']->value;?>
"<?php }?>><span><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blogs_rating'];?>
</span></a>
                </small>
            </th>
        </tr>
        </thead>
    <?php } else { ?>
        <thead>
        <tr>
            <th class="cell-name">
                <small><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blogs_title'];?>
</small>
            </th>

            <?php if (E::IsUser()) {?>
                <th class="cell-join">
                    <small><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_join_leave'];?>
</small>
                </th>
            <?php }?>

            <th class="cell-readers">
                <small><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blogs_readers'];?>
</small>
            </th>
            <th class="cell-rating align-center">
                <small><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blogs_rating'];?>
</small>
            </th>
        </tr>
        </thead>
    <?php }?>

    <tbody>
    <?php if ($_smarty_tpl->tpl_vars['aBlogs']->value) {?>
        <?php  $_smarty_tpl->tpl_vars['oBlog'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oBlog']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aBlogs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oBlog']->key => $_smarty_tpl->tpl_vars['oBlog']->value) {
$_smarty_tpl->tpl_vars['oBlog']->_loop = true;
?>
            <?php $_smarty_tpl->tpl_vars['oUserOwner'] = new Smarty_variable($_smarty_tpl->tpl_vars['oBlog']->value->getOwner(), null, 0);?>
            <?php $_smarty_tpl->tpl_vars['oBlogType'] = new Smarty_variable($_smarty_tpl->tpl_vars['oBlog']->value->getBlogType(), null, 0);?>
            <tr>
                <td class="cell-name">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getUrlFull();?>
">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getAvatarPath(48);?>
" width="48" height="48" class="avatar visible-lg"/>
                    </a>

                    <h4>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getUrlFull();?>
" class="blog-name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oBlog']->value->getTitle(), ENT_QUOTES, 'UTF-8', true);?>
</a>
                        <?php if ($_smarty_tpl->tpl_vars['oBlogType']->value) {?>
                            <?php if ($_smarty_tpl->tpl_vars['oBlogType']->value->IsHidden()) {?>
                                <span title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_closed'];?>
" class="glyphicon glyphicon-eye-close"></span>
                            <?php } elseif ($_smarty_tpl->tpl_vars['oBlogType']->value->IsPrivate()) {?>
                                <span title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_closed'];?>
" class="glyphicon glyphicon-lock"></span>
                            <?php }?>
                        <?php }?>
                    </h4>

                    <p class="blog-description"><?php echo htmlspecialchars(smarty_modifier_truncate(trim(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['oBlog']->value->getDescription())),150,'...'), ENT_QUOTES, 'UTF-8', true);?>
</p>
                </td>

                <?php if (E::IsUser()) {?>
                    <td class="small cell-join">
                        <?php if ((E::UserId()!=$_smarty_tpl->tpl_vars['oBlog']->value->getOwnerId())&&$_smarty_tpl->tpl_vars['oBlogType']->value->GetMembership(ModuleBlog::BLOG_USER_JOIN_FREE)) {?>
                            <a href="#" onclick="ls.blog.toggleJoin(this, <?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getId();?>
); return false;"
                               class="link-dotted">
                                <?php if ($_smarty_tpl->tpl_vars['oBlog']->value->getUserIsJoin()) {?>
                                    <?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_leave'];?>

                                <?php } else { ?>
                                    <?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_join'];?>

                                <?php }?>
                            </a>
                        <?php } else { ?>
                            &mdash;
                        <?php }?>
                    </td>
                <?php }?>

                <td class="small cell-readers" id="blog_user_count_<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getId();?>
"><?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getCountTopic();?>
</td>
                <td class="small text-success cell-rating"><?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getRating();?>
</td>
            </tr>
        <?php } ?>
    <?php } else { ?>
        <tr>
            <td colspan="3">
                <?php if ($_smarty_tpl->tpl_vars['sBlogsEmptyList']->value) {?>
                    <?php echo $_smarty_tpl->tpl_vars['sBlogsEmptyList']->value;?>

                <?php } else { ?>

                <?php }?>
            </td>
        </tr>
    <?php }?>
    </tbody>
</table>
<?php }} ?>
