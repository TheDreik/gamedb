<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:25
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/widgets/widget.stream_comment.tpl" */ ?>
<?php /*%%SmartyHeaderCode:185693380255940ed5776565-11119140%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c378ef4057fa25d2b6b991e6eece0fd48a80ec47' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/widgets/widget.stream_comment.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '185693380255940ed5776565-11119140',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aComments' => 0,
    'oComment' => 0,
    'oTopic' => 0,
    'oUser' => 0,
    'aLang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ed5817271_33259595',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ed5817271_33259595')) {function content_55940ed5817271_33259595($_smarty_tpl) {?><?php if (!is_callable('smarty_function_date_format')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.date_format.php';
if (!is_callable('smarty_function_router')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.router.php';
if (!is_callable('smarty_modifier_truncate')) include '/var/www/kolenka/gamedb/engine/libs/Smarty/libs/plugins/modifier.truncate.php';
?><?php if ($_smarty_tpl->tpl_vars['aComments']->value) {?>
    <ul class="list-unstyled item-list">
        <?php  $_smarty_tpl->tpl_vars['oComment'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oComment']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aComments']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oComment']->key => $_smarty_tpl->tpl_vars['oComment']->value) {
$_smarty_tpl->tpl_vars['oComment']->_loop = true;
?>
            <?php $_smarty_tpl->tpl_vars['oUser'] = new Smarty_variable($_smarty_tpl->tpl_vars['oComment']->value->getUser(), null, 0);?>
            <?php $_smarty_tpl->tpl_vars['oTopic'] = new Smarty_variable($_smarty_tpl->tpl_vars['oComment']->value->getTarget(), null, 0);?>
            <?php $_smarty_tpl->tpl_vars['oBlog'] = new Smarty_variable($_smarty_tpl->tpl_vars['oTopic']->value->getBlog(), null, 0);?>
            <li class="js-title-comment js-title-comment" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oTopic']->value->getTitle(), ENT_QUOTES, 'UTF-8', true);?>
">
                <p>
                    <?php if (!is_null($_smarty_tpl->tpl_vars['oComment']->value->getGuestLogin())) {?>
                        <?php echo $_smarty_tpl->tpl_vars['oComment']->value->getGuestLogin();?>

                    <?php } else { ?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['oUser']->value->getProfileUrl();?>
" class="author"><?php echo $_smarty_tpl->tpl_vars['oUser']->value->getDisplayName();?>
</a>
                    <?php }?>
                    <time datetime="<?php echo smarty_function_date_format(array('date'=>$_smarty_tpl->tpl_vars['oComment']->value->getDate(),'format'=>'c'),$_smarty_tpl);?>
" class="text-muted">
                        · <?php echo smarty_function_date_format(array('date'=>$_smarty_tpl->tpl_vars['oComment']->value->getDate(),'hours_back'=>"12",'minutes_back'=>"60",'now'=>"60",'day'=>"day H:i",'format'=>"j F Y, H:i"),$_smarty_tpl);?>

                    </time>
                </p>
                <a href="<?php if (Config::Get('module.comment.nested_per_page')) {?><?php echo smarty_function_router(array('page'=>'comments'),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getUrl();?>
#comment<?php }?><?php echo $_smarty_tpl->tpl_vars['oComment']->value->getId();?>
"
                   class="stream-topic"><?php echo htmlspecialchars(smarty_modifier_truncate(trim(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['oComment']->value->getText())),100,'...'), ENT_QUOTES, 'UTF-8', true);?>
</a> &rarr;
                   <a href="<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getUrl();?>
" class="stream-topic" style="color:#906090"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oTopic']->value->getTitle(), ENT_QUOTES, 'UTF-8', true);?>
</a>
            </li>
        <?php } ?>
    </ul>
<?php } else { ?>
    <?php echo $_smarty_tpl->tpl_vars['aLang']->value['widget_stream_comments_no'];?>

<?php }?>

<footer class="small text-muted">
    <a href="<?php echo smarty_function_router(array('page'=>'comments'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['widget_stream_comments_all'];?>
</a> ·
    <a href="<?php echo smarty_function_router(array('page'=>'rss'),$_smarty_tpl);?>
allcomments/">RSS</a>
</footer>
<?php }} ?>
