<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:08:08
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/modals/modal.auth.tpl" */ ?>
<?php /*%%SmartyHeaderCode:194816235855941068e3c388-12361983%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ee5548a6c4e4fe5a8e117516805db1a30b074fa4' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/modals/modal.auth.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '194816235855941068e3c388-12361983',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
    'PATH_WEB_CURRENT' => 0,
    '_aRequest' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55941068f0a298_33550653',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55941068f0a298_33550653')) {function content_55941068f0a298_33550653($_smarty_tpl) {?><?php if (!is_callable('smarty_function_router')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.router.php';
if (!is_callable('smarty_function_hook')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.hook.php';
if (!is_callable('smarty_block_hookb')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/block.hookb.php';
if (!is_callable('smarty_function_asset')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.asset.php';
?><?php if (!E::IsUser()) {?>
    <div class="modal fade in" id="modal-auth">
        <div class="modal-dialog">
            <div class="modal-content">

                <header class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_authorization'];?>
</h4>
                </header>

                <div class="modal-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#" data-toggle="tab" data-target=".js-pane-login" class="js-tab-login"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_login_submit'];?>
</a></li>
                        <?php if (!Config::Get('general.reg.invite')) {?>
                            <li><a href="#" data-toggle="tab" data-target=".js-pane-registration" class="js-tab-registration"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['registration'];?>
</a></li>
                        <?php } else { ?>
                            <li><a href="<?php echo smarty_function_router(array('page'=>'registration'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['registration'];?>
</a></li>
                        <?php }?>
                        <li><a href="#" data-toggle="tab" data-target=".js-pane-reminder" class="js-tab-reminder"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['password_reminder'];?>
</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active js-pane-login">
                            <?php echo smarty_function_hook(array('run'=>'pane_login_begin'),$_smarty_tpl);?>


                            <form action="<?php echo smarty_function_router(array('page'=>'login'),$_smarty_tpl);?>
" method="post" class="js-form-login">
                                <?php echo smarty_function_hook(array('run'=>'form_login_begin'),$_smarty_tpl);?>


                                <div class="form-group">
                                    <label for="input-login"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_login'];?>
</label>
                                    <input type="text" name="login" id="input-login" class="form-control js-focus-in" required>
                                </div>

                                <div class="form-group">
                                    <label for="input-password"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_password'];?>
</label>
                                    <input type="password" name="password" id="input-password" class="form-control" required>

                                    <p class="help-block">
                                        <small class="text-danger validate-error-hide validate-error-login"></small>
                                    </p>
                                </div>

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" checked> <?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_login_remember'];?>

                                    </label>
                                </div>

                                <?php echo smarty_function_hook(array('run'=>'form_login_end'),$_smarty_tpl);?>


                                <input type="hidden" name="return-path" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['PATH_WEB_CURRENT']->value, ENT_QUOTES, 'UTF-8', true);?>
">
                                <button type="submit" name="submit_login" class="btn btn-success js-form-login-submit"
                                        disabled="disabled"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_login_submit'];?>
</button>

                            </form>

                            <?php echo smarty_function_hook(array('run'=>'pane_login_end'),$_smarty_tpl);?>

                        </div>

                        <?php if (!Config::Get('general.reg.invite')) {?>
                        <div class="tab-pane js-pane-registration">
                            <?php echo smarty_function_hook(array('run'=>'pane_registration_begin','isPopup'=>true),$_smarty_tpl);?>

                            <form action="<?php echo smarty_function_router(array('page'=>'registration'),$_smarty_tpl);?>
" method="post" class="js-form-registration">
                                <?php echo smarty_function_hook(array('run'=>'form_registration_begin','isPopup'=>true),$_smarty_tpl);?>


                                <div class="form-group">
                                    <label for="input-registration-login"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['registration_login'];?>
</label>
                                    <span class="glyphicon glyphicon-question-sign text-muted js-tip-help"
                                          title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['registration_login_notice'];?>
"></span>
                                    <span class="glyphicon glyphicon-ok text-success validate-ok-field-login"
                                          style="display: none"></span>
                                    <input type="text" name="login" id="input-registration-login"
                                           value="<?php echo $_smarty_tpl->tpl_vars['_aRequest']->value['login'];?>
" class="form-control js-ajax-validate js-focus-in" required/>

                                    <p class="help-block">
                                        <small class="text-danger validate-error-hide validate-error-field-login"></small>
                                    </p>
                                </div>

                                <div class="form-group">
                                    <label for="input-registration-mail"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['registration_mail'];?>
</label>
                                    <span class="glyphicon glyphicon-question-sign text-muted js-tip-help" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['registration_mail_notice'];?>
"></span>
                                    <span class="glyphicon glyphicon-ok text-success validate-ok-field-mail" style="display: none"></span>
                                    <input type="text" name="mail" id="input-registration-mail" value="<?php echo $_smarty_tpl->tpl_vars['_aRequest']->value['mail'];?>
" class="form-control js-ajax-validate" required/>

                                    <p class="help-block">
                                        <small class="text-danger validate-error-hide validate-error-field-mail"></small>
                                    </p>
                                </div>

                                <div class="form-group">
                                    <label for="input-registration-password"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['registration_password'];?>
</label>
                                    <span class="glyphicon glyphicon-question-sign text-muted js-tip-help" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['registration_password_notice'];?>
"></span>
                                    <span class="glyphicon glyphicon-ok text-success validate-ok-field-password" style="display: none"></span>
                                    <input type="password" name="password" id="input-registration-password" value="" class="form-control js-ajax-validate" required/>

                                    <p class="help-block">
                                        <small class="text-danger validate-error-hide validate-error-field-password"></small>
                                    </p>
                                </div>

                                <div class="form-group">
                                    <label for="input-registration-password-confirm"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['registration_password_retry'];?>
</label>
                                    <span class="glyphicon glyphicon-ok text-success validate-ok-field-password_confirm" style="display: none"></span>
                                    <input type="password" value="" id="input-registration-password-confirm" name="password_confirm" class="form-control js-ajax-validate" required/>

                                    <p class="help-block">
                                        <small class="text-danger validate-error-hide validate-error-field-password_confirm"></small>
                                    </p>
                                </div>

                                <?php $_smarty_tpl->smarty->_tag_stack[] = array('hookb', array('run'=>"form_registration_captcha")); $_block_repeat=true; echo smarty_block_hookb(array('run'=>"form_registration_captcha"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                                    <div class="form-group">
                                        <label for="input-registration-captcha" class="captcha"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['registration_captcha'];?>
</label>
                                        <img src="<?php echo smarty_function_asset(array('file'=>"assets/images/loader.gif"),$_smarty_tpl);?>
" onclick="this.src='<?php echo smarty_function_router(array('page'=>'captcha'),$_smarty_tpl);?>
?n='+Math.random();"
                                             class="form-control captcha-image"/>
                                        <input type="text" name="captcha" id="input-registration-captcha" value=""
                                               maxlength="3" class="form-control captcha-input js-ajax-validate" required/>

                                        <p class="help-block">
                                            <small class="text-danger validate-error-hide validate-error-field-captcha"></small>
                                        </p>
                                    </div>
                                <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hookb(array('run'=>"form_registration_captcha"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


                                <?php echo smarty_function_hook(array('run'=>'form_registration_end','isPopup'=>true),$_smarty_tpl);?>


                                <input type="hidden" name="return-path" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['PATH_WEB_CURRENT']->value, ENT_QUOTES, 'UTF-8', true);?>
">
                                <button type="submit" name="submit_register" class="btn btn-success js-form-registration-submit"
                                        disabled="disabled"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['registration_submit'];?>
</button>

                            </form>
                            <?php echo smarty_function_hook(array('run'=>'pane_registration_end','isPopup'=>true),$_smarty_tpl);?>

                        </div>
                        <?php }?>

                        <div class="tab-pane js-pane-reminder">
                            <?php echo smarty_function_hook(array('run'=>'pane_reminder_begin','isPopup'=>true),$_smarty_tpl);?>

                            <form action="<?php echo smarty_function_router(array('page'=>'login'),$_smarty_tpl);?>
reminder/" method="POST" class="js-form-reminder">
                                <?php echo smarty_function_hook(array('run'=>'form_reminder_begin','isPopup'=>true),$_smarty_tpl);?>

                                <div class="form-group">
                                    <label for="input-reminder-mail"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['password_reminder_email'];?>
</label>
                                    <input type="text" name="mail" id="input-reminder-mail" class="form-control js-focus-in" required/>

                                    <p class="help-block">
                                        <small class="text-danger validate-error-hide validate-error-reminder"></small>
                                    </p>
                                </div>

                                <?php echo smarty_function_hook(array('run'=>'form_reminder_end','isPopup'=>true),$_smarty_tpl);?>


                                <button type="submit" name="submit_reminder" class="btn btn-success js-form-reminder-submit"
                                        disabled="disabled"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['password_reminder_submit'];?>
</button>

                            </form>
                            <?php echo smarty_function_hook(array('run'=>'pane_reminder_end','isPopup'=>true),$_smarty_tpl);?>

                        </div>
                    </div>

                </div><!-- /.modal-body -->

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <script type="text/javascript">
        jQuery(function ($) {
            var selectFirstInput = function() {
                $('#modal-auth .tab-pane.active input[type=text]:first:visible').focus();
            }
            $('#modal-auth').on('shown.bs.modal', selectFirstInput);
            $('#modal-auth [data-toggle=tab]').on('shown.bs.tab', selectFirstInput);

            // --- //
            $('.js-form-login-submit').prop('disabled', false);

            // --- //
            $('.js-form-registration input.js-ajax-validate').blur(function (e) {
                var params = { };
                var fieldName = $(e.target).attr('name');
                var fieldValue = $(e.target).val();
                var form = $(e.target).parents('form').first();

                if (fieldName == 'password_confirm') {
                    params['password'] = $('#input-registration-password').val();
                }
                if (fieldName == 'password') {
                    params['password'] = $('#input-registration-password').val();
                    if ($('#input-registration-password-confirm').val()) {
                        ls.user.validateRegistrationField(form,  'password_confirm', $('#input-registration-password-confirm').val(), { password: fieldValue });
                    }
                }
                ls.user.validateRegistrationField(form, fieldName, fieldValue, params);
            });
            $('.js-form-registration-submit').prop('disabled', false);

            // -- //
            $('.js-form-reminder-submit').prop('disabled', false);
        });
    </script>

<?php }?>
<?php }} ?>
