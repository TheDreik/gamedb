<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:22
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/widgets/widget.stream.tpl" */ ?>
<?php /*%%SmartyHeaderCode:29175563755940ed2c708b5-26285271%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bdd89d4d82f44918a924aa36e0388da763fb6eb5' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/widgets/widget.stream.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '29175563755940ed2c708b5-26285271',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
    'aWidgetParams' => 0,
    'aItem' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ed2c94d86_15185236',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ed2c94d86_15185236')) {function content_55940ed2c94d86_15185236($_smarty_tpl) {?><?php if (!is_callable('smarty_function_router')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.router.php';
if (!is_callable('smarty_function_hook')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.hook.php';
?><section class="panel panel-default widget widget-type-stream">
    <div class="panel-body">

        <header class="widget-header">
            <h3 class="widget-title">
                <a href="<?php echo smarty_function_router(array('page'=>'comments'),$_smarty_tpl);?>
" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['widget_stream_comments_all'];?>
"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['widget_stream'];?>
</a>
            </h3>
        </header>

        <div class="widget-content">
            <ul class="nav nav-pills widget-content-navs js-widget-stream-navs">
                <?php  $_smarty_tpl->tpl_vars['aItem'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['aItem']->_loop = false;
 $_smarty_tpl->tpl_vars['sKey'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['aWidgetParams']->value['items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['aItem']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['aItem']->key => $_smarty_tpl->tpl_vars['aItem']->value) {
$_smarty_tpl->tpl_vars['aItem']->_loop = true;
 $_smarty_tpl->tpl_vars['sKey']->value = $_smarty_tpl->tpl_vars['aItem']->key;
 $_smarty_tpl->tpl_vars['aItem']->index++;
 $_smarty_tpl->tpl_vars['aItem']->first = $_smarty_tpl->tpl_vars['aItem']->index === 0;
?>
                    <li <?php if ($_smarty_tpl->tpl_vars['aItem']->first) {?>class="active"<?php }?>>
                        <a href="#" data-toggle="tab" data-type="<?php echo $_smarty_tpl->tpl_vars['aItem']->value['type'];?>
"><?php echo $_smarty_tpl->tpl_vars['aLang']->value[$_smarty_tpl->tpl_vars['aItem']->value['text']];?>
</a>
                    </li>
                <?php } ?>
                <?php echo smarty_function_hook(array('run'=>'widget_stream_nav_item'),$_smarty_tpl);?>

            </ul>
            <div class="widget-content-body js-widget-stream-content">
            </div>
        </div>

    </div>
</section>
<?php }} ?>
