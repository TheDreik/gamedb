<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:42
         compiled from "/var/www/kolenka/gamedb/common/plugins/br/templates/skin/start-kit/tpls/widgets/widget.brandingBlog.tpl" */ ?>
<?php /*%%SmartyHeaderCode:46822304655940ee6c4a4f4-83853084%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ccba4942b964dc89e840e4d43fca3c79eea158b2' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/plugins/br/templates/skin/start-kit/tpls/widgets/widget.brandingBlog.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '46822304655940ee6c4a4f4-83853084',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'oBlog' => 0,
    'aLang' => 0,
    'sTemplatePathBr' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ee6d0c769_51076346',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ee6d0c769_51076346')) {function content_55940ee6d0c769_51076346($_smarty_tpl) {?><?php if (!is_callable('smarty_function_lang_load')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.lang_load.php';
if (!is_callable('smarty_function_asset')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.asset.php';
?><?php if (Config::Get('plugin.br.blog.allow_blog')==true) {?>
<script>
    $(function () {
        ls.lang.load(<?php echo smarty_function_lang_load(array('name'=>"plugin.br.blog_image_loading_good,plugin.br.blog_saved,plugin.br.blog_image_loading_revert,plugin.br.scroll_to_intensive,plugin.br.blog_branding_removed"),$_smarty_tpl);?>
);
    })
</script>

<div class="panel panel-default sidebar widget widget-blog <?php if (Router::GetActionEvent()!='add') {?>js-branding-blog-panel js-branding-panel<?php }?> branding-panel"
     data-lang-blog-image-loading-good="plugin.br.blog_image_loading_good"
     data-lang-blog-opacity-loading-good="plugin.br.blog_saved"
     data-lang-blog-image-loading-revert="plugin.br.blog_image_loading_revert"
     data-lang-blog-branding-removed="plugin.br.blog_branding_removed"
     data-theme-background-top-padding="<?php echo Config::Get('plugin.br.themes.startkit.background_top_padding');?>
px"

     data-content-selector="#container"
<?php if ($_smarty_tpl->tpl_vars['oBlog']->value) {?>
     data-branding-has="1"
     data-branding-target-id="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getId();?>
"
     data-branding-target-type="blog-branding"
     data-branding-background="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getBackground();?>
"
     data-branding-opacity="<?php if ($_smarty_tpl->tpl_vars['oBlog']->value->getOpacity()) {?><?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getOpacity();?>
<?php } else { ?>100<?php }?>"
     data-branding-background-color="<?php if (!is_null($_smarty_tpl->tpl_vars['oBlog']->value->getBackgroundColor())) {?><?php if ($_smarty_tpl->tpl_vars['oBlog']->value->getBackgroundColor()=='#0') {?>#000000<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getBackgroundColor();?>
<?php }?><?php } else { ?>#FFFFFF<?php }?>"
     data-branding-use-background-color="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getUseBackgroundColor();?>
"
     data-branding-font-color="<?php if (!is_null($_smarty_tpl->tpl_vars['oBlog']->value->getFontColor())) {?><?php if ($_smarty_tpl->tpl_vars['oBlog']->value->getFontColor()=='0') {?>#000000<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getFontColor();?>
<?php }?><?php } else { ?>#000000<?php }?>"
     data-branding-header-color="<?php if (!is_null($_smarty_tpl->tpl_vars['oBlog']->value->getHeaderColor())) {?><?php if ($_smarty_tpl->tpl_vars['oBlog']->value->getHeaderColor()=='0') {?>#000000<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getHeaderColor();?>
<?php }?><?php } else { ?>#000000<?php }?>"
     data-branding-header-step="<?php if ($_smarty_tpl->tpl_vars['oBlog']->value->getHeaderStep()) {?><?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getHeaderStep();?>
<?php } else { ?>0<?php }?>"
     data-branding-title="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getBrandingTitle();?>
"
     data-branding-description="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getBrandingDescription();?>
"
     data-branding-background-type="<?php if ($_smarty_tpl->tpl_vars['oBlog']->value->getBackgroundType()) {?><?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getBackgroundType();?>
<?php } else { ?>0<?php }?>"
<?php }?>
     data-palette="<?php echo smarty_function_asset(array('file'=>"assets/images/HueSaturation.png",'plugin'=>"br"),$_smarty_tpl);?>
"
     >

    <div class="panel-body pab24">
        <h4 class="panel-header">
            <i class="fa fa-file-image-o"></i>
            <?php echo $_smarty_tpl->tpl_vars['aLang']->value['plugin']['br']['branding'];?>

        </h4>

        <div class="panel-content">
            <?php if (Router::GetActionEvent()=='add') {?>
                <div>
                    <?php echo $_smarty_tpl->tpl_vars['aLang']->value['plugin']['br']['branding_save_notice'];?>

                </div>
            <?php } else { ?>
                
                <?php if (Config::Get('plugin.br.blog.allow_step')) {?><?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['sTemplatePathBr']->value)."/tpls/widgets/blog_parts/header-slider.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('oBlog'=>$_smarty_tpl->tpl_vars['oBlog']->value), 0);?>
<?php }?>
                
                <?php if (Config::Get('plugin.br.blog.allow_background')) {?><?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['sTemplatePathBr']->value)."/tpls/widgets/blog_parts/upload-blog-image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }?>

                
                <?php if (Config::Get('plugin.br.blog.allow_background')) {?><?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['sTemplatePathBr']->value)."/tpls/widgets/blog_parts/background-type.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }?>

                
                <?php if (Config::Get('plugin.br.blog.allow_background')) {?><?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['sTemplatePathBr']->value)."/tpls/widgets/blog_parts/opacity-slider.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('oBlog'=>$_smarty_tpl->tpl_vars['oBlog']->value), 0);?>
<?php }?>

                
                <?php if (Config::Get('plugin.br.blog.allow_font')) {?><?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['sTemplatePathBr']->value)."/tpls/widgets/blog_parts/font-color-picker.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }?>

                
                <?php if (Config::Get('plugin.br.blog.allow_header')) {?><?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['sTemplatePathBr']->value)."/tpls/widgets/blog_parts/header-color-picker.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }?>

            <?php }?>
        </div>

        <?php if ($_smarty_tpl->tpl_vars['oBlog']->value) {?>
            <div class="clearfix">
                <a class="btn btn-success   corner-no js-blog-save-branding" href="#" onclick="return false;">
                    <i class="glyphicon glyphicon-upload"></i>&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['aLang']->value['plugin']['br']['branding_save_blog'];?>

                </a>
                <a class="btn btn-danger  corner-no pull-right  js-blog-delete-branding" <?php if (!$_smarty_tpl->tpl_vars['oBlog']->value->getTargetId()) {?>style="display: none"<?php }?>
                   data-toggle="tooltip" data-placement="left" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['plugin']['br']['remove_branding'];?>
"
                   href="#"
                   onclick="return false;">
                    <i class="glyphicon glyphicon-remove"></i>
                </a>
            </div>
        <?php }?>
    </div>

</div>
<?php }?><?php }} ?>
