<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:02:28
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/modals/modal.blog_delete.tpl" */ ?>
<?php /*%%SmartyHeaderCode:146734290755940f14dc2732-91205279%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a8d157dd3265aa4cc37c05491553764871ea2e68' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/modals/modal.blog_delete.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '146734290755940f14dc2732-91205279',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
    'oBlog' => 0,
    'aBlogs' => 0,
    'oBlogDelete' => 0,
    'ALTO_SECURITY_KEY' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940f14e43914_20940702',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940f14e43914_20940702')) {function content_55940f14e43914_20940702($_smarty_tpl) {?><?php if (!is_callable('smarty_function_router')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.router.php';
?><div class="modal fade in" id="modal-blog_delete">
    <div class="modal-dialog">
        <div class="modal-content">

            <header class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    <?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_admin_delete_title'];?>

                </h4>
            </header>

            <form action="<?php echo smarty_function_router(array('page'=>'blog'),$_smarty_tpl);?>
delete/<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getId();?>
/" method="POST">
                <div class="modal-body">
                    <?php if (E::IsAdmin()) {?>
                        <div class="form-group">
                            <label for="topic_move_to"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_admin_delete_move'];?>
</label>
                            <select name="topic_move_to" id="topic_move_to" class="form-control">
                                <option value="-1"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_delete_clear'];?>
</option>
                                <?php if ($_smarty_tpl->tpl_vars['aBlogs']->value) {?>
                                    <optgroup label="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['blogs'];?>
">
                                        <?php  $_smarty_tpl->tpl_vars['oBlogDelete'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oBlogDelete']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aBlogs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oBlogDelete']->key => $_smarty_tpl->tpl_vars['oBlogDelete']->value) {
$_smarty_tpl->tpl_vars['oBlogDelete']->_loop = true;
?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['oBlogDelete']->value->getId();?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oBlogDelete']->value->getTitle(), ENT_QUOTES, 'UTF-8', true);?>
</option>
                                        <?php } ?>
                                    </optgroup>
                                <?php }?>
                            </select>
                        </div>
                    <?php } else { ?>
                        <input type="hidden" name="topic_move_to" id="topic_move_to" value="-1">
                    <?php }?>
                    <p><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_admin_delete_confirm'];?>
</p>
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="security_key" value="<?php echo $_smarty_tpl->tpl_vars['ALTO_SECURITY_KEY']->value;?>
"/>
                    <button type="submit" class="btn btn-primary"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_delete'];?>
</button>
                </div>
            </form>

        </div>
    </div>
</div>
<?php }} ?>
