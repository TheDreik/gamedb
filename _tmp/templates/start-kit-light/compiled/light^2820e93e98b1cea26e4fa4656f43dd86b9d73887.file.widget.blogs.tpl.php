<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:27
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/widgets/widget.blogs.tpl" */ ?>
<?php /*%%SmartyHeaderCode:81035474455940ed788da23-67669788%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2820e93e98b1cea26e4fa4656f43dd86b9d73887' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/widgets/widget.blogs.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '81035474455940ed788da23-67669788',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
    'sBlogsTop' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ed78cd1f3_68879261',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ed78cd1f3_68879261')) {function content_55940ed78cd1f3_68879261($_smarty_tpl) {?><?php if (!is_callable('smarty_function_router')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.router.php';
?><div class="panel panel-default widget" id="widget_blogs">
    <div class="panel-body">

        <header class="widget-header">
            <h3 class="widget-title"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['widget_blogs'];?>
</h3>
        </header>

        <div class="widget-content">
            <?php if (E::IsUser()) {?>
                <ul class="nav nav-pills js-block-blogs-nav">
                    <li class="active js-widget-blogs-item" data-type="top"><a href="#"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['widget_blogs_top'];?>
</a></li>
                    <li class="js-widget-blogs-item" data-type="join"><a href="#"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['widget_blogs_join'];?>
</a></li>
                    <li class="js-widget-blogs-item" data-type="self"><a href="#"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['widget_blogs_self'];?>
</a></li>
                </ul>
            <?php }?>

            <div class="js-widget-blogs-content">
                <?php echo $_smarty_tpl->tpl_vars['sBlogsTop']->value;?>

            </div>

            <footer>
                <a href="<?php echo smarty_function_router(array('page'=>'blogs'),$_smarty_tpl);?>
" class="small"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['widget_blogs_all'];?>
</a>
            </footer>
        </div>

    </div>
</div>
<script>
    jQuery(document).ready(function(){
        $('.js-widget-blogs-item').click(function(){
            ls.widgets.load(this, 'blogs');
            return false;
        });
    });
</script><?php }} ?>
