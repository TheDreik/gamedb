<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:34
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/modals/modal.upload_photoset.tpl" */ ?>
<?php /*%%SmartyHeaderCode:82351265955940ede3caa20-12465137%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd4ede9fce8eac147677d9b54618e461c21411fff' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/modals/modal.upload_photoset.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '82351265955940ede3caa20-12465137',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
    '_aRequest' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ede3d9c24_19442000',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ede3d9c24_19442000')) {function content_55940ede3d9c24_19442000($_smarty_tpl) {?><div class="modal fade in" id="modal-upload_photoset">
    <div class="modal-dialog">
        <div class="modal-content">

            <header class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg'];?>
</h4>
            </header>

            <div class="modal-body">
                <form id="photoset-upload-form" method="POST" enctype="multipart/form-data" onsubmit="return false;">

                    <div id="topic-photo-upload-input" class="topic-photo-upload-input">
                        <label for="photoset-upload-file"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_photoset_choose_image'];?>
:</label>
                        <input type="file" id="photoset-upload-file" name="Filedata"/><br><br>

                        <button type="submit" class="button button-primary"  onclick="ls.photoset.upload();">
                            <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_photoset_upload_choose'];?>

                        </button>
                        <button type="submit" class="button"  onclick="ls.photoset.closeForm();">
                            <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_photoset_upload_close'];?>

                        </button>

                        <input type="hidden" name="is_iframe" value="true"/>
                        <input type="hidden" name="topic_id" value="<?php echo $_smarty_tpl->tpl_vars['_aRequest']->value['topic_id'];?>
"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php }} ?>
