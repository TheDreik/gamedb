<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:22
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/commons/common.messages.tpl" */ ?>
<?php /*%%SmartyHeaderCode:136003016455940ed29c4d87-54300857%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '483a238e22f61ecccfa78d81fe53266c056d5bfe' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/commons/common.messages.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '136003016455940ed29c4d87-54300857',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'noShowSystemMessage' => 0,
    'aMsgError' => 0,
    'aMsg' => 0,
    'aMsgNotice' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ed2a14310_26545894',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ed2a14310_26545894')) {function content_55940ed2a14310_26545894($_smarty_tpl) {?><?php if (!$_smarty_tpl->tpl_vars['noShowSystemMessage']->value) {?>
    <?php if ($_smarty_tpl->tpl_vars['aMsgError']->value) {?>
        <div class="alert alert-danger alert-message">
            <span class="alert-message-sign glyphicon glyphicon-exclamation-sign"></span>
            <ul class="list-unstyled alert-message-list">
                <?php  $_smarty_tpl->tpl_vars['aMsg'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['aMsg']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aMsgError']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['aMsg']->key => $_smarty_tpl->tpl_vars['aMsg']->value) {
$_smarty_tpl->tpl_vars['aMsg']->_loop = true;
?>
                    <li>
                        <?php if ($_smarty_tpl->tpl_vars['aMsg']->value['title']!='') {?>
                            <strong><?php echo $_smarty_tpl->tpl_vars['aMsg']->value['title'];?>
</strong>:
                        <?php }?>
                        <?php echo $_smarty_tpl->tpl_vars['aMsg']->value['msg'];?>

                    </li>
                <?php } ?>
            </ul>
        </div>
    <?php }?>


    <?php if ($_smarty_tpl->tpl_vars['aMsgNotice']->value) {?>
        <div class="alert alert-success alert-message">
            <span class="alert-message-sign glyphicon glyphicon-info-sign"></span>
            <ul class="list-unstyled alert-message-list">
                <?php  $_smarty_tpl->tpl_vars['aMsg'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['aMsg']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aMsgNotice']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['aMsg']->key => $_smarty_tpl->tpl_vars['aMsg']->value) {
$_smarty_tpl->tpl_vars['aMsg']->_loop = true;
?>
                    <li>
                        <?php if ($_smarty_tpl->tpl_vars['aMsg']->value['title']!='') {?>
                            <strong><?php echo $_smarty_tpl->tpl_vars['aMsg']->value['title'];?>
</strong>:
                        <?php }?>
                        <?php echo $_smarty_tpl->tpl_vars['aMsg']->value['msg'];?>

                    </li>
                <?php } ?>
            </ul>
        </div>
    <?php }?>
<?php }?>
<?php }} ?>
