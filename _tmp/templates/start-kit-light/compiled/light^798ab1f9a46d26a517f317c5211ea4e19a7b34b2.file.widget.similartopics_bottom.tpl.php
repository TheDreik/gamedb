<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:03:30
         compiled from "/var/www/kolenka/gamedb/common/plugins/similartopics/templates/skin/default/tpls/widgets/widget.similartopics_bottom.tpl" */ ?>
<?php /*%%SmartyHeaderCode:164443659655940f52d4d689-71799802%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '798ab1f9a46d26a517f317c5211ea4e19a7b34b2' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/plugins/similartopics/templates/skin/default/tpls/widgets/widget.similartopics_bottom.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '164443659655940f52d4d689-71799802',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aWidgetParams' => 0,
    'oTopic' => 0,
    'aSimilarTopics' => 0,
    'aLang' => 0,
    'sCssClass' => 0,
    'bPreview' => 0,
    'oSimilarTopic' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940f52dd77e5_28825061',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940f52dd77e5_28825061')) {function content_55940f52dd77e5_28825061($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['aSimilarTopics'] = new Smarty_variable($_smarty_tpl->tpl_vars['oTopic']->value->getSimilarTopics($_smarty_tpl->tpl_vars['aWidgetParams']->value['limit']), null, 0);?>
<?php $_smarty_tpl->tpl_vars['nSimilarCount'] = new Smarty_variable($_smarty_tpl->tpl_vars['oTopic']->value->CountSimilarTopics(), null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['aSimilarTopics']->value) {?>
    <div class="simtopics panel panel-default">
        <div class="panel-body">

            <header class="simtopics-header">
                <h4 class="simtopics-title"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['plugin']['similartopics']['widget_title'];?>
</h4>
            </header>

            <div class="simtopics-content">

                <ul class="list-unstyled row">
                    <?php if (count($_smarty_tpl->tpl_vars['aSimilarTopics']->value)>1) {?><?php $_smarty_tpl->tpl_vars['sCssClass'] = new Smarty_variable("col-xs-6", null, 0);?><?php }?>
                    <?php $_smarty_tpl->tpl_vars['bPreview'] = new Smarty_variable($_smarty_tpl->tpl_vars['aWidgetParams']->value['preview']['enable'], null, 0);?>
                    <?php  $_smarty_tpl->tpl_vars['oSimilarTopic'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oSimilarTopic']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aSimilarTopics']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oSimilarTopic']->key => $_smarty_tpl->tpl_vars['oSimilarTopic']->value) {
$_smarty_tpl->tpl_vars['oSimilarTopic']->_loop = true;
?>
                        <li class="<?php echo $_smarty_tpl->tpl_vars['sCssClass']->value;?>
 <?php if ($_smarty_tpl->tpl_vars['bPreview']->value) {?>simtopics-preview-on<?php }?>">
                            <?php if ($_smarty_tpl->tpl_vars['aWidgetParams']->value['preview']['enable']) {?>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['oSimilarTopic']->value->getUrl();?>
" class="simtopics-topic-preview">
                                    <?php if ($_smarty_tpl->tpl_vars['oSimilarTopic']->value->getPreviewImage()) {?>
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['oSimilarTopic']->value->getPreviewImageUrl($_smarty_tpl->tpl_vars['aWidgetParams']->value['preview']['size']['default']);?>
">
                                    <?php }?>
                                </a>
                            <?php }?>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['oSimilarTopic']->value->getUrl();?>
" class="simtopics-topic-title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oSimilarTopic']->value->getTitle(), ENT_QUOTES, 'UTF-8', true);?>
</a>
                            <div class="simitopics-topic-intro">
                                <?php echo $_smarty_tpl->tpl_vars['oSimilarTopic']->value->getIntroText();?>

                            </div>
                        </li>
                    <?php } ?>
                </ul>

                
            </div>

        </div>
    </div>

<script>
    $(function(){
        var selector=".simtopics",
            el=$(selector);

        if (el.length) {
            el.insertAfter(".js-topic");
        }
    });
</script>
<?php }?>
<?php }} ?>
