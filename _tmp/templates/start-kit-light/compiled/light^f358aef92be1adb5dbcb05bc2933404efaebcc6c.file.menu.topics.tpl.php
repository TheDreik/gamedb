<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:27
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/menus/menu.topics.tpl" */ ?>
<?php /*%%SmartyHeaderCode:26488170655940ed757e2d4-60086166%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f358aef92be1adb5dbcb05bc2933404efaebcc6c' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/menus/menu.topics.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '26488170655940ed757e2d4-60086166',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'sMenuSubItemSelect' => 0,
    'aLang' => 0,
    'iCountTopicsNew' => 0,
    'sMenuItemSelect' => 0,
    'sPeriodSelectCurrent' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ed76c5b73_94255429',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ed76c5b73_94255429')) {function content_55940ed76c5b73_94255429($_smarty_tpl) {?><?php if (!is_callable('smarty_function_router')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.router.php';
if (!is_callable('smarty_function_hook')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.hook.php';
?><ul class="nav nav-pills context-menu">
    <li class="bordered<?php if ($_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value=='good') {?> active<?php }?>"><a
                href="<?php echo Config::Get('path.root.url');?>
"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_all_good'];?>
</a></li>

    <li class="bordered<?php if ($_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value=='new') {?> active<?php }?>">
        <?php if ($_smarty_tpl->tpl_vars['iCountTopicsNew']->value>0) {?>
            <a href="<?php echo smarty_function_router(array('page'=>'index'),$_smarty_tpl);?>
newall/" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_top_period_24h'];?>
"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_all_new'];?>

                +<?php echo $_smarty_tpl->tpl_vars['iCountTopicsNew']->value;?>
</a>
        <?php } else { ?>
            <a href="<?php echo smarty_function_router(array('page'=>'index'),$_smarty_tpl);?>
newall/"
               title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_top_period_all'];?>
"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_all_new'];?>
</a>
        <?php }?>
    </li>

    <li class="bordered<?php if ($_smarty_tpl->tpl_vars['sMenuItemSelect']->value=='feed') {?> active<?php }?>">
        <a href="<?php echo smarty_function_router(array('page'=>'feed'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['userfeed_title'];?>
</a>
    </li>

    <li>&nbsp;</li>

    <li class="dropdown<?php if ($_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value=='discussed') {?> active<?php }?>">
        <a href="<?php echo smarty_function_router(array('page'=>'index'),$_smarty_tpl);?>
discussed/" class="dropdown-toggle" data-toggle="dropdown">
            <?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_all_discussed'];?>

            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li <?php if ($_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value=='discussed'&$_smarty_tpl->tpl_vars['sPeriodSelectCurrent']->value=='1') {?>class="active"<?php }?>><a
                        href="<?php echo smarty_function_router(array('page'=>'index'),$_smarty_tpl);?>
discussed/?period=1"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_top_period_24h'];?>
</a></li>
            <li <?php if ($_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value=='discussed'&$_smarty_tpl->tpl_vars['sPeriodSelectCurrent']->value=='7') {?>class="active"<?php }?>><a
                        href="<?php echo smarty_function_router(array('page'=>'index'),$_smarty_tpl);?>
discussed/?period=7"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_top_period_7d'];?>
</a></li>
            <li <?php if ($_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value=='discussed'&$_smarty_tpl->tpl_vars['sPeriodSelectCurrent']->value=='30') {?>class="active"<?php }?>><a
                        href="<?php echo smarty_function_router(array('page'=>'index'),$_smarty_tpl);?>
discussed/?period=30"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_top_period_30d'];?>
</a></li>
            <li <?php if ($_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value=='discussed'&$_smarty_tpl->tpl_vars['sPeriodSelectCurrent']->value=='all') {?>class="active"<?php }?>><a
                        href="<?php echo smarty_function_router(array('page'=>'index'),$_smarty_tpl);?>
discussed/?period=all"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_top_period_all'];?>
</a></li>
        </ul>
    </li>

    <li class="dropdown<?php if ($_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value=='top') {?> active<?php }?>">
        <a href="<?php echo smarty_function_router(array('page'=>'index'),$_smarty_tpl);?>
top/" class="dropdown-toggle" data-toggle="dropdown">
            <?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_all_top'];?>

            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li <?php if ($_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value=='top'&$_smarty_tpl->tpl_vars['sPeriodSelectCurrent']->value=='1') {?>class="active"<?php }?>><a
                        href="<?php echo smarty_function_router(array('page'=>'index'),$_smarty_tpl);?>
top/?period=1"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_top_period_24h'];?>
</a></li>
            <li <?php if ($_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value=='top'&$_smarty_tpl->tpl_vars['sPeriodSelectCurrent']->value=='7') {?>class="active"<?php }?>><a
                        href="<?php echo smarty_function_router(array('page'=>'index'),$_smarty_tpl);?>
top/?period=7"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_top_period_7d'];?>
</a></li>
            <li <?php if ($_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value=='top'&$_smarty_tpl->tpl_vars['sPeriodSelectCurrent']->value=='30') {?>class="active"<?php }?>><a
                        href="<?php echo smarty_function_router(array('page'=>'index'),$_smarty_tpl);?>
top/?period=30"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_top_period_30d'];?>
</a></li>
            <li <?php if ($_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value=='top'&$_smarty_tpl->tpl_vars['sPeriodSelectCurrent']->value=='all') {?>class="active"<?php }?>><a
                        href="<?php echo smarty_function_router(array('page'=>'index'),$_smarty_tpl);?>
top/?period=all"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_top_period_all'];?>
</a></li>
        </ul>
    </li>

    <?php echo smarty_function_hook(array('run'=>'menu_blog_index_item'),$_smarty_tpl);?>

</ul>

<?php echo smarty_function_hook(array('run'=>'menu_blog'),$_smarty_tpl);?>

<?php }} ?>
