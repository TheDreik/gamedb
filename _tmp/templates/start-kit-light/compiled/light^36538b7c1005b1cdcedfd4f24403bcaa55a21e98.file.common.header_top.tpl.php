<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:22
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/commons/common.header_top.tpl" */ ?>
<?php /*%%SmartyHeaderCode:182128413755940ed2795615-32952781%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '36538b7c1005b1cdcedfd4f24403bcaa55a21e98' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/commons/common.header_top.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '182128413755940ed2795615-32952781',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'iUserCurrentCountTalkNew' => 0,
    'aLang' => 0,
    'ALTO_SECURITY_KEY' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ed288b682_57432785',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ed288b682_57432785')) {function content_55940ed288b682_57432785($_smarty_tpl) {?><?php if (!is_callable('smarty_function_hook')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.hook.php';
if (!is_callable('smarty_function_asset')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.asset.php';
if (!is_callable('smarty_function_router')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.router.php';
?><header id="header" role="banner">

    <?php echo smarty_function_hook(array('run'=>'header_top_begin'),$_smarty_tpl);?>


    <nav class="navbar navbar-inverse navbar-<?php echo Config::Get('view.header.top');?>
-top">
        <div class="container">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <hgroup class="site-info">
                    <h1 class="site-name"><a class="navbar-brand" href="<?php echo Config::Get('path.root.url');?>
"><?php if (Config::Get('view.header.logo')) {?><img src="<?php echo smarty_function_asset(array('file'=>Config::Get('view.header.logo')),$_smarty_tpl);?>
" alt="<?php echo Config::Get('view.name');?>
" class="navbar-brand-logo"><?php }?><?php if (Config::Get('view.header.name')) {?><?php echo Config::Get('view.header.name');?>
<?php }?></a></h1>
                </hgroup>
            </div>

            <?php echo smarty_function_hook(array('run'=>'userbar_nav'),$_smarty_tpl);?>


            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <?php echo $_smarty_tpl->getSubTemplate ("menus/menu.main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


                <ul class="nav navbar-nav navbar-right">
                    <?php if (E::IsUser()) {?>
                        <?php if ($_smarty_tpl->tpl_vars['iUserCurrentCountTalkNew']->value) {?>
                            <li>
                                <a href="<?php echo smarty_function_router(array('page'=>'talk'),$_smarty_tpl);?>
" class="new-messages" title="<?php if ($_smarty_tpl->tpl_vars['iUserCurrentCountTalkNew']->value) {?><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_privat_messages_new'];?>
<?php }?>">
                                    <span class="glyphicon glyphicon-envelope"></span> +<?php echo $_smarty_tpl->tpl_vars['iUserCurrentCountTalkNew']->value;?>

                                </a>
                            </li>
                        <?php }?>
                        <li class="dropdown nav-userbar">
                            <a data-toggle="dropdown" data-target="#" href="<?php echo E::User()->getProfileUrl();?>
" class="dropdown-toggle username">
                                <img src="<?php echo E::User()->getAvatarUrl(32);?>
" alt="<?php echo E::User()->getDisplayName();?>
" class="avatar"/>
                                <?php echo E::User()->getDisplayName();?>

                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo E::User()->getProfileUrl();?>
"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_menu_profile'];?>
</a>
                                </li>
                                <li><a href="<?php echo smarty_function_router(array('page'=>'talk'),$_smarty_tpl);?>
" id="new_messages"  title="<?php if ($_smarty_tpl->tpl_vars['iUserCurrentCountTalkNew']->value) {?><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_privat_messages_new'];?>
<?php }?>">
                                        <?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_privat_messages'];?>

                                        <?php if ($_smarty_tpl->tpl_vars['iUserCurrentCountTalkNew']->value) {?> <span class="new-messages">+<?php echo $_smarty_tpl->tpl_vars['iUserCurrentCountTalkNew']->value;?>
</span><?php }?></a>
                                </li>
                                <li>
                                    <a href="<?php echo E::User()->getProfileUrl();?>
wall/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_menu_profile_wall'];?>
</a>
                                </li>
                                <li>
                                    <a href="<?php echo E::User()->getProfileUrl();?>
created/topics/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_menu_publication'];?>
</a>
                                </li>
                                <li>
                                    <a href="<?php echo E::User()->getProfileUrl();?>
favourites/topics/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_menu_profile_favourites'];?>
</a>
                                </li>
                                <li>
                                    <a href="<?php echo smarty_function_router(array('page'=>'settings'),$_smarty_tpl);?>
profile/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_settings'];?>
</a>
                                </li>
                                <?php echo smarty_function_hook(array('run'=>'userbar_item'),$_smarty_tpl);?>

                                <li>
                                    <a href="<?php echo smarty_function_router(array('page'=>'login'),$_smarty_tpl);?>
exit/?security_key=<?php echo $_smarty_tpl->tpl_vars['ALTO_SECURITY_KEY']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['exit'];?>
</a>
                                </li>
                            </ul>
                        </li>
                    <?php } else { ?>
                        <?php echo smarty_function_hook(array('run'=>'userbar_item'),$_smarty_tpl);?>

                        <li>
                            <a href="<?php echo smarty_function_router(array('page'=>'login'),$_smarty_tpl);?>
" class="js-modal-auth-login"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_login_submit'];?>
</a>
                        </li>
                        <li class="hidden-sm">
                            <a href="<?php echo smarty_function_router(array('page'=>'registration'),$_smarty_tpl);?>
" class="js-modal-auth-registration"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['registration_submit'];?>
</a>
                        </li>
                    <?php }?>
                </ul>
            </div>

        </div>
    </nav>

    <?php echo smarty_function_hook(array('run'=>'header_top_end'),$_smarty_tpl);?>


</header>
<?php }} ?>
