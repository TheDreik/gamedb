<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:34
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/topics/topic.type_default-edit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8858190155940ede2e7ee6-14725556%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2db0511a42a42f4cab71853d715f39ada3035ebc' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/topics/topic.type_default-edit.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8858190155940ede2e7ee6-14725556',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'sMode' => 0,
    'aLang' => 0,
    'ALTO_SECURITY_KEY' => 0,
    'bPersonalBlog' => 0,
    'aBlogsAllow' => 0,
    'oBlog' => 0,
    '_aRequest' => 0,
    'aEditTopicUrl' => 0,
    'oContentType' => 0,
    'oField' => 0,
    'oTopic' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ede3c4f47_38645908',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ede3c4f47_38645908')) {function content_55940ede3c4f47_38645908($_smarty_tpl) {?><?php if (!is_callable('smarty_function_hook')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.hook.php';
?><?php echo $_smarty_tpl->getSubTemplate ('modals/modal.upload_photoset.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ('commons/common.editor.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php if ($_smarty_tpl->tpl_vars['sMode']->value!='add') {?>
    <div class="page-header">
        <h1><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_topic_edit'];?>
</h1>
    </div>
<?php }?>

<?php echo smarty_function_hook(array('run'=>'add_topic_begin'),$_smarty_tpl);?>


<form action="" method="POST" enctype="multipart/form-data" id="form-topic-add" class="wrapper-content">
    <?php echo smarty_function_hook(array('run'=>'form_add_topic_begin'),$_smarty_tpl);?>


    <input type="hidden" name="security_key" value="<?php echo $_smarty_tpl->tpl_vars['ALTO_SECURITY_KEY']->value;?>
"/>

    <div class="form-group">
        <label for="blog_id"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_blog'];?>
</label>
        <select name="blog_id" id="blog_id" onChange="ls.blog.loadInfo(jQuery(this).val());" class="form-control">
            <?php if ($_smarty_tpl->tpl_vars['bPersonalBlog']->value) {?>
                <option value="0"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_blog_personal'];?>
</option>
            <?php }?>
            <?php  $_smarty_tpl->tpl_vars['oBlog'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oBlog']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aBlogsAllow']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oBlog']->key => $_smarty_tpl->tpl_vars['oBlog']->value) {
$_smarty_tpl->tpl_vars['oBlog']->_loop = true;
?>
                <option value="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getId();?>
"
                        <?php if ($_smarty_tpl->tpl_vars['_aRequest']->value['blog_id']==$_smarty_tpl->tpl_vars['oBlog']->value->getId()) {?>selected<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oBlog']->value->getTitle(), ENT_QUOTES, 'UTF-8', true);?>
</option>
            <?php } ?>
        </select>

        <p class="help-block">
            <small><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_blog_notice'];?>
</small>
        </p>
    </div>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            ls.blog.loadInfo($('#blog_id').val());
        });
    </script>

    <div class="form-group">
        <label for="topic_title"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_title'];?>
</label>
        <input type="text" id="topic_title" name="topic_title" value="<?php echo $_smarty_tpl->tpl_vars['_aRequest']->value['topic_title'];?>
"
               class="form-control"/>

        <p class="help-block">
            <small><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_title_notice'];?>
</small>
        </p>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['sMode']->value!='add'&&E::IsAdmin()) {?>
        <div class="form-group">
            <label for="topic_url"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_url'];?>
:</label><span class="b-topic-url-demo"><?php echo $_smarty_tpl->tpl_vars['aEditTopicUrl']->value['before'];?>
</span><span class="b-topic_url_demo-edit"><?php echo $_smarty_tpl->tpl_vars['aEditTopicUrl']->value['input'];?>
</span><?php if ($_smarty_tpl->tpl_vars['_aRequest']->value['topic_url_input']&&E::IsAdmin()) {?><input type="text" id="topic_url" name="topic_url" value="<?php echo $_smarty_tpl->tpl_vars['_aRequest']->value['topic_url_input'];?>
"class="input-text input-width-300" style="display: none;"/><?php }?><span class="b-topic_url_demo"><?php echo $_smarty_tpl->tpl_vars['aEditTopicUrl']->value['after'];?>
</span>
            <?php if ($_smarty_tpl->tpl_vars['sMode']->value!='add'&&$_smarty_tpl->tpl_vars['_aRequest']->value['topic_url_input']&&E::IsAdmin()) {?>
                <button class="btn btn-default js-tip-help" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_url_edit'];?>
"
                        onclick="ls.topic.editUrl(this); return false;"><i class="glyphicon glyphicon-edit"></i></button>
            <?php }?>
            <button class="btn btn-default js-tip-help" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_url_short'];?>
"
                    onclick="ls.topic.shortUrl('<?php echo $_smarty_tpl->tpl_vars['_aRequest']->value['topic_url_short'];?>
'); return false;"><i
                        class="glyphicon glyphicon-share"></i></button>
            <small class="note"></small>
        </div>
    <?php }?>

    <div class="form-group">
        <label for="topic_text"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_text'];?>
</label>
        <textarea name="topic_text" id="topic_text" rows="20"
                  class="form-control js-editor-wysiwyg js-editor-markitup"><?php echo $_smarty_tpl->tpl_vars['_aRequest']->value['topic_text'];?>
</textarea>

        <?php if (!Config::Get('view.wysiwyg')) {?>
            <?php echo $_smarty_tpl->getSubTemplate ('fields/field.tags_help.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('sTagsTargetId'=>"topic_text"), 0);?>

        <?php }?>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['oContentType']->value->isAllow('link')) {?>
        <?php echo $_smarty_tpl->getSubTemplate ("fields/field.link-edit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['oContentType']->value->isAllow('poll')) {?>
        <?php echo $_smarty_tpl->getSubTemplate ("fields/field.poll-edit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['oContentType']->value->isAllow('photoset')) {?>
        <?php echo $_smarty_tpl->getSubTemplate ("fields/field.photoset-edit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['oContentType']->value) {?>
        <?php  $_smarty_tpl->tpl_vars['oField'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oField']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['oContentType']->value->getFields(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oField']->key => $_smarty_tpl->tpl_vars['oField']->value) {
$_smarty_tpl->tpl_vars['oField']->_loop = true;
?>
            <?php echo $_smarty_tpl->getSubTemplate ("fields/customs/field.custom.".((string)$_smarty_tpl->tpl_vars['oField']->value->getFieldType())."-edit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('oField'=>$_smarty_tpl->tpl_vars['oField']->value), 0);?>

        <?php } ?>
    <?php }?>

    <?php echo $_smarty_tpl->getSubTemplate ("fields/field.tags-edit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    <div class="checkbox">
        <label>
            <input type="checkbox" id="topic_forbid_comment" name="topic_forbid_comment" value="1"
                   <?php if ($_smarty_tpl->tpl_vars['_aRequest']->value['topic_forbid_comment']==1) {?>checked<?php }?> />
            <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_forbid_comment'];?>

        </label>

        <p class="help-block">
            <small><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_forbid_comment_notice'];?>
</small>
        </p>
    </div>

    <?php if (E::IsAdmin()) {?>
        <div class="checkbox">
            <label>
                <input type="checkbox" id="topic_publish_index" name="topic_publish_index" value="1"
                       <?php if ($_smarty_tpl->tpl_vars['_aRequest']->value['topic_publish_index']==1) {?>checked<?php }?> />
                <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_publish_index'];?>

            </label>

            <p class="help-block">
                <small><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_publish_index_notice'];?>
</small>
            </p>
        </div>
    <?php }?>

    <input type="hidden" name="topic_type" value="topic"/>

    <?php echo smarty_function_hook(array('run'=>'form_add_topic_end'),$_smarty_tpl);?>


    <button type="submit" name="submit_topic_publish" class="btn btn-success pull-right">
        <?php if ($_smarty_tpl->tpl_vars['oTopic']->value&&$_smarty_tpl->tpl_vars['oTopic']->value->getPublish()) {?>
            <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_submit_publish_update'];?>

        <?php } else { ?>
            <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_submit_publish'];?>

        <?php }?>
    </button>
    <button type="submit" name="submit_preview" class="btn btn-default js-topic-preview-text-button">
        <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_submit_preview'];?>

    </button>
    <button type="submit" name="submit_topic_draft" class="btn btn-default">
        <?php if ($_smarty_tpl->tpl_vars['oTopic']->value&&$_smarty_tpl->tpl_vars['oTopic']->value->getPublish()) {?>
            <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_submit_publish_draft'];?>

        <?php } else { ?>
            <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_submit_draft'];?>

        <?php }?>
    </button>
</form>
<div class="topic-preview js-topic-preview-place" style="display: none;"></div>

<?php echo smarty_function_hook(array('run'=>'add_topic_end'),$_smarty_tpl);?>


<?php }} ?>
