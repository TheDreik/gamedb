<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:22
         compiled from "/var/www/kolenka/gamedb/common/plugins/categories/templates/skin/default/tpls/widgets/widget.categories.tpl" */ ?>
<?php /*%%SmartyHeaderCode:160755220755940ed2be7e37-68653497%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '52e24776ed9e95922241b09bc1a41ccc7d1a5ff5' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/plugins/categories/templates/skin/default/tpls/widgets/widget.categories.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '160755220755940ed2be7e37-68653497',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'oCurrentCategory' => 0,
    'aLang' => 0,
    'aCategories' => 0,
    'oCategory' => 0,
    'sCategoryUrl' => 0,
    'aBlogs' => 0,
    'aWidgetParams' => 0,
    'oBlog' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ed2c4c597_53835145',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ed2c4c597_53835145')) {function content_55940ed2c4c597_53835145($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['oCurrentCategory']->value) {?>
    <?php $_smarty_tpl->tpl_vars['sCategoryUrl'] = new Smarty_variable($_smarty_tpl->tpl_vars['oCurrentCategory']->value->getCategoryUrl(), null, 0);?>
<?php }?>
<section class="panel panel-default widget widget-type-categories">
    <div class="panel-body">

        <header class="widget-header">
            <h3 class="widget-title"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['plugin']['categories']['categories'];?>
</h3>
        </header>

        <div class="panel-group nav-accordion" id="widget-category-list">
            <?php  $_smarty_tpl->tpl_vars['oCategory'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oCategory']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aCategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oCategory']->key => $_smarty_tpl->tpl_vars['oCategory']->value) {
$_smarty_tpl->tpl_vars['oCategory']->_loop = true;
?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#widget-category-list" href="#widget-category-blogs-<?php echo $_smarty_tpl->tpl_vars['oCategory']->value->getId();?>
">
                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oCategory']->value->getTitle(), ENT_QUOTES, 'UTF-8', true);?>

                        </a>
                    </h4>
                </div>
                <div id="widget-category-blogs-<?php echo $_smarty_tpl->tpl_vars['oCategory']->value->getId();?>
" class="panel-collapse collapse <?php if ($_smarty_tpl->tpl_vars['sCategoryUrl']->value==$_smarty_tpl->tpl_vars['oCategory']->value->getCategoryUrl()) {?>in<?php }?>">
                    <div class="panel-body">
                        <?php $_smarty_tpl->tpl_vars['aBlogs'] = new Smarty_variable($_smarty_tpl->tpl_vars['oCategory']->value->getBlogs(), null, 0);?>
                        <?php if ($_smarty_tpl->tpl_vars['aBlogs']->value) {?>
                            <?php if ($_smarty_tpl->tpl_vars['aWidgetParams']->value['simple']) {?>
                                <ul class="category-blogs-simple">
                                    <?php  $_smarty_tpl->tpl_vars['oBlog'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oBlog']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aBlogs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oBlog']->key => $_smarty_tpl->tpl_vars['oBlog']->value) {
$_smarty_tpl->tpl_vars['oBlog']->_loop = true;
?>
                                        <li>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getUrlFull();?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oBlog']->value->getTitle(), ENT_QUOTES, 'UTF-8', true);?>
</a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } else { ?>
                                <?php echo $_smarty_tpl->getSubTemplate ("widgets/widget.blogs_top.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                            <?php }?>
                        <?php }?>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>

    </div>
</section>

<?php }} ?>
