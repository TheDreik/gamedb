<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:34
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/fields/field.photoset-edit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:110460275255940ede520ec7-91051912%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6ca48da5d1573f134138f9176e92fd5a52cc5cef' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/fields/field.photoset-edit.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '110460275255940ede520ec7-91051912',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    '_aRequest' => 0,
    'aLang' => 0,
    'aPhotos' => 0,
    'oPhoto' => 0,
    'bIsMainPhoto' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ede58cb17_39353528',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ede58cb17_39353528')) {function content_55940ede58cb17_39353528($_smarty_tpl) {?><?php if (!is_callable('smarty_function_json')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.json.php';
if (!is_callable('smarty_modifier_ls_lang')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/modifier.ls_lang.php';
?><script type="text/javascript">
    jQuery(function ($) {
        if (window.swfobject.getFlashPlayerVersion()) {
            ls.photoset.initSwfUpload({
                post_params: {
                    'topic_id': <?php echo smarty_function_json(array('var'=>$_smarty_tpl->tpl_vars['_aRequest']->value['topic_id']),$_smarty_tpl);?>

                }
            });
        }
    });
</script>

<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">
            <a data-toggle="collapse" href="##topic-field-photoset">
                <span class="glyphicon glyphicon-plus-sign"></span>
                <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_toggle_images'];?>

            </a>
        </h5>
    </div>
    <div id="topic-field-photoset" class="panel-collapse collapse <?php if (count($_smarty_tpl->tpl_vars['aPhotos']->value)) {?>in<?php }?>">
        <div class="panel-body topic-photo-upload">
            <div class="small text-muted topic-photo-upload-rules">
                <?php $_smarty_tpl->tpl_vars['nMaxSixe'] = new Smarty_variable(Config::Get('module.topic.photoset.photo_max_size'), null, 0);?>
                <?php $_smarty_tpl->tpl_vars['nMaxCount'] = new Smarty_variable(Config::Get('module.topic.photoset.count_photos_max'), null, 0);?>
                <?php echo smarty_modifier_ls_lang($_smarty_tpl->tpl_vars['aLang']->value['topic_photoset_upload_rules'],"SIZE%%".((string)$_smarty_tpl->tpl_vars['nMaxSixe']->value),"COUNT%%".((string)$_smarty_tpl->tpl_vars['nMaxCount']->value));?>

            </div>

            <input type="hidden" name="topic_main_photo" id="topic_main_photo" value="<?php echo $_smarty_tpl->tpl_vars['_aRequest']->value['topic_main_photo'];?>
"/>

            <ul id="swfu_images" class="small list-unstyled">
                <?php if (count($_smarty_tpl->tpl_vars['aPhotos']->value)) {?>
                    <?php  $_smarty_tpl->tpl_vars['oPhoto'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oPhoto']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aPhotos']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oPhoto']->key => $_smarty_tpl->tpl_vars['oPhoto']->value) {
$_smarty_tpl->tpl_vars['oPhoto']->_loop = true;
?>
                        <?php if ($_smarty_tpl->tpl_vars['_aRequest']->value['topic_main_photo']&&$_smarty_tpl->tpl_vars['_aRequest']->value['topic_main_photo']==$_smarty_tpl->tpl_vars['oPhoto']->value->getId()) {?>
                            <?php $_smarty_tpl->tpl_vars['bIsMainPhoto'] = new Smarty_variable(true, null, 0);?>
                        <?php }?>
                        <li id="photoset_photo_<?php echo $_smarty_tpl->tpl_vars['oPhoto']->value->getId();?>
" <?php if ($_smarty_tpl->tpl_vars['bIsMainPhoto']->value) {?>class="marked-as-preview"<?php }?>>
                            <img src="<?php echo $_smarty_tpl->tpl_vars['oPhoto']->value->getWebPath('100crop');?>
" alt="image"/>
                            <textarea onblur="ls.photoset.setPreviewDescription('<?php echo $_smarty_tpl->tpl_vars['oPhoto']->value->getId();?>
')"
                                      class="form-control"><?php echo $_smarty_tpl->tpl_vars['oPhoto']->value->getDescription();?>
</textarea>
                            <br/>
                            <a href="#"
                               onclick="ls.photoset.deletePhoto('<?php echo $_smarty_tpl->tpl_vars['oPhoto']->value->getId();?>
'); return false;"
                               class="action-image-delete"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_photoset_photo_delete'];?>
</a>
                            <span id="photo_preview_state_<?php echo $_smarty_tpl->tpl_vars['oPhoto']->value->getId();?>
" class="photo-preview-state">
                            <?php if ($_smarty_tpl->tpl_vars['bIsMainPhoto']->value) {?>
                                <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_photoset_is_preview'];?>

                            <?php } else { ?>
                                <a href="#" onclick="ls.photoset.setPreview('<?php echo $_smarty_tpl->tpl_vars['oPhoto']->value->getId();?>
'); return false;"
                                   class="action-image-setpreview"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_photoset_mark_as_preview'];?>
</a>
                            <?php }?>
                            </span>
                        </li>
                        <?php $_smarty_tpl->tpl_vars['bIsMainPhoto'] = new Smarty_variable(false, null, 0);?>
                    <?php } ?>
                <?php }?>
                <li id="photoset_photo_ID" style="display: none;">
                    <img src="" alt="image"/>
                    <textarea onblur="ls.photoset.setPreviewDescription('ID')" class="form-control"></textarea>
                    <br/>
                    <a href="#"
                       onclick="ls.photoset.deletePhoto('ID'); return false;"
                       class="action-image-delete"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_photoset_photo_delete'];?>
</a>
                            <span id="photo_preview_state_ID" class="photo-preview-state">
                            <a href="#" onclick="ls.photoset.setPreview('ID'); return false;"
                               class="action-image-setpreview"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_photoset_mark_as_preview'];?>
</a>
                            </span>
                </li>
                <li class="photoset-upload-progress js-photoset-upload-progress" style="display: none;">
                    <div class="progress progress-striped active">
                        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0;">
                            0%
                        </div>
                    </div>
                    <span class="photoset-upload-filename js-photoset-upload-filename"></span>
                    <span class="photoset-upload-status js-photoset-upload-status"></span>
                </li>
            </ul>

            <div>
                <span id="photoset-upload-place"></span>
                <a href="#" id="photoset-upload-button"
                   onclick="ls.photoset.showForm(); return false;"
                   class="btn btn-default">
                    <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_photoset_upload_choose'];?>

                </a>
            </div>
        </div>
    </div>
</div>

<?php }} ?>
