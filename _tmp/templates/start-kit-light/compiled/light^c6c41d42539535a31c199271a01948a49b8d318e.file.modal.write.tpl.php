<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:22
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/modals/modal.write.tpl" */ ?>
<?php /*%%SmartyHeaderCode:131780152555940ed26ee251-53211358%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c6c41d42539535a31c199271a01948a49b8d318e' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/modals/modal.write.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '131780152555940ed26ee251-53211358',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
    'iUserCurrentCountTopicDraft' => 0,
    'sLang' => 0,
    'aContentTypes' => 0,
    'oContentType' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ed2768028_54601207',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ed2768028_54601207')) {function content_55940ed2768028_54601207($_smarty_tpl) {?><?php if (!is_callable('smarty_function_router')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.router.php';
if (!is_callable('smarty_modifier_declension')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/modifier.declension.php';
if (!is_callable('smarty_function_hook')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.hook.php';
?><div class="modal fade in modal-write" id="modal-write">
    <div class="modal-dialog">
        <div class="modal-content">

            <header class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['block_create'];?>
</h4>
            </header>

            <div class="modal-body"><ul class="list-unstyled list-inline modal-write-list"><?php if ($_smarty_tpl->tpl_vars['iUserCurrentCountTopicDraft']->value) {?><li class="write-item-type-draft"><a href="<?php echo smarty_function_router(array('page'=>'content'),$_smarty_tpl);?>
drafts/" class="write-item-image"></a><a href="<?php echo smarty_function_router(array('page'=>'content'),$_smarty_tpl);?>
drafts/" class="write-item-link"><?php echo $_smarty_tpl->tpl_vars['iUserCurrentCountTopicDraft']->value;?>
 <?php echo smarty_modifier_declension($_smarty_tpl->tpl_vars['iUserCurrentCountTopicDraft']->value,$_smarty_tpl->tpl_vars['aLang']->value['draft_declension'],$_smarty_tpl->tpl_vars['sLang']->value);?>
</a></li><?php }?><?php  $_smarty_tpl->tpl_vars['oContentType'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oContentType']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aContentTypes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oContentType']->key => $_smarty_tpl->tpl_vars['oContentType']->value) {
$_smarty_tpl->tpl_vars['oContentType']->_loop = true;
?><?php if ($_smarty_tpl->tpl_vars['oContentType']->value->isAccessible()) {?><li class="write-item-type-topic"><a href="<?php echo smarty_function_router(array('page'=>'content'),$_smarty_tpl);?>
<?php echo $_smarty_tpl->tpl_vars['oContentType']->value->getContentUrl();?>
/add/" class="write-item-image"></a><a href="<?php echo smarty_function_router(array('page'=>'content'),$_smarty_tpl);?>
<?php echo $_smarty_tpl->tpl_vars['oContentType']->value->getContentUrl();?>
/add/" class="write-item-link"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oContentType']->value->getContentTitle(), ENT_QUOTES, 'UTF-8', true);?>
</a></li><?php }?><?php } ?><li class="write-item-type-blog"><a href="<?php echo smarty_function_router(array('page'=>'blog'),$_smarty_tpl);?>
add" class="write-item-image"></a><a href="<?php echo smarty_function_router(array('page'=>'blog'),$_smarty_tpl);?>
add" class="write-item-link"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['block_create_blog'];?>
</a></li><li class="write-item-type-message"><a href="<?php echo smarty_function_router(array('page'=>'talk'),$_smarty_tpl);?>
add" class="write-item-image"></a><a href="<?php echo smarty_function_router(array('page'=>'talk'),$_smarty_tpl);?>
add" class="write-item-link"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['block_create_talk'];?>
</a></li><?php echo smarty_function_hook(array('run'=>'write_item','isPopup'=>true),$_smarty_tpl);?>
</ul></div>

        </div>
    </div>
</div>
<?php }} ?>
