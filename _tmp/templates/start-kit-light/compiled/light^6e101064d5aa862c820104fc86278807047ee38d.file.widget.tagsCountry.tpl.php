<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:03:53
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/widgets/widget.tagsCountry.tpl" */ ?>
<?php /*%%SmartyHeaderCode:201500593855940f69200892-84491006%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6e101064d5aa862c820104fc86278807047ee38d' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/widgets/widget.tagsCountry.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '201500593855940f69200892-84491006',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aCountryList' => 0,
    'aLang' => 0,
    'oCountry' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940f69240022_18932233',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940f69240022_18932233')) {function content_55940f69240022_18932233($_smarty_tpl) {?><?php if (!is_callable('smarty_function_router')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.router.php';
?><?php if ($_smarty_tpl->tpl_vars['aCountryList']->value&&count($_smarty_tpl->tpl_vars['aCountryList']->value)>0) {?>
    <section class="panel panel-default widget">
        <div class="panel-body">

            <header class="widget-header">
                <h3 class="widget-title"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['widget_country_tags'];?>
</h3>
            </header>

            <div class="widget-content">
                <ul class="list-unstyled list-inline tag-cloud word-wrap">
                    <?php  $_smarty_tpl->tpl_vars['oCountry'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oCountry']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aCountryList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oCountry']->key => $_smarty_tpl->tpl_vars['oCountry']->value) {
$_smarty_tpl->tpl_vars['oCountry']->_loop = true;
?>
                        <li><a class="tag-size-<?php echo $_smarty_tpl->tpl_vars['oCountry']->value->getSize();?>
"
                               href="<?php echo smarty_function_router(array('page'=>'people'),$_smarty_tpl);?>
country/<?php echo $_smarty_tpl->tpl_vars['oCountry']->value->getId();?>
/"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oCountry']->value->getName(), ENT_QUOTES, 'UTF-8', true);?>
</a>
                        </li>
                    <?php } ?>
                </ul>
            </div>

        </div>
    </section>
<?php }?>
<?php }} ?>
