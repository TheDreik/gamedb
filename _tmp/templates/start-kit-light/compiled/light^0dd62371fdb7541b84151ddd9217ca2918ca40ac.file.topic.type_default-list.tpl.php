<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:03:36
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/topics/topic.type_default-list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12429116755940f58b7f228-67775240%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0dd62371fdb7541b84151ddd9217ca2918ca40ac' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/topics/topic.type_default-list.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12429116755940f58b7f228-67775240',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'oTopic' => 0,
    'aLang' => 0,
    'oBlog' => 0,
    'oUser' => 0,
    'oVote' => 0,
    'sVoteClass' => 0,
    'bVoteInfoShow' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940f58f40178_40744148',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940f58f40178_40744148')) {function content_55940f58f40178_40744148($_smarty_tpl) {?><?php if (!is_callable('smarty_function_router')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.router.php';
if (!is_callable('smarty_function_date_format')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.date_format.php';
if (!is_callable('smarty_function_hook')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.hook.php';
if (!is_callable('smarty_block_hookb')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/block.hookb.php';
?><?php $_smarty_tpl->tpl_vars['oBlog'] = new Smarty_variable($_smarty_tpl->tpl_vars['oTopic']->value->getBlog(), null, 0);?>
<?php $_smarty_tpl->tpl_vars['oUser'] = new Smarty_variable($_smarty_tpl->tpl_vars['oTopic']->value->getUser(), null, 0);?>
<?php $_smarty_tpl->tpl_vars['oVote'] = new Smarty_variable($_smarty_tpl->tpl_vars['oTopic']->value->getVote(), null, 0);?>
<article class="topic topic-type-<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getType();?>
 js-topic">
    
        <header class="topic-header">
            <?php if (E::IsUser()&&($_smarty_tpl->tpl_vars['oTopic']->value->CanEditedBy(E::User())||$_smarty_tpl->tpl_vars['oTopic']->value->CanDeletedBy(E::User()))) {?>
                <ul class="list-unstyled list-inline small pull-right actions">
                    <li><span class="glyphicon glyphicon-cog actions-tool"></span></li>
                    <?php if ($_smarty_tpl->tpl_vars['oTopic']->value->CanEditedBy(E::User())) {?>
                        <li>
                            <a href="<?php echo smarty_function_router(array('page'=>'content'),$_smarty_tpl);?>
edit/<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getId();?>
/" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_edit'];?>
" class="actions-edit">
                                <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_edit'];?>

                            </a>
                        </li>
                    <?php }?>

                    <?php if ($_smarty_tpl->tpl_vars['oTopic']->value->CanDeletedBy(E::User())) {?>
                        <li>
                            <a href="#" class="actions-delete" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_delete'];?>
"
                               onclick="ls.topic.remove('<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getId();?>
', '<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getTitle();?>
'); return false;">
                                <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_delete'];?>

                            </a>
                        </li>
                    <?php }?>
                </ul>
            <?php }?>

            <h2 class="topic-header-title">
                <a href="<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getUrl();?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oTopic']->value->getTitle(), ENT_QUOTES, 'UTF-8', true);?>
</a>

                <?php if ($_smarty_tpl->tpl_vars['oTopic']->value->getPublish()==0) {?>
                    <span class="glyphicon glyphicon-file text-muted" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_unpublish'];?>
"></span>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['oTopic']->value->getType()=='link') {?>
                    <span class="glyphicon glyphicon-globe text-muted" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_link'];?>
"></span>
                <?php }?>
            </h2>

            <div class="topic-header-info">
                <a href="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getUrlFull();?>
" class="topic-blog"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oBlog']->value->getTitle(), ENT_QUOTES, 'UTF-8', true);?>
</a>

                <time datetime="<?php echo smarty_function_date_format(array('date'=>$_smarty_tpl->tpl_vars['oTopic']->value->getDate(),'format'=>'c'),$_smarty_tpl);?>
"
                      title="<?php echo smarty_function_date_format(array('date'=>$_smarty_tpl->tpl_vars['oTopic']->value->getDate(),'format'=>'j F Y, H:i'),$_smarty_tpl);?>
" class="topic-info-date">
                    <?php echo smarty_function_date_format(array('date'=>$_smarty_tpl->tpl_vars['oTopic']->value->getDate(),'hours_back'=>"12",'minutes_back'=>"60",'now'=>"60",'day'=>"day H:i",'format'=>"j F Y, H:i"),$_smarty_tpl);?>

                </time>
            </div>
        </header>
    

    
        <div class="topic-content text">
            <?php echo smarty_function_hook(array('run'=>'topic_content_begin','topic'=>$_smarty_tpl->tpl_vars['oTopic']->value,'bTopicList'=>true),$_smarty_tpl);?>


            <?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getTextShort();?>


            <div class="clearfix"></div>

            <?php if ($_smarty_tpl->tpl_vars['oTopic']->value->getTextShort()!=$_smarty_tpl->tpl_vars['oTopic']->value->getText()) {?>
                <br/>
                <a href="<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getUrl();?>
#cut" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_read_more'];?>
" class="read-more">
                    <?php if ($_smarty_tpl->tpl_vars['oTopic']->value->getCutText()) {?>
                        <?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getCutText();?>
...
                    <?php } else { ?>
                        <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_read_more'];?>
...
                    <?php }?>
                </a>
            <?php }?>

            <?php echo smarty_function_hook(array('run'=>'topic_content_end','topic'=>$_smarty_tpl->tpl_vars['oTopic']->value,'bTopicList'=>true),$_smarty_tpl);?>

        </div>
    

    
        <?php $_smarty_tpl->tpl_vars['oBlog'] = new Smarty_variable($_smarty_tpl->tpl_vars['oTopic']->value->getBlog(), null, 0);?>
        <?php $_smarty_tpl->tpl_vars['oUser'] = new Smarty_variable($_smarty_tpl->tpl_vars['oTopic']->value->getUser(), null, 0);?>
        <?php $_smarty_tpl->tpl_vars['oVote'] = new Smarty_variable($_smarty_tpl->tpl_vars['oTopic']->value->getVote(), null, 0);?>
        <?php $_smarty_tpl->tpl_vars['oFavourite'] = new Smarty_variable($_smarty_tpl->tpl_vars['oTopic']->value->getFavourite(), null, 0);?>
        <footer class="topic-footer">
            <?php echo $_smarty_tpl->getSubTemplate ("fields/field.tags-list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


            <div class="topic-share" id="topic_share_<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getId();?>
">
                <?php $_smarty_tpl->smarty->_tag_stack[] = array('hookb', array('run'=>"topic_share",'topic'=>$_smarty_tpl->tpl_vars['oTopic']->value,'bTopicList'=>true)); $_block_repeat=true; echo smarty_block_hookb(array('run'=>"topic_share",'topic'=>$_smarty_tpl->tpl_vars['oTopic']->value,'bTopicList'=>true), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                    <div class="yashare-auto-init" data-yashareTitle="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oTopic']->value->getTitle(), ENT_QUOTES, 'UTF-8', true);?>
"
                         data-yashareLink="<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getUrl();?>
" data-yashareL10n="ru" data-yashareType="none"
                         data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,gplus"></div>
                <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hookb(array('run'=>"topic_share",'topic'=>$_smarty_tpl->tpl_vars['oTopic']->value,'bTopicList'=>true), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            </div>

            <ul class="list-unstyled list-inline small topic-footer-info">
                <li class="topic-info-author">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['oUser']->value->getProfileUrl();?>
" class="avatar js-popup-<?php echo $_smarty_tpl->tpl_vars['oUser']->value->getId();?>
">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['oUser']->value->getAvatarUrl(24);?>
" alt="<?php echo $_smarty_tpl->tpl_vars['oUser']->value->getDisplayName();?>
" />
                    </a>
                    <a rel="author" href="<?php echo $_smarty_tpl->tpl_vars['oUser']->value->getProfileUrl();?>
"><?php echo $_smarty_tpl->tpl_vars['oUser']->value->getDisplayName();?>
</a>
                </li>
                <li class="topic-info-favourite">
                    <a href="#" onclick="return ls.favourite.toggle(<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getId();?>
,this,'topic');"
                       class="favourite <?php if (E::IsUser()&&$_smarty_tpl->tpl_vars['oTopic']->value->getIsFavourite()) {?>active<?php }?>"><span
                                class="glyphicon glyphicon-star"></span></a>
                    <span class="text-muted favourite-count"
                          id="fav_count_topic_<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getId();?>
"><?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getCountFavourite();?>
</span>
                </li>
                <li class="topic-info-share"><a href="#" class="glyphicon glyphicon-share-alt"
                                                title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_share'];?>
"
                                                onclick="jQuery('#topic_share_' + '<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getId();?>
').slideToggle(); return false;"></a>
                </li>

                <li class="topic-info-comments">
                    <?php if ($_smarty_tpl->tpl_vars['oTopic']->value->getCountCommentNew()) {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getUrl();?>
#comments" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_comment_read'];?>
" class="new">
                            <span class="glyphicon glyphicon-comment icon-active"></span>
                            <span><?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getCountComment();?>
</span>
                            <span class="count">+<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getCountCommentNew();?>
</span>
                        </a>
                    <?php } elseif ($_smarty_tpl->tpl_vars['oTopic']->value->getCountComment()) {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getUrl();?>
#comments" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_comment_read'];?>
"
                           class="icon-active">
                            <span class="glyphicon glyphicon-comment"></span>
                            <span><?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getCountComment();?>
</span>
                        </a>
                    <?php } else { ?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getUrl();?>
#comments" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_comment_read'];?>
">
                            <span class="glyphicon glyphicon-comment"></span>
                            <span><?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getCountComment();?>
</span>
                        </a>
                    <?php }?>
                </li>

                <?php $_smarty_tpl->tpl_vars['sVoteClass'] = new Smarty_variable('', null, 0);?>
                <?php if ($_smarty_tpl->tpl_vars['oVote']->value||E::UserId()==$_smarty_tpl->tpl_vars['oTopic']->value->getUserId()||strtotime($_smarty_tpl->tpl_vars['oTopic']->value->getDateAdd())<time()-Config::Get('acl.vote.topic.limit_time')) {?>
                    <?php if ($_smarty_tpl->tpl_vars['oTopic']->value->getRating()>0) {?>
                        <?php $_smarty_tpl->tpl_vars['sVoteClass'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['sVoteClass']->value)." vote-count-positive", null, 0);?>
                    <?php } elseif ($_smarty_tpl->tpl_vars['oTopic']->value->getRating()<0) {?>
                        <?php $_smarty_tpl->tpl_vars['sVoteClass'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['sVoteClass']->value)." vote-count-negative", null, 0);?>
                    <?php }?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['oVote']->value) {?>
                    <?php $_smarty_tpl->tpl_vars['sVoteClass'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['sVoteClass']->value)." voted", null, 0);?>
                    <?php if ($_smarty_tpl->tpl_vars['oVote']->value->getDirection()>0) {?>
                        <?php $_smarty_tpl->tpl_vars['sVoteClass'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['sVoteClass']->value)." voted-up", null, 0);?>
                    <?php } elseif ($_smarty_tpl->tpl_vars['oVote']->value->getDirection()<0) {?>
                        <?php $_smarty_tpl->tpl_vars['sVoteClass'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['sVoteClass']->value)." voted-down", null, 0);?>
                    <?php }?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['oTopic']->value->isVoteInfoShow()) {?>
                    <?php $_smarty_tpl->tpl_vars['bVoteInfoShow'] = new Smarty_variable(true, null, 0);?>
                <?php }?>
                <?php $_smarty_tpl->tpl_vars['bVoteInfoShow'] = new Smarty_variable(true, null, 0);?>
                <li class="pull-right vote js-vote <?php echo $_smarty_tpl->tpl_vars['sVoteClass']->value;?>
" data-target-type="topic" data-target-id="<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getId();?>
">
                    <div class="vote-up js-vote-up"><span class="glyphicon glyphicon-plus-sign"></span></div>
                    <div id="vote_total_topic_<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getId();?>
" class="vote-count js-vote-rating <?php if ($_smarty_tpl->tpl_vars['bVoteInfoShow']->value) {?>js-infobox-vote-topic<?php }?>"
                         title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_vote_count'];?>
: <?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getCountVote();?>
">
                        <?php if ($_smarty_tpl->tpl_vars['bVoteInfoShow']->value) {?>
                            <?php if ($_smarty_tpl->tpl_vars['oTopic']->value->getRating()>0) {?>+<?php }?><?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getRating();?>

                        <?php } else { ?>
                            <a href="#" onclick="return ls.vote.vote(<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getId();?>
,this,0,'topic');">?</a>
                        <?php }?>
                    </div>
                    <div class="vote-down js-vote-down"><span class="glyphicon glyphicon-minus-sign"></span></div>
                    <?php if ($_smarty_tpl->tpl_vars['bVoteInfoShow']->value) {?>
                        <div id="vote-info-topic-<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getId();?>
" style="display: none;">
                            <ul class="list-unstyled vote-topic-info">
                                <li>
                                    <span class="glyphicon glyphicon-thumbs-up"></span><?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getCountVoteUp();?>

                                </li>
                                <li>
                                    <span class="glyphicon glyphicon-thumbs-down"></span><?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getCountVoteDown();?>

                                </li>
                                <li>
                                    <span class="glyphicon glyphicon-eye-open"></span><?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getCountVoteAbstain();?>

                                </li>
                                <?php echo smarty_function_hook(array('run'=>'topic_show_vote_stats','topic'=>$_smarty_tpl->tpl_vars['oTopic']->value),$_smarty_tpl);?>

                            </ul>
                        </div>
                    <?php }?>
                </li>

                <?php echo smarty_function_hook(array('run'=>'topic_show_info','topic'=>$_smarty_tpl->tpl_vars['oTopic']->value),$_smarty_tpl);?>

            </ul>

        </footer>
    
</article> <!-- /.topic -->
<?php }} ?>
