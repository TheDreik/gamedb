<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:27
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/widgets/widget.tags.tpl" */ ?>
<?php /*%%SmartyHeaderCode:203159154355940ed7783482-39435269%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9c36b9d2f4bb45e955abf4dab3ad4b0f8cad43b3' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/widgets/widget.tags.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '203159154355940ed7783482-39435269',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
    'aTags' => 0,
    'oTag' => 0,
    'aTagsUser' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ed7826222_21576684',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ed7826222_21576684')) {function content_55940ed7826222_21576684($_smarty_tpl) {?><?php if (!is_callable('smarty_function_hook')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.hook.php';
if (!is_callable('smarty_function_router')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.router.php';
?><section class="panel panel-default widget">
    <div class="panel-body">

        <header class="widget-header">
            <h3 class="widget-title"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['widget_tags'];?>
</h3>
        </header>

        <div class="widget-content">
            <?php if (E::IsUser()) {?>
                <ul class="nav nav-pills">
                    <li class="active">
                        <a href="#" data-toggle="tab" data-target=".js-widget-tags-all"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_favourite_tags_block_all'];?>
</a>
                    </li>
                    <li>
                        <a href="#" data-toggle="tab" data-target=".js-widget-tags-user"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_favourite_tags_block_user'];?>
</a>
                    </li>

                    <?php echo smarty_function_hook(array('run'=>'widget_tags_nav_item'),$_smarty_tpl);?>

                </ul>
            <?php }?>

            <div class="tab-content">
                <div class="tab-pane active js-widget-tags-all">
                    <?php if ($_smarty_tpl->tpl_vars['aTags']->value) {?>
                        <ul class="list-unstyled list-inline tag-cloud word-wrap">
                            <?php  $_smarty_tpl->tpl_vars['oTag'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oTag']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aTags']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oTag']->key => $_smarty_tpl->tpl_vars['oTag']->value) {
$_smarty_tpl->tpl_vars['oTag']->_loop = true;
?>
                                <li><a class="tag-size-<?php echo $_smarty_tpl->tpl_vars['oTag']->value->getSize();?>
"
                                       href="<?php echo smarty_function_router(array('page'=>'tag'),$_smarty_tpl);?>
<?php echo rawurlencode($_smarty_tpl->tpl_vars['oTag']->value->getText());?>
/"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oTag']->value->getText(), ENT_QUOTES, 'UTF-8', true);?>
</a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } else { ?>
                        <div class="notice-empty"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['widget_tags_empty'];?>
</div>
                    <?php }?>
                </div>

                <?php if (E::IsUser()) {?>
                    <div class="tab-pane js-widget-tags-user">
                        <?php if ($_smarty_tpl->tpl_vars['aTagsUser']->value) {?>
                            <ul class="list-unstyled list-inline tag-cloud word-wrap">
                                <?php  $_smarty_tpl->tpl_vars['oTag'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oTag']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aTagsUser']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oTag']->key => $_smarty_tpl->tpl_vars['oTag']->value) {
$_smarty_tpl->tpl_vars['oTag']->_loop = true;
?>
                                    <li>
                                        <a class="tag-size-<?php echo $_smarty_tpl->tpl_vars['oTag']->value->getSize();?>
" href="<?php echo smarty_function_router(array('page'=>'tag'),$_smarty_tpl);?>
<?php echo rawurlencode($_smarty_tpl->tpl_vars['oTag']->value->getText());?>
/">
                                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oTag']->value->getText(), ENT_QUOTES, 'UTF-8', true);?>

                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } else { ?>
                            <p class="text-muted"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['widget_tags_empty'];?>
</p>
                        <?php }?>
                    </div>
                <?php }?>
            </div>
        </div>

    </div>
</section>
<?php }} ?>
