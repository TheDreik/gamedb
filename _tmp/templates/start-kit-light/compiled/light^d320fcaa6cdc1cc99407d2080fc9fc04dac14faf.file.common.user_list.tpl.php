<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:03:52
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/commons/common.user_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:154678289255940f68efc648-69321280%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd320fcaa6cdc1cc99407d2080fc9fc04dac14faf' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/commons/common.user_list.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '154678289255940f68efc648-69321280',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'bUsersUseOrder' => 0,
    'sUsersRootPage' => 0,
    'sUsersOrder' => 0,
    'sUsersOrderWayNext' => 0,
    'sUsersOrderWay' => 0,
    'aLang' => 0,
    'aUsersList' => 0,
    'oUserList' => 0,
    'oUserNote' => 0,
    'oSession' => 0,
    'sUserListEmpty' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940f6911fb31_58788941',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940f6911fb31_58788941')) {function content_55940f6911fb31_58788941($_smarty_tpl) {?><?php if (!is_callable('smarty_function_date_format')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.date_format.php';
?><table class="table table-users">
    <?php if ($_smarty_tpl->tpl_vars['bUsersUseOrder']->value) {?>
        <thead>
        <tr>
            <th class="cell-name">
                <small>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['sUsersRootPage']->value;?>
?order=user_login&order_way=<?php if ($_smarty_tpl->tpl_vars['sUsersOrder']->value=='user_login') {?><?php echo $_smarty_tpl->tpl_vars['sUsersOrderWayNext']->value;?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['sUsersOrderWay']->value;?>
<?php }?>"
                       <?php if ($_smarty_tpl->tpl_vars['sUsersOrder']->value=='user_login') {?>class="<?php echo $_smarty_tpl->tpl_vars['sUsersOrderWay']->value;?>
"<?php }?>><span><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user'];?>
</span></a>
                </small>
            </th>
            <th class="cell-date">
                <small><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_date_last'];?>
</small>
            </th>
            <th class="cell-skill">
                <small>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['sUsersRootPage']->value;?>
?order=user_skill&order_way=<?php if ($_smarty_tpl->tpl_vars['sUsersOrder']->value=='user_skill') {?><?php echo $_smarty_tpl->tpl_vars['sUsersOrderWayNext']->value;?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['sUsersOrderWay']->value;?>
<?php }?>"
                       <?php if ($_smarty_tpl->tpl_vars['sUsersOrder']->value=='user_skill') {?>class="<?php echo $_smarty_tpl->tpl_vars['sUsersOrderWay']->value;?>
"<?php }?>><span><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_skill'];?>
</span></a>
                </small>
            </th>
            <th class="cell-rating">
                <small>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['sUsersRootPage']->value;?>
?order=user_rating&order_way=<?php if ($_smarty_tpl->tpl_vars['sUsersOrder']->value=='user_rating') {?><?php echo $_smarty_tpl->tpl_vars['sUsersOrderWayNext']->value;?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['sUsersOrderWay']->value;?>
<?php }?>"
                       <?php if ($_smarty_tpl->tpl_vars['sUsersOrder']->value=='user_rating') {?>class="<?php echo $_smarty_tpl->tpl_vars['sUsersOrderWay']->value;?>
"<?php }?>><span><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_rating'];?>
</span></a>
                </small>
            </th>
        </tr>
        </thead>
    <?php } else { ?>
        <thead>
        <tr>
            <th class="cell-name">
                <small><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user'];?>
</small>
            </th>
            <th class="cell-date">
                <small><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_date_last'];?>
</small>
            </th>
            <th class="cell-skill">
                <small><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_skill'];?>
</small>
            </th>
            <th class="cell-rating">
                <small><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_rating'];?>
</small>
            </th>
        </tr>
        </thead>
    <?php }?>

    <tbody>
    <?php if ($_smarty_tpl->tpl_vars['aUsersList']->value) {?>
        <?php  $_smarty_tpl->tpl_vars['oUserList'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oUserList']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aUsersList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oUserList']->key => $_smarty_tpl->tpl_vars['oUserList']->value) {
$_smarty_tpl->tpl_vars['oUserList']->_loop = true;
?>
            <?php $_smarty_tpl->tpl_vars['oSession'] = new Smarty_variable($_smarty_tpl->tpl_vars['oUserList']->value->getSession(), null, 0);?>
            <?php $_smarty_tpl->tpl_vars['oUserNote'] = new Smarty_variable($_smarty_tpl->tpl_vars['oUserList']->value->getUserNote(), null, 0);?>
            <tr>
                <td class="cell-name">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['oUserList']->value->getProfileUrl();?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['oUserList']->value->getAvatarUrl(48);?>
"
                                                                 alt="<?php echo $_smarty_tpl->tpl_vars['oUserList']->value->getDisplayName();?>
"
                                                                 class="avatar visible-lg"/></a>

                    <div class="name <?php if (!$_smarty_tpl->tpl_vars['oUserList']->value->getProfileName()) {?>no-realname<?php }?>">
                        <p class="username">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['oUserList']->value->getProfileUrl();?>
"><?php echo $_smarty_tpl->tpl_vars['oUserList']->value->getDisplayName();?>
</a>
                            <?php if ($_smarty_tpl->tpl_vars['oUserNote']->value) {?>
                                <span class="glyphicon glyphicon-comment text-muted js-infobox"
                                      title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oUserNote']->value->getText(), ENT_QUOTES, 'UTF-8', true);?>
"></span>
                            <?php }?>
                        </p>
                        <?php if ($_smarty_tpl->tpl_vars['oUserList']->value->getProfileName()) {?>
                            <p class="text-muted realname">
                            <small><?php echo $_smarty_tpl->tpl_vars['oUserList']->value->getProfileName();?>
</small></p><?php }?>
                    </div>
                </td>
                <td class="small text-muted cell-date">
                    <?php if ($_smarty_tpl->tpl_vars['oSession']->value) {?>
                        <?php echo smarty_function_date_format(array('date'=>$_smarty_tpl->tpl_vars['oSession']->value->getDateLast(),'hours_back'=>"12",'minutes_back'=>"60",'now'=>"60",'day'=>"day H:i",'format'=>"d.m.y, H:i"),$_smarty_tpl);?>

                    <?php }?>
                </td>
                <td class="small text-info cell-skill"><?php echo $_smarty_tpl->tpl_vars['oUserList']->value->getSkill();?>
</td>
                <td class="small cell-rating<?php if ($_smarty_tpl->tpl_vars['oUserList']->value->getRating()<0) {?> text-danger negative<?php } else { ?> text-success<?php }?>"><?php echo $_smarty_tpl->tpl_vars['oUserList']->value->getRating();?>
</td>
            </tr>
        <?php } ?>
    <?php } else { ?>
        <tr>
            <td colspan="5">
                <?php if ($_smarty_tpl->tpl_vars['sUserListEmpty']->value) {?>
                    <?php echo $_smarty_tpl->tpl_vars['sUserListEmpty']->value;?>

                <?php } else { ?>
                    <?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_empty'];?>

                <?php }?>
            </td>
        </tr>
    <?php }?>
    </tbody>
</table>
<?php }} ?>
