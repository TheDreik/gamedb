<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:27
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/widgets/widget.blogs_top.tpl" */ ?>
<?php /*%%SmartyHeaderCode:118602046155940ed784c725-99279860%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7fbaede59b505905d767f0ad3c9c8faadce94127' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/widgets/widget.blogs_top.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '118602046155940ed784c725-99279860',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aBlogs' => 0,
    'oBlog' => 0,
    'aLang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ed78868b2_00506663',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ed78868b2_00506663')) {function content_55940ed78868b2_00506663($_smarty_tpl) {?><ul class="list-unstyled item-list">
    <?php  $_smarty_tpl->tpl_vars['oBlog'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oBlog']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aBlogs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oBlog']->key => $_smarty_tpl->tpl_vars['oBlog']->value) {
$_smarty_tpl->tpl_vars['oBlog']->_loop = true;
?>
        <li class="media">
            <a href="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getUrlFull();?>
" class="pull-left">
                <img src="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getAvatarPath(36);?>
" width="36" height="36" class="media-object avatar"/>
            </a>

            <div class="media-body">
                <a href="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getUrlFull();?>
" class="blog-top"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oBlog']->value->getTitle(), ENT_QUOTES, 'UTF-8', true);?>
</a>

                <p class="small text-muted"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_rating'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getRating();?>
</strong></p>
            </div>
        </li>
    <?php } ?>
</ul>
<?php }} ?>
