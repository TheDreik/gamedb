<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:03:37
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/fields/field.tags-list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:76656385855940f59023d18-84283313%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'be5b0a6c0ee1acf23a91edd86fbde8f8768d8971' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/fields/field.tags-list.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
    '7327b8835f08f0e14a05cb0189a0af70479498f3' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/fields/field.tags-show.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '76656385855940f59023d18-84283313',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'oTopic' => 0,
    'oFavourite' => 0,
    'aTags' => 0,
    'aFavouriteTags' => 0,
    'sTag' => 0,
    'aLang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940f590ea4c1_28510820',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940f590ea4c1_28510820')) {function content_55940f590ea4c1_28510820($_smarty_tpl) {?><?php if (!is_callable('smarty_function_router')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.router.php';
?><?php $_smarty_tpl->tpl_vars['aTags'] = new Smarty_variable($_smarty_tpl->tpl_vars['oTopic']->value->getTagsArray(), null, 0);?>
<?php if (E::IsUser()&&$_smarty_tpl->tpl_vars['oFavourite']->value) {?>
    <?php $_smarty_tpl->tpl_vars['aFavouriteTags'] = new Smarty_variable($_smarty_tpl->tpl_vars['oFavourite']->value->getTagsArray(), null, 0);?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['aTags']->value||$_smarty_tpl->tpl_vars['aFavouriteTags']->value) {?>
<ul class="small text-muted list-unstyled list-inline topic-tags js-favourite-insert-after-form js-favourite-tags-topic-<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getId();?>
">
    <li><span class="glyphicon glyphicon-tags"></span></li>

    <?php if ($_smarty_tpl->tpl_vars['aTags']->value) {?><?php  $_smarty_tpl->tpl_vars['sTag'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sTag']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aTags']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['sTag']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['sTag']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['sTag']->key => $_smarty_tpl->tpl_vars['sTag']->value) {
$_smarty_tpl->tpl_vars['sTag']->_loop = true;
 $_smarty_tpl->tpl_vars['sTag']->iteration++;
 $_smarty_tpl->tpl_vars['sTag']->last = $_smarty_tpl->tpl_vars['sTag']->iteration === $_smarty_tpl->tpl_vars['sTag']->total;
?><li><a rel="tag" href="<?php echo smarty_function_router(array('page'=>'tag'),$_smarty_tpl);?>
<?php echo rawurlencode($_smarty_tpl->tpl_vars['sTag']->value);?>
/"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sTag']->value, ENT_QUOTES, 'UTF-8', true);?>
</a><?php if (!$_smarty_tpl->tpl_vars['sTag']->last) {?>, <?php }?></li><?php } ?><?php } else { ?><li><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_tags_empty'];?>
</li><?php }?><?php if (E::IsUser()) {?><?php if ($_smarty_tpl->tpl_vars['aFavouriteTags']->value) {?><?php  $_smarty_tpl->tpl_vars['sTag'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sTag']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aFavouriteTags']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['sTag']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['sTag']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['sTag']->key => $_smarty_tpl->tpl_vars['sTag']->value) {
$_smarty_tpl->tpl_vars['sTag']->_loop = true;
 $_smarty_tpl->tpl_vars['sTag']->iteration++;
 $_smarty_tpl->tpl_vars['sTag']->last = $_smarty_tpl->tpl_vars['sTag']->iteration === $_smarty_tpl->tpl_vars['sTag']->total;
?><li class="topic-tags-user js-favourite-tag-user"><a rel="tag" href="<?php echo E::User()->getProfileUrl();?>
favourites/topics/tag/<?php echo rawurlencode($_smarty_tpl->tpl_vars['sTag']->value);?>
/"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sTag']->value, ENT_QUOTES, 'UTF-8', true);?>
</a><?php if (!$_smarty_tpl->tpl_vars['sTag']->last) {?>, <?php }?></li><?php } ?><?php }?><li class="topic-tags-edit js-favourite-tag-edit" <?php if (!$_smarty_tpl->tpl_vars['oFavourite']->value) {?>style="display:none;"<?php }?>><a href="#" onclick="return ls.favourite.showEditTags(<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getId();?>
,'topic',this);" class="link-dotted"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['favourite_form_tags_button_show'];?>
</a></li><?php }?>
</ul>
<?php }?>
<?php }} ?>
