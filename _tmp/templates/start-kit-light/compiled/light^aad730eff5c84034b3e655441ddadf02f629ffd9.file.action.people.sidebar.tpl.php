<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:03:53
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/actions/people/action.people.sidebar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:115113480855940f6913e606-59909705%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'aad730eff5c84034b3e655441ddadf02f629ffd9' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/actions/people/action.people.sidebar.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '115113480855940f6913e606-59909705',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
    'aStat' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940f691952c2_39809952',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940f691952c2_39809952')) {function content_55940f691952c2_39809952($_smarty_tpl) {?><?php if (!is_callable('smarty_function_hook')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.hook.php';
if (!is_callable('smarty_function_widget')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.widget.php';
?><?php echo smarty_function_hook(array('run'=>'people_sidebar_begin'),$_smarty_tpl);?>


<?php echo smarty_function_widget(array('name'=>'tagsCity'),$_smarty_tpl);?>

<?php echo smarty_function_widget(array('name'=>'tagsCountry'),$_smarty_tpl);?>


<section class="panel panel-default widget">
    <div class="panel-body">

        <header class="widget-header">
            <h3 class="widget-title"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_stats'];?>
</h3>
        </header>

        <div class="widget-content">
            <ul class="list-unstyled">
                <li><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_stats_all'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['aStat']->value['count_all'];?>
</strong></li>
                <li><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_stats_active'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['aStat']->value['count_active'];?>
</strong></li>
                <li><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_stats_noactive'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['aStat']->value['count_inactive'];?>
</strong></li>
            </ul>

            <br/>

            <ul class="list-unstyled">
                <li><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_stats_sex_man'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['aStat']->value['count_sex_man'];?>
</strong></li>
                <li><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_stats_sex_woman'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['aStat']->value['count_sex_woman'];?>
</strong></li>
                <li><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_stats_sex_other'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['aStat']->value['count_sex_other'];?>
</strong></li>
            </ul>
        </div>

    </div>
</section>

<?php echo smarty_function_hook(array('run'=>'people_sidebar_end'),$_smarty_tpl);?>

<?php }} ?>
