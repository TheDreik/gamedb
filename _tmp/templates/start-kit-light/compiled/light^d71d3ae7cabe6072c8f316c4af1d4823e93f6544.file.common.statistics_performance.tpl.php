<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:22
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/commons/common.statistics_performance.tpl" */ ?>
<?php /*%%SmartyHeaderCode:42966875455940ed2cc0e08-82602975%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd71d3ae7cabe6072c8f316c4af1d4823e93f6544' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/commons/common.statistics_performance.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '42966875455940ed2cc0e08-82602975',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'bIsShowStatsPerformance' => 0,
    'aStatsPerformance' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ed2d340e8_12144159',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ed2d340e8_12144159')) {function content_55940ed2d340e8_12144159($_smarty_tpl) {?><?php if (!is_callable('smarty_function_hook')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.hook.php';
?><?php if ($_smarty_tpl->tpl_vars['bIsShowStatsPerformance']->value) {?>
    <div id="stat-performance">
        <div class="container">

            <?php echo smarty_function_hook(array('run'=>'statistics_performance_begin'),$_smarty_tpl);?>


            <table>
                <tr>
                    <td>
                        <h4>Database</h4>
                        query: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['sql']['count'];?>
</strong><br/>
                        time: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['sql']['time'];?>
</strong>
                    </td>
                    <td>
                        <h4>Cache - <?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['cache']['mode'];?>
</h4>
                        query: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['cache']['count'];?>
</strong><br/>
                        &mdash; set: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['cache']['count_set'];?>
</strong><br/>
                        &mdash; get: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['cache']['count_get'];?>
</strong><br/>
                        time: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['cache']['time'];?>
</strong>
                    </td>
                    <td>
                        <h4>Viewer</h4>
                        total time: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['viewer']['total'];?>
</strong><br/>
                        &mdash; preprocess time: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['viewer']['preproc'];?>
</strong><br/>
                        &mdash; render calls: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['viewer']['count'];?>
</strong><br/>
                        &mdash; render time: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['viewer']['time'];?>
</strong><br/>
                    </td>
                    <td>
                        <h4>PHP - <?php echo @constant('PHP_VERSION');?>
</h4>
                        time load modules: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['engine']['time_load_module'];?>
</strong><br/>
                        included files: <br/>
                        &mdash; count: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['engine']['files_count'];?>
</strong><br/>
                        &mdash; time: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['engine']['files_time'];?>
</strong><br/>
                        full time:
                        <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['engine']['full_time'];?>
<?php if ($_smarty_tpl->tpl_vars['aStatsPerformance']->value['engine']['exec_time']) {?> / <?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['engine']['exec_time'];?>
<?php }?></strong>
                    </td>
                    <td>
                        <h4>Memory</h4>
                        memory limit: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['memory']['limit'];?>
</strong><br/>
                        memory usage: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['memory']['usage'];?>
</strong><br/>
                        peak usage: <strong><?php echo $_smarty_tpl->tpl_vars['aStatsPerformance']->value['memory']['peak'];?>
</strong>
                    </td>
                    <?php echo smarty_function_hook(array('run'=>'statistics_performance_item'),$_smarty_tpl);?>

                </tr>
            </table>

            <?php echo smarty_function_hook(array('run'=>'statistics_performance_end'),$_smarty_tpl);?>


        </div>
    </div>
<?php }?>
<?php }} ?>
