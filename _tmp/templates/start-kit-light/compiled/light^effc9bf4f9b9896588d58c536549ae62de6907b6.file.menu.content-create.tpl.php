<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:34
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/menus/menu.content-create.tpl" */ ?>
<?php /*%%SmartyHeaderCode:172208737755940ede1df3c4-51341139%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'effc9bf4f9b9896588d58c536549ae62de6907b6' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/menus/menu.content-create.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '172208737755940ede1df3c4-51341139',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
    'sMenuItemSelect' => 0,
    'aContentTypes' => 0,
    'oContentType' => 0,
    'sMenuSubItemSelect' => 0,
    'iUserCurrentCountTopicDraft' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ede2a61e6_71953016',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ede2a61e6_71953016')) {function content_55940ede2a61e6_71953016($_smarty_tpl) {?><?php if (!is_callable('smarty_function_hook')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.hook.php';
if (!is_callable('smarty_function_router')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.router.php';
?><script type="text/javascript">
    jQuery(function ($) {
        var trigger = $('#dropdown-create-trigger');
        var menu = $('#dropdown-create-menu');
        var pos = trigger.position();

        // Dropdown
        menu.css({ 'left': pos.left - 5 });

        trigger.click(function () {
            menu.slideToggle();
            return false;
        });

        // Hide menu
        $(document).click(function () {
            menu.slideUp();
        });

        $('body').on("click", "#dropdown-create-trigger, #dropdown-create-menu", function (e) {
            e.stopPropagation();
        });
    });
</script>

<div class="dropdown-create">
        <div class="page-header">
            <h1><?php echo $_smarty_tpl->tpl_vars['aLang']->value['block_create'];?>
:
                <a href="#" class="dropdown-create-trigger link-dashed" id="dropdown-create-trigger">
                    <?php if ($_smarty_tpl->tpl_vars['sMenuItemSelect']->value!='blog') {?>
                        <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_menu_add'];?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['sMenuItemSelect']->value=='blog') {?>
                        <?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_create'];?>

                    <?php } else { ?>
                        <?php echo smarty_function_hook(array('run'=>'menu_create_item_select','sMenuItemSelect'=>$_smarty_tpl->tpl_vars['sMenuItemSelect']->value),$_smarty_tpl);?>

                    <?php }?>
                </a>
            </h1>
        </div>

    <ul class="dropdown-menu" id="dropdown-create-menu" style="display: none">
        <li <?php if ($_smarty_tpl->tpl_vars['sMenuItemSelect']->value!='blog') {?>class="active"<?php }?>><a
                    href="<?php echo smarty_function_router(array('page'=>'content'),$_smarty_tpl);?>
add/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_menu_add'];?>
</a></li>
        <li <?php if ($_smarty_tpl->tpl_vars['sMenuItemSelect']->value=='blog') {?>class="active"<?php }?>><a
                    href="<?php echo smarty_function_router(array('page'=>'blog'),$_smarty_tpl);?>
add/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_menu_create'];?>
</a></li>
        <?php echo smarty_function_hook(array('run'=>'menu_create_item','sMenuItemSelect'=>$_smarty_tpl->tpl_vars['sMenuItemSelect']->value),$_smarty_tpl);?>

    </ul>
</div>

<?php if ($_smarty_tpl->tpl_vars['sMenuItemSelect']->value=='topic') {?>
    <ul class="nav nav-pills nav-filter-wrapper">
        <?php  $_smarty_tpl->tpl_vars['oContentType'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oContentType']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aContentTypes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oContentType']->key => $_smarty_tpl->tpl_vars['oContentType']->value) {
$_smarty_tpl->tpl_vars['oContentType']->_loop = true;
?>
            <?php if ($_smarty_tpl->tpl_vars['oContentType']->value->isAccessible()) {?>
                <li <?php if ($_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value==$_smarty_tpl->tpl_vars['oContentType']->value->getContentUrl()) {?>class="active"<?php }?>>
                    <a href="<?php echo smarty_function_router(array('page'=>'content'),$_smarty_tpl);?>
<?php echo $_smarty_tpl->tpl_vars['oContentType']->value->getContentUrl();?>
/add/"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oContentType']->value->getContentTitle(), ENT_QUOTES, 'UTF-8', true);?>
</a>
                </li>
            <?php }?>
        <?php } ?>
        <?php if ($_smarty_tpl->tpl_vars['iUserCurrentCountTopicDraft']->value) {?>
            <li class="pull-right<?php if ($_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value=='drafts') {?> active<?php }?>">
                <a href="<?php echo smarty_function_router(array('page'=>'content'),$_smarty_tpl);?>
drafts/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_menu_drafts'];?>
 (<?php echo $_smarty_tpl->tpl_vars['iUserCurrentCountTopicDraft']->value;?>
)</a>
            </li>
        <?php }?>
        <?php echo smarty_function_hook(array('run'=>'menu_create_topic_item'),$_smarty_tpl);?>

    </ul>
<?php }?>

<?php echo smarty_function_hook(array('run'=>'menu_create','sMenuItemSelect'=>$_smarty_tpl->tpl_vars['sMenuItemSelect']->value,'sMenuSubItemSelect'=>$_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value),$_smarty_tpl);?>

<?php }} ?>
