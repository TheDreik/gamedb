<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:34
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/modals/modal.upload_img.tpl" */ ?>
<?php /*%%SmartyHeaderCode:34558688655940ede3f6f36-67847876%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3ca512c60a2a6a1b11a87bc3293f802831d4adc8' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/modals/modal.upload_img.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '34558688655940ede3f6f36-67847876',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
    'sToLoad' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ede44b0b0_57924809',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ede44b0b0_57924809')) {function content_55940ede44b0b0_57924809($_smarty_tpl) {?><?php if (!is_callable('smarty_function_hook')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.hook.php';
?><div class="modal fade in" id="modal-upload_img">
    <div class="modal-dialog">
        <div class="modal-content">

            <header class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg'];?>
</h4>
            </header>

            <div class="modal-body">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#" data-toggle="tab" data-target=".js-pane-upload_img_pc"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_from_pc'];?>
</a></li>
                    <li>
                        <a href="#" data-toggle="tab" data-target=".js-pane-upload_img_link"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_from_link'];?>
</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active js-pane-upload_img_pc">
                        <form method="POST" action="" enctype="multipart/form-data" id="block_upload_img_content_pc"
                              onsubmit="return false;" class="js-block-upload-img-content">
                            <div class="form-group">
                                <label><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_file'];?>
</label>
                                <br/>
                                <div class="btn btn-default btn-file">
                                    <span><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_choose_file'];?>
</span>
                                    <input type="file" name="img_file" id="img_file" />
                                </div>
                            </div>

                            <?php echo smarty_function_hook(array('run'=>"uploadimg_source"),$_smarty_tpl);?>


                            <div class="row">
                                <div class="form-group col-xs-6">
                                    <label for="form-image-align"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_align'];?>
</label>
                                    <select name="align" id="form-image-align" class="form-control">
                                        <option value=""><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_align_no'];?>
</option>
                                        <option value="left"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_align_left'];?>
</option>
                                        <option value="right"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_align_right'];?>
</option>
                                        <option value="center"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_align_center'];?>
</option>
                                    </select>
                                </div>

                                <div class="form-group col-xs-6 js-img_width">
                                    <label><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_size_width_max'];?>
</label>
                                    <div class="input-group">
                                        <input type="text" name="img_width" value="100" class="form-control"/>
                                        <span class="input-group-addon">%</span>
                                    </div>
                                    <input type="hidden" name="img_width_unit" value="percent" />
                                    <input type="hidden" name="img_width_ref" value="text" />
                                    <input type="hidden" name="img_width_text" value="" />
                                    <p class="help-block"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_size_width_max_text'];?>
</p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="form-image-title"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_title'];?>
</label>
                                <input type="text" name="title" id="form-image-title" value="" class="form-control"/>
                            </div>

                            <?php echo smarty_function_hook(array('run'=>"uploadimg_additional"),$_smarty_tpl);?>


                            <button type="submit" class="btn btn-default"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_cancel'];?>
</button>
                            <button type="submit" class="btn btn-success" onclick="ls.ajaxUploadImg(this,'<?php echo $_smarty_tpl->tpl_vars['sToLoad']->value;?>
');">
                                <?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_submit'];?>

                            </button>
                        </form>
                    </div>

                    <div class="tab-pane js-pane-upload_img_link">
                        <form method="POST" action="" enctype="multipart/form-data" id="block_upload_img_content_link"
                              onsubmit="return false;" class="tab-content js-block-upload-img-content">
                            <div class="form-group">
                                <label for="img_file"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_url'];?>
</label>
                                <input type="text" name="img_url" id="img_url" value="http://" class="form-control"/>
                            </div>

                            <div class="form-group">
                                <label for="form-image-url-align"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_align'];?>
</label>
                                <select name="align" id="form-image-url-align" class="form-control">
                                    <option value=""><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_align_no'];?>
</option>
                                    <option value="left"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_align_left'];?>
</option>
                                    <option value="right"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_align_right'];?>
</option>
                                    <option value="center"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_align_center'];?>
</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="form-image-url-title"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_title'];?>
</label>
                                <input type="text" name="title" id="form-image-url-title" value="" class="form-control"/>
                            </div>

                            <?php echo smarty_function_hook(array('run'=>"uploadimg_link_additional"),$_smarty_tpl);?>


                            <button type="submit" class="btn btn-success" onclick="ls.insertImageToEditor(this);">
                                <?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_link_submit_paste'];?>

                            </button>
                            <?php echo $_smarty_tpl->tpl_vars['aLang']->value['_or'];?>

                            <button type="submit" class="btn btn-success" onclick="ls.ajaxUploadImg(this,'<?php echo $_smarty_tpl->tpl_vars['sToLoad']->value;?>
');">
                                <?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_link_submit_load'];?>

                            </button>
                            <button type="submit" class="btn btn-default"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['uploadimg_cancel'];?>
</button>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<script>
$(function(){
    $('.js-img_width').each(function(){
        var imgWidthGroup = $('.js-img_width'),
            textWidth = imgWidthGroup.closest('.content-inner').width();

        imgWidthGroup.find('[name=img_width_text]').val(textWidth);
    });
});
</script><?php }} ?>
