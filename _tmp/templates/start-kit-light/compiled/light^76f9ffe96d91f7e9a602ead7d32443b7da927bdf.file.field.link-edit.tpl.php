<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:34
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/fields/field.link-edit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:100600302855940ede4affb3-05261624%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '76f9ffe96d91f7e9a602ead7d32443b7da927bdf' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/fields/field.link-edit.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '100600302855940ede4affb3-05261624',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
    '_aRequest' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ede4c0772_30192265',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ede4c0772_30192265')) {function content_55940ede4c0772_30192265($_smarty_tpl) {?><div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">
            <a data-toggle="collapse" href="#topic-field-link">
                <span class="glyphicon glyphicon-plus-sign"></span>
                <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_field_link_add'];?>

            </a>
        </h5>
    </div>
    <div id="topic-field-link" class="panel-collapse collapse <?php if ($_smarty_tpl->tpl_vars['_aRequest']->value['topic_field_link']) {?>in<?php }?>">
        <div class="panel-body form-group">
            <label for="topic-field-link-input"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_field_link_label'];?>
:</label>
            <input type="text" id="topic-field-link-input" name="topic_field_link" value="<?php echo $_smarty_tpl->tpl_vars['_aRequest']->value['topic_field_link'];?>
"
                   class="input-text form-control"/>

            <p class="help-block">
                <small class="note"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_field_link_notice'];?>
</small>
            </p>
        </div>
    </div>
</div>

<?php }} ?>
