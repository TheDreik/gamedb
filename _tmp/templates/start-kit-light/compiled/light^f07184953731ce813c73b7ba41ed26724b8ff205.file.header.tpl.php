<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:09:52
         compiled from "/var/www/kolenka/gamedb/_tmp/templates/start-kit-light/ls-src/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:859021923559410d0bfe859-27703687%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f07184953731ce813c73b7ba41ed26724b8ff205' => 
    array (
      0 => '/var/www/kolenka/gamedb/_tmp/templates/start-kit-light/ls-src/header.tpl',
      1 => 1435766992,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '859021923559410d0bfe859-27703687',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'sHtmlTitle' => 0,
    'sHtmlDescription' => 0,
    'sHtmlKeywords' => 0,
    'aHtmlHeadFiles' => 0,
    'aHtmlRssAlternate' => 0,
    'sHtmlCanonical' => 0,
    'bRefreshToHome' => 0,
    'ALTO_SECURITY_KEY' => 0,
    '_sPhpSessionId' => 0,
    'aRouter' => 0,
    'sPage' => 0,
    'sPath' => 0,
    'aLangJs' => 0,
    'body_classes' => 0,
    'noSidebar' => 0,
    'sidebarPosition' => 0,
    'sMenuItemSelect' => 0,
    'sAction' => 0,
    'sEvent' => 0,
    'aParams' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_559410d0de24a9_58044366',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_559410d0de24a9_58044366')) {function content_559410d0de24a9_58044366($_smarty_tpl) {?><?php if (!is_callable('smarty_function_hook')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.hook.php';
if (!is_callable('smarty_function_asset')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.asset.php';
if (!is_callable('smarty_function_router')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.router.php';
if (!is_callable('smarty_function_json')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.json.php';
if (!is_callable('smarty_function_wgroup')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.wgroup.php';
?><!DOCTYPE html>



<!--[if lt IE 7]>
<html class="no-js ie6 oldie" lang="<?php echo Config::Get('i18n.lang');?>
" dir="<?php echo Config::Get('i18n.dir');?>
"> <![endif]-->
<!--[if IE 7]>
<html class="no-js ie7 oldie" lang="<?php echo Config::Get('i18n.lang');?>
" dir="<?php echo Config::Get('i18n.dir');?>
"> <![endif]-->
<!--[if IE 8]>
<html class="no-js ie8 oldie" lang="<?php echo Config::Get('i18n.lang');?>
" dir="<?php echo Config::Get('i18n.dir');?>
"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="<?php echo Config::Get('i18n.lang');?>
" dir="<?php echo Config::Get('i18n.dir');?>
"> <!--<![endif]-->

<head>

<?php echo smarty_function_hook(array('run'=>'layout_head_begin'),$_smarty_tpl);?>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo $_smarty_tpl->tpl_vars['sHtmlTitle']->value;?>
</title>

    <meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['sHtmlDescription']->value;?>
">
    <meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['sHtmlKeywords']->value;?>
">

    <?php echo $_smarty_tpl->tpl_vars['aHtmlHeadFiles']->value['css'];?>


    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

    <link href="<?php echo smarty_function_asset(array('file'=>"img/favicon.png",'theme'=>true),$_smarty_tpl);?>
?v1" rel="shortcut icon"/>
    <link rel="search" type="application/opensearchdescription+xml" href="<?php echo smarty_function_router(array('page'=>'search'),$_smarty_tpl);?>
opensearch/"
          title="<?php echo Config::Get('view.name');?>
"/>

    <?php if ($_smarty_tpl->tpl_vars['aHtmlRssAlternate']->value) {?>
        <link rel="alternate" type="application/rss+xml" href="<?php echo $_smarty_tpl->tpl_vars['aHtmlRssAlternate']->value['url'];?>
"
              title="<?php echo $_smarty_tpl->tpl_vars['aHtmlRssAlternate']->value['title'];?>
">
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['sHtmlCanonical']->value) {?>
        <link rel="canonical" href="<?php echo $_smarty_tpl->tpl_vars['sHtmlCanonical']->value;?>
"/>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['bRefreshToHome']->value) {?>
        <meta HTTP-EQUIV="Refresh" CONTENT="3; URL=<?php echo Config::Get('path.root.url');?>
/">
    <?php }?>

    <?php echo smarty_function_hook(array('run'=>"html_head_tags"),$_smarty_tpl);?>


    <script type="text/javascript">
        var DIR_WEB_ROOT        = '<?php echo Config::Get('path.root.url');?>
';
        var DIR_STATIC_SKIN     = '<?php echo Config::Get('path.static.skin');?>
';
        var DIR_ROOT_ENGINE_LIB = '<?php echo Config::Get('path.root.engine_lib');?>
';
        var ALTO_SECURITY_KEY   = '<?php echo $_smarty_tpl->tpl_vars['ALTO_SECURITY_KEY']->value;?>
';
        var SESSION_ID          = '<?php echo $_smarty_tpl->tpl_vars['_sPhpSessionId']->value;?>
';


        var tinymce = false;
        var TINYMCE_LANG = <?php if (Config::Get('lang.current')=='ru') {?>'ru'<?php } else { ?>'en'<?php }?>;

        var aRouter = [];
        <?php  $_smarty_tpl->tpl_vars['sPath'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sPath']->_loop = false;
 $_smarty_tpl->tpl_vars['sPage'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['aRouter']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sPath']->key => $_smarty_tpl->tpl_vars['sPath']->value) {
$_smarty_tpl->tpl_vars['sPath']->_loop = true;
 $_smarty_tpl->tpl_vars['sPage']->value = $_smarty_tpl->tpl_vars['sPath']->key;
?>
        aRouter['<?php echo $_smarty_tpl->tpl_vars['sPage']->value;?>
'] = '<?php echo $_smarty_tpl->tpl_vars['sPath']->value;?>
';
        <?php } ?>

    </script>

    <?php echo $_smarty_tpl->tpl_vars['aHtmlHeadFiles']->value['js'];?>


    <script type="text/javascript">
        ls.cfg.wysiwyg = '<?php echo Config::Get('view.wysiwyg');?>
' ? true : false;
        ls.lang.load(<?php echo smarty_function_json(array('var'=>$_smarty_tpl->tpl_vars['aLangJs']->value),$_smarty_tpl);?>
);
        ls.registry.set('comment_max_tree', <?php echo smarty_function_json(array('var'=>Config::Get('module.comment.max_tree')),$_smarty_tpl);?>
);
        ls.registry.set('widget_stream_show_tip', <?php echo smarty_function_json(array('var'=>Config::Get('block.stream.show_tip')),$_smarty_tpl);?>
);
    </script>

    <!--[if lt IE 9]>
    <script src="<?php echo smarty_function_asset(array('file'=>'assets/js/respond.min.js'),$_smarty_tpl);?>
"></script>
    <![endif]-->

    <!--[if IE 7]>
    <link rel="stylesheet" href="<?php echo Config::Get('path.static.skin');?>
/themes/default/icons/css/fontello-ie7.css">
    <![endif]-->
    <script>
        function toggleCodes(on) {
            var obj = document.getElementById('icons');
            if (on) {
                obj.className += ' codesOn';
            } else {
                obj.className = obj.className.replace(' codesOn', '');
            }
        }
    </script>

    <?php if (Config::Get('view.header.top')=='fixed') {?>
        <?php $_smarty_tpl->tpl_vars['body_classes'] = new Smarty_variable(($_smarty_tpl->tpl_vars['body_classes']->value).(' i-fixed_navbar'), null, 0);?>
    <?php }?>

    <?php if (E::IsUser()) {?>
        <?php $_smarty_tpl->tpl_vars['body_classes'] = new Smarty_variable(($_smarty_tpl->tpl_vars['body_classes']->value).(' ls-user-role-user'), null, 0);?>

        <?php if (E::IsAdmin()) {?>
            <?php $_smarty_tpl->tpl_vars['body_classes'] = new Smarty_variable(($_smarty_tpl->tpl_vars['body_classes']->value).(' ls-user-role-admin'), null, 0);?>
        <?php }?>
    <?php } else { ?>
        <?php $_smarty_tpl->tpl_vars['body_classes'] = new Smarty_variable(($_smarty_tpl->tpl_vars['body_classes']->value).(' ls-user-role-guest'), null, 0);?>
    <?php }?>

    <?php if (!E::IsAdmin()) {?>
        <?php $_smarty_tpl->tpl_vars['body_classes'] = new Smarty_variable(($_smarty_tpl->tpl_vars['body_classes']->value).(' ls-user-role-not-admin'), null, 0);?>
    <?php }?>

<?php echo smarty_function_hook(array('run'=>'layout_head_end'),$_smarty_tpl);?>


</head>
<body class="<?php echo $_smarty_tpl->tpl_vars['body_classes']->value;?>
">

<?php echo smarty_function_hook(array('run'=>'layout_body_begin'),$_smarty_tpl);?>


<?php if (E::IsUser()) {?>
    <?php echo $_smarty_tpl->getSubTemplate ('modals/modal.write.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php echo $_smarty_tpl->getSubTemplate ('modals/modal.favourite_tags.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php } else { ?>
    <?php echo $_smarty_tpl->getSubTemplate ('modals/modal.auth.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ('modals/modal.empty.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ('commons/common.header_top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php if (Config::Get('view.header.banner')) {?>
    <?php echo smarty_function_wgroup(array('group'=>"topbanner"),$_smarty_tpl);?>

<?php }?>

<?php echo $_smarty_tpl->getSubTemplate ('commons/common.header_nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<section id="container" class="<?php echo smarty_function_hook(array('run'=>'container_class'),$_smarty_tpl);?>
">
    <div id="wrapper" class="container <?php echo smarty_function_hook(array('run'=>'wrapper_class'),$_smarty_tpl);?>
">
        <div class="row">

            <?php if (!$_smarty_tpl->tpl_vars['noSidebar']->value&&$_smarty_tpl->tpl_vars['sidebarPosition']->value=='left') {?>
                <?php echo $_smarty_tpl->getSubTemplate ('commons/common.sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <?php }?>

            <div id="content" role="main" class="<?php if ($_smarty_tpl->tpl_vars['noSidebar']->value) {?>col-md-12 col-lg-12<?php } else { ?>col-md-9 col-lg-9<?php }?> content<?php if ($_smarty_tpl->tpl_vars['sidebarPosition']->value=='left') {?> content-right<?php }?>"
                 <?php if ($_smarty_tpl->tpl_vars['sMenuItemSelect']->value=='profile') {?>itemscope itemtype="http://data-vocabulary.org/Person"<?php }?>>

                <?php echo $_smarty_tpl->getSubTemplate ('commons/common.messages.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


                <div class="content-inner action-<?php echo $_smarty_tpl->tpl_vars['sAction']->value;?>
<?php if ($_smarty_tpl->tpl_vars['sEvent']->value) {?> event-<?php echo $_smarty_tpl->tpl_vars['sEvent']->value;?>
<?php }?><?php if ($_smarty_tpl->tpl_vars['aParams']->value[0]) {?> params-<?php echo $_smarty_tpl->tpl_vars['aParams']->value[0];?>
<?php }?>">
                    <?php echo $_smarty_tpl->getSubTemplate ('menus/menu.content.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


                    
                    <?php echo smarty_function_hook(array('run'=>'content_begin'),$_smarty_tpl);?>
<?php }} ?>
