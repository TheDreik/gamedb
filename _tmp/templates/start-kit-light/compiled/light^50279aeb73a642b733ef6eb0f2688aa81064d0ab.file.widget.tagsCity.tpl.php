<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:03:53
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/widgets/widget.tagsCity.tpl" */ ?>
<?php /*%%SmartyHeaderCode:79377477355940f691adac8-37271447%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '50279aeb73a642b733ef6eb0f2688aa81064d0ab' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/widgets/widget.tagsCity.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '79377477355940f691adac8-37271447',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aCityList' => 0,
    'aLang' => 0,
    'oCity' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940f691f05c1_69903741',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940f691f05c1_69903741')) {function content_55940f691f05c1_69903741($_smarty_tpl) {?><?php if (!is_callable('smarty_function_router')) include '/var/www/kolenka/gamedb/engine/classes/modules/viewer/plugs/function.router.php';
?><?php if ($_smarty_tpl->tpl_vars['aCityList']->value&&count($_smarty_tpl->tpl_vars['aCityList']->value)>0) {?>
    <section class="panel panel-default widget">
        <div class="panel-body">

            <header class="widget-header">
                <h3 class="widget-title"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['widget_city_tags'];?>
</h3>
            </header>

            <div class="widget-content">
                <ul class="list-unstyled list-inline tag-cloud word-wrap">
                    <?php  $_smarty_tpl->tpl_vars['oCity'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oCity']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aCityList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oCity']->key => $_smarty_tpl->tpl_vars['oCity']->value) {
$_smarty_tpl->tpl_vars['oCity']->_loop = true;
?>
                        <li><a class="tag-size-<?php echo $_smarty_tpl->tpl_vars['oCity']->value->getSize();?>
"
                               href="<?php echo smarty_function_router(array('page'=>'people'),$_smarty_tpl);?>
city/<?php echo $_smarty_tpl->tpl_vars['oCity']->value->getId();?>
/"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oCity']->value->getName(), ENT_QUOTES, 'UTF-8', true);?>
</a>
                        </li>
                    <?php } ?>
                </ul>
            </div>

        </div>
    </section>
<?php }?>
<?php }} ?>
