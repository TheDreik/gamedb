<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:34
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/fields/field.poll-edit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:85890237755940ede4c3412-99778103%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3a80fc9eaabe1546390ebf71e44d43c70d9523a3' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/fields/field.poll-edit.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '85890237755940ede4c3412-99778103',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
    '_aRequest' => 0,
    'bEditDisabled' => 0,
    'sAnswer' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ede51d281_82296380',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ede51d281_82296380')) {function content_55940ede51d281_82296380($_smarty_tpl) {?><div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">
            <a data-toggle="collapse" href="#topic-field-poll">
                <span class="glyphicon glyphicon-plus-sign"></span>
                <?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_field_poll_add'];?>

            </a>
        </h5>
    </div>
    <div id="topic-field-poll" class="panel-collapse collapse <?php if (count($_smarty_tpl->tpl_vars['_aRequest']->value['topic_field_answers'])>0) {?>in<?php }?>">
        <div class="panel-body form-group topic-poll-add js-poll-edit">
            <label><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_question_create_question'];?>
:</label>
            <input type="text" value="<?php echo $_smarty_tpl->tpl_vars['_aRequest']->value['topic_field_question'];?>
" name="topic_field_question"
                   class="form-control" <?php if ($_smarty_tpl->tpl_vars['bEditDisabled']->value) {?>disabled<?php }?> />
            <br/>
            <label><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_question_create_answers'];?>
</label>
            <ul class="list-unstyled topic-poll-add-list js-poll-list">
                <?php if (count($_smarty_tpl->tpl_vars['_aRequest']->value['topic_field_answers'])>=2) {?>
                    <?php  $_smarty_tpl->tpl_vars['sAnswer'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sAnswer']->_loop = false;
 $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['_aRequest']->value['topic_field_answers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sAnswer']->key => $_smarty_tpl->tpl_vars['sAnswer']->value) {
$_smarty_tpl->tpl_vars['sAnswer']->_loop = true;
 $_smarty_tpl->tpl_vars['i']->value = $_smarty_tpl->tpl_vars['sAnswer']->key;
?>
                        <li class="topic-poll-add-item js-poll-item">
                            <input type="text" value="<?php echo $_smarty_tpl->tpl_vars['sAnswer']->value;?>
" name="topic_field_answers[]"
                                   class="form-control" <?php if ($_smarty_tpl->tpl_vars['bEditDisabled']->value) {?>disabled<?php }?> />
                            <?php if (!$_smarty_tpl->tpl_vars['bEditDisabled']->value&&$_smarty_tpl->tpl_vars['i']->value>1) {?>
                                <a href="#" class="glyphicon glyphicon-remove btn-remove js-poll-item-remove" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['delete'];?>
" onclick="return ls.poll.removeItem(this);"></a>
                            <?php }?>
                        </li>
                    <?php } ?>
                <?php } else { ?>
                    <li class="topic-poll-add-item js-poll-item">
                        <input type="text" value="" name="topic_field_answers[]"
                               class="form-control" <?php if ($_smarty_tpl->tpl_vars['bEditDisabled']->value) {?>disabled<?php }?> />
                        <a href="#" class="glyphicon glyphicon-remove js-poll-item-remove" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['delete'];?>
"></a>
                    </li>
                    <li class="topic-poll-add-item js-poll-item">
                        <input type="text" value="" name="topic_field_answers[]"
                               class="form-control" <?php if ($_smarty_tpl->tpl_vars['bEditDisabled']->value) {?>disabled<?php }?> />
                        <a href="#" class="glyphicon glyphicon-remove js-poll-item-remove" title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['delete'];?>
"></a>
                    </li>
                <?php }?>
            </ul>

            <?php if (!$_smarty_tpl->tpl_vars['bEditDisabled']->value) {?>
                <a href="#" class="link-dotted js-poll-add-button"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_question_create_answers_add'];?>
</a>
            <?php }?>
        </div>
    </div>
</div>

<?php }} ?>
