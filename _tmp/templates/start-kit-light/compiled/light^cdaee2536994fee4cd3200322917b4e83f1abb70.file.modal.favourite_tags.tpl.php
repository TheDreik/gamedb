<?php /* Smarty version Smarty-3.1.19, created on 2015-07-01 20:01:22
         compiled from "/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/modals/modal.favourite_tags.tpl" */ ?>
<?php /*%%SmartyHeaderCode:75100086555940ed27707b5-52952329%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cdaee2536994fee4cd3200322917b4e83f1abb70' => 
    array (
      0 => '/var/www/kolenka/gamedb/common/templates/skin/start-kit/tpls/modals/modal.favourite_tags.tpl',
      1 => 1435764810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '75100086555940ed27707b5-52952329',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55940ed2789b09_23532621',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55940ed2789b09_23532621')) {function content_55940ed2789b09_23532621($_smarty_tpl) {?><?php if (E::IsUser()) {?>
    <div class="modal fade in" id="favourite-form-tags">
        <div class="modal-dialog">
            <div class="modal-content">

                <header class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['add_favourite_tags'];?>
</h4>
                </header>

                <form onsubmit="return ls.favourite.saveTags(this);">
                    <div class="modal-body">
                        <input type="hidden" name="target_type" value="" id="favourite-form-tags-target-type">
                        <input type="hidden" name="target_id" value="" id="favourite-form-tags-target-id">

                        <div class="form-group">
                            <input type="text" name="tags" value="" id="favourite-form-tags-tags"
                                   class="form-control autocomplete-tags-sep">
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="submit" name="" class="btn btn-success">
                        <?php echo $_smarty_tpl->tpl_vars['aLang']->value['favourite_form_tags_button_save'];?>
</button>
                        <button type="submit" name="" class="btn btn-default">
                        <?php echo $_smarty_tpl->tpl_vars['aLang']->value['favourite_form_tags_button_cancel'];?>
</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
<?php }?>
<?php }} ?>
