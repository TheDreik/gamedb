{hook run='content_end'}
                    
                </div>
                <!-- /content-inner -->
            </div>
            <!-- /content -->

            {if !$noSidebar AND $sidebarPosition != 'left'}
                {include file='commons/common.sidebar.tpl'}
            {/if}

        </div>
        <!-- /row -->
    </div>
    <!-- /wrapper -->
</section>
<!-- /container -->

<footer id="footer" class="footer">


    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3 footer-bottom-copyright" >

                </div>

                <div class="col-sm-3 col-md-3 col-lg-3 text-right pull-right footer-bottom-copyright" >
                    2014-2015, На коленке by <span style="color:orange;">F</span><span style="color:cyan;">I</span>ce
                </div>
            </div>
        </div>
    </div>
</footer>

{include file='commons/common.toolbar.tpl'}

{hook run='layout_body_end'}

<div id="vs-vote" class="vs-vote" style="display:none;">
        <div class="vs_arrow_bubble_top" style="display:none;"> </div>
        <div class="vs_wrap">
            <div class="vs_close"><a href="#" onclick="ls.voter.closeDiv(); return false"></a></div>
            <div class="vs_header"></div>
                <a href="#" onclick="ls.voter.prevPage(); return false;" class="vs_user_prev"> </a>
                <div class="vs_plus_users"></div>
                <div class="vs_minus_users"></div>
                <a href="#" onclick="ls.voter.nextPage(); return false;" class="vs_user_next"> </a>
            <div class="vs_paginator bg">
                <div class="vs_pag_inner_1">
                    <div class="vs_pag_inner_2">
                        <a href="#" class="active"> </a>
                        <a href="#"> </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="vs_arrow_bubble_bottom"> </div>
</div>       
{literal}
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter27735843 = new Ya.Metrika({id:27735843,
                    webvisor:false});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/27735843" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
{/literal}
</body>
</html>
