<?php
/*-------------------------------------------------------
*
*	Plugin "Voter"
*	Author: Lapenok Nikolai (Klaus)
*	Site: virtualsports.ru
*	Contact e-mail: reverty@gmail.com
*
---------------------------------------------------------
*/
/**
 * Запрещаем напрямую через браузер обращение к этому файлу.
 */
if (!class_exists('Plugin')) {
	die('Hacking attemp!');
}

class PluginVoter extends Plugin {


    // Объявление делегирований (нужны для того, чтобы назначить свои экшны и шаблоны)
	public $aDelegates = array(
            
    );

	// Объявление переопределений (модули, мапперы и сущности)
	protected $aInherits=array(
	  'action' => array('ActionAjax')	   
    );

	// Активация плагина
	public function Activate() { 
		return true;
	}
    
	// Деактивация плагина
	public function Deactivate(){        
		return true;	 
    }


	// Инициализация плагина
	public function Init() {
        $this->Viewer_AppendScript(Plugin::GetTemplateWebPath(__CLASS__).'js/voter.js');
		$this->Viewer_AppendStyle(Plugin::GetTemplateWebPath(__CLASS__).'css/voter.css');
	}

   
}
?>