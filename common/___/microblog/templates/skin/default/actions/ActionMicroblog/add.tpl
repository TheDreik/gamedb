{assign var="noSidebar" value=false}
{include file='header.tpl'}

<form action="/microblog/add" method="POST">
	<input type="hidden" name="security_ls_key" value="{$LIVESTREET_SECURITY_KEY}" />

	<p><label for="text">{$aLang.plugin.microblog.create_text}:</label>
	<input type="text" id="text" class="input-text input-width-full" name="text" class="input-wide" />	</p>

	<p>
		<button type="submit" class="button button-primary" name="submit_microblog">{$aLang.plugin.microblog.create_submit_save}</button>
	</p>

</form>

{include file='footer.tpl'}