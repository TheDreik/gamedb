{include file='header.tpl'}

{block name="layout_vars"}
    {$menu="topics"}
{/block}

<div id="comments" class="comments">

<form class="wall-submit" action="/microblog/add" method="POST">
            <div class="form-group">
                <label for="wall-text">Добавить пост</label>
                <input type="hidden" name="security_ls_key" value="{$LIVESTREET_SECURITY_KEY}" />
                <textarea class="form-control js-wall-reply-parent-text" name="text" id="text" rows="4"></textarea>
            </div>

            <button class="btn btn-success js-button-wall-submit" name="submit_microblog" type="submit">Отправить</button>
        </form>
<!--<form action="/microblog/add" method="POST">
	<input type="hidden" name="security_ls_key" value="{$LIVESTREET_SECURITY_KEY}" />
	<textarea id="text" class="input-text input-width-full" name="text" class="input-wide"></textarea>
	<button type="submit" class="btn btn-success js-button-submit" name="submit_microblog">{$aLang.plugin.microblog.create_submit_save}</button>
</form>-->
<!--<a href="/microblog/add/">{$aLang.plugin.microblog.add}</a>-->
<hr>
{foreach from=$aPosts item=oPost} 	
	<div class="comment-wrapper" id="comment-{$oPost->getId()}">
		<section class="comment">
		{if $iCurrentPage != 0}
			<!--<a href="#">{$aLang.plugin.microblog.link}</a>-->
		{/if}
		<a class="comment-avatar js-popup-user-65" href="/profile/{$LS->User_GetUserById($oPost->getUserId())->getLogin()}/">
            <img src="{$LS->User_GetUserById($oPost->getUserId())->getAvatarUrl(64)}" />
        </a>
<ul class="list-unstyled small comment-info">
            <li class="comment-info-author">
                <a href="/profile/{$LS->User_GetUserById($oPost->getUserId())->getLogin()}/">{$LS->User_GetUserById($oPost->getUserId())->getLogin()}</a>
            </li>
            <li class="comment-info-date">
                <a title="Ссылка на комментарий" class="link-dotted" href="#comment3223">
                                    <time datetime="{date_format date=$oPost->getDate() format='c'}">{date_format date=$oPost->getDate() hours_back="12" minutes_back="60" now="60" day="day H:i" format="j F Y, H:i"}</time>
                </a>
            </li>

        </ul>
		{$oPost->getText()}
		<ul class="list-unstyled list-inline small comment-actions">
            <li><a class="reply-link reply-microblog link-dotted" onclick="return false;" href="#">Ответить</a></li>
		</ul>
		<form style="display: none;" name="subpost" method="post" action="/microblog/add/" class="wall-submit wall-submit-reply active">
                <div class="form-group">
                	<input type="hidden" value="{$oPost->getId()}" name="parent_id">
                    <textarea name="text" placeholder="Ответить..." class="form-control js-wall-reply-text" id="wall-reply-text" rows="4"></textarea>
                </div>
                <button class="btn btn-success js-button-subwall-submit" type="button">Отправить</button>
        </form>

		</section>
		{$subPostCount = count($oPost->getChilds())}
		{if $subPostCount > 3}
			<a href="javascript:void(0);" class="display-comments">Показать старые ответы ({$subPostCount-3})</a>
		{/if}
		<div class="second-level">
		{$subPostIndex = 0} 
		{foreach from=$oPost->getChilds() item=oSubPost}
			{$subPostIndex = $subPostIndex+1}
			<div class="comment-wrapper-more-padding"{if ($subPostCount-3 >= $subPostIndex)} style="display:none;"{/if}>
			<section class="comment">
			<a class="comment-avatar js-popup-user-65" href="/profile/{$LS->User_GetUserById($oSubPost->getUserId())->getLogin()}/">
	            <img src="{$LS->User_GetUserById($oSubPost->getUserId())->getAvatarUrl(64)}" />
	        </a>
<ul class="list-unstyled small comment-info">
            <li class="comment-info-author">
                <a href="/profile/{$LS->User_GetUserById($oSubPost->getUserId())->getLogin()}/">{$LS->User_GetUserById($oSubPost->getUserId())->getLogin()}</a>
            </li>
            <li class="comment-info-date">
                    <time datetime="{date_format date=$oSubPost->getDate() format='c'}">{date_format date=$oSubPost->getDate() hours_back="12" minutes_back="60" now="60" day="day H:i" format="j F Y, H:i"}</time>
            </li>

        </ul>
			{$oSubPost->getText()}
			
			</section>
			</div>
		{/foreach}

		</div>
		{if $iCurrentPage != 0}
			{if count($oPost->getChilds()) >= 10}
				<a href="/microblog/post/{$oPost->getId()}">{$aLang.plugin.microblog.see_post}</a>
			{/if}
		{/if}
			
		
	</div>
	<hr>
{/foreach}	

{if $iPageCount > 1}
	<div class="paging">
        <ul class="pagination">
		{for $i=1 to $iPageCount}
			{if $i eq $iCurrentPage}
				<li class="active"><span>{$i}</span></li>
			{else}
				<li><a href="/microblog/page/{$i}">{$i}</a></li>
			{/if}
		{/for}
		</ul>
	</div>
{/if}
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$(".reply-microblog").click(function() {
			$(this).parent().parent().parent().children("form").show();
		});
        $(".wall-submit textarea#text, .comment-wrapper form textarea").charCount({
            allowed: 250,
            warning: 1
        });
        $(".wall-submit textarea#text, .comment-wrapper form textarea").bind('input propertychange', function() {
            //~ console.log($(this)[0].textLength);
            if ($(this)[0].textLength > 250){
                $(this).val($(this).val().slice(0, 250));
            }
        });
        $(".js-button-subwall-submit").click(function() {
        	var dText, dParentId, urlGet;
        	urlGet = $(location).attr('pathname').replace("add", "");
        	dParentId = $(this).parent().find("input[name='parent_id']").val();
        	dText = $(this).parent().find("textarea[name='text']").val();
        	$(this).parent().find("textarea[name='text']").val("");
        	$(this).parent().hide();
        	$("#comment-"+dParentId+" .second-level").prepend("Отправка ответа...");
			$.ajax({
			type: "POST",
			url: "/microblog/add",
			data: {
				"parent_id":dParentId,
				"text":dText,
			},
			success: function(data) {
				$.get( urlGet, function( data ) {
					//var comment = $("#comments .comment-wrapper:first-child", data);
					//$("#comments .comment-wrapper:first-child").prepend(comment);
					$("#comment-"+dParentId+" .second-level").html($("#comment-"+dParentId+" .second-level", data).html());

				});
			}
			});
        });
        $(".display-comments").click(function() {
        	$(this).parent().find(".comment-wrapper-more-padding").show();
        	$(this).remove();
        });
        /*$(".wall-submit .js-button-wall-submit").click(function() {
        	var dText, dSec;
        	dSec = $(".wall-submit input[name='security_ls_key']").val();
        	dText = $(".wall-submit #text").val();
        	dSub = $(this).val();
			$.ajax({
			type: "POST",
			url: "/microblog/add",
			data: {
				"security_ls_key":dSec,
				"text":dText,
			},
			success: function(data) {
				$.get( "/microblog/", function( data ) {
					//var comment = $("#comments .comment-wrapper:first-child", data);
					//$("#comments .comment-wrapper:first-child").prepend(comment);
					$("#comments").html($("#comments", data).html());
				});
			}
			});
        });*/
	});
</script>
{include file='footer.tpl'}
