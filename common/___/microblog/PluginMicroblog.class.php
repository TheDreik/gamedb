<?php
/**
 * Запрещаем напрямую через браузер обращение к этому файлу.
 */
if (!class_exists('Plugin')) {
	die('Hacking attempt!');
}

class PluginMicroblog extends Plugin {

	protected $aInherits = array(
		'module' => array(
			//'PluginSitemap_ModuleSitemap' => 'PluginPage_ModuleSitemap',
		),
	);


	/**
	 * Активация плагина.
	 * Создание таблицы в базе данных при ее отсутствии.
	 */
	public function Activate() {
		if (!$this->isTableExists('prefix_microblog')) {
			/**
			 * При активации выполняем SQL дамп
			 */
			$this->ExportSQL(dirname(__FILE__).'/dump.sql');
		}
		return true;
	}

	public function Init() {
		parent::Init();
	}
}
?>