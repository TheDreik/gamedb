<?php

class PluginMicroblog_ActionMicroblog extends ActionPlugin {
	
	public function Init() {
	}
	/**
	 * Регистрируем евенты
	 *
	 */
	protected function RegisterEvent() {
		$this->AddEventPreg('/^page/i', '/^(\d+)/i','EventMain');
		$this->AddEventPreg('/^post/i', '/^(\d+)/i','EventSinglePost');
		$this->AddEvent('','EventMain');
		$this->AddEvent('add','EventAdd');
	}


	/**********************************************************************************
	 ************************ РЕАЛИЗАЦИЯ ЭКШЕНА ***************************************
	 **********************************************************************************
	 */

	 protected function EventMain() {
	 	$params = $this->getParams();
	 	if (count($this->getParams())) $iCurrentPage = $params[0]-1; else $iCurrentPage = 0;
		//$iCurrentPage = count($this->getParams()) ? $this->getParams()[0]-1 : 0;
		$aPosts=$this->PluginMicroblog_Microblog_GetMicroblogs(($iCurrentPage * 10));
		$this->Viewer_Assign('aPosts',$aPosts);
		$this->Viewer_Assign('iCurrentPage', $iCurrentPage+1);
		$this->Viewer_Assign('iPageCount', $this->PluginMicroblog_Microblog_GetCount() / 10);
		$this->SetTemplateAction('main');
	}
	
	protected function EventSinglePost() {
		if (!count($this->getParams())){
			return $this->EventNotFound();
		}
		$params = $this->getParams();
		$aPosts=$this->PluginMicroblog_Microblog_GetMicroblog($params[0]);
		$this->Viewer_Assign('aPosts', $aPosts);
		$this->Viewer_Assign('iCurrentPage', 0);
		$this->Viewer_Assign('iPageCount', 0);
		$this->SetTemplateAction('main');
	}
	
	protected function EventAdd(){
		// TODO не работает редирект на главную

		$this->SubmitMicroblog();
		header('location:/microblog/');
	}
	
	protected function SubmitMicroblog(){
		/**
		 * Проверяем корректность полей
		 *\/
		if (!$this->CheckPageFields()) {
			return ;
		}
		/**
		 * Заполняем свойства
		 */
		
		$oPost=Engine::GetEntity('PluginMicroblog_Microblog');
		$oPost->setDate(date("Y-m-d H:i:s"));

// The Regular Expression filter
$reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";


// Check if there is a url in the text
if(preg_match($reg_exUrl, getRequest('text'), $url)) {

       // make the urls hyper links
       $text = preg_replace($reg_exUrl, "<a target=\"_blank\" href=\"{$url[0]}\">{$url[0]}</a> ", getRequest('text'));

} else {

       // if no urls in the text just return the text
       $text = getRequest('text');

}

		$oPost->setText($text);
		$oPost->setUserId($this->User_GetUserCurrent()->getId());
		if (getRequest("parent_id")){
			$oPost->setParentId(getRequest("parent_id"));
		}
		
		if ($this->PluginMicroblog_Microblog_AddMicroblog($oPost)) {
			//if (getRequest("parent_id")){
				//$oAction = $this->EventMain();
				//$oAction->setParam(0, getRequest("parent_id"));
				//return $oAction;
				//$this->setParam(0, getRequest("parent_id"));
				//return  $this->EventSinglePost();
				//return Router::GetPath('/post/').getRequest("parent_id").'/';
				//return Router::Action('microblog');
				//return Router::Action('microblog','post',getRequest("parent_id"));
				//return Router::Action('EventSinglePost', Null, array(getRequest("parent_id")));
			//} else {
				
				return $this->EventMain();
			//}
		} else {
			$this->Message_AddError($this->Lang_Get('system_error'));
		}
	}
	
	/**
	 * Проверка полей на корректность
	 *
	 * @return unknown
	 */
	protected function CheckPageFields() {
		$this->Security_ValidateSendForm();

		$bOk=true;
		/**
		 * Проверяем есть ли заголовок топика
		 */
		if (!func_check(getRequest('page_title',null,'post'),'text',2,200)) {
			$this->Message_AddError($this->Lang_Get('plugin.page.create_title_error'),$this->Lang_Get('error'));
			$bOk=false;
		}
		/**
		 * Проверяем есть ли заголовок топика, с заменой всех пробельных символов на "_"
		 */
		$pageUrl=preg_replace("/\s+/",'_',(string)getRequest('page_url',null,'post'));
		$_REQUEST['page_url']=$pageUrl;
		if (!func_check(getRequest('page_url',null,'post'),'login',1,50)) {
			$this->Message_AddError($this->Lang_Get('plugin.page.create_url_error'),$this->Lang_Get('error'));
			$bOk=false;
		}
		/**
		 * Проверяем на счет плохих УРЛов
		 */
		if (in_array(getRequest('page_url',null,'post'),$this->aBadPageUrl)) {
			$this->Message_AddError($this->Lang_Get('plugin.page.create_url_error_bad').' '.join(',',$this->aBadPageUrl),$this->Lang_Get('error'));
			$bOk=false;
		}
		/**
		 * Проверяем есть ли содержание страницы
		 */
		if (!func_check(getRequest('page_text',null,'post'),'text',1,50000)) {
			$this->Message_AddError($this->Lang_Get('plugin.page.create_text_error'),$this->Lang_Get('error'));
			$bOk=false;
		}
		/**
		 * Проверяем страницу в которую хотим вложить
		 */
		if (getRequest('page_pid')!=0 and !($oPageParent=$this->PluginPage_Page_GetPageById(getRequest('page_pid')))) {
			$this->Message_AddError($this->Lang_Get('plugin.page.create_parent_page_error'),$this->Lang_Get('error'));
			$bOk=false;
		}
		/**
		 * Проверяем сортировку
		 */
		if (getRequest('page_sort') and !is_numeric(getRequest('page_sort'))) {
			$this->Message_AddError($this->Lang_Get('plugin.page.create_sort_error'),$this->Lang_Get('error'));
			$bOk=false;
		}
		/**
		 * Выполнение хуков
		 */
		$this->Hook_Run('check_page_fields', array('bOk'=>&$bOk));

		return $bOk;
	}
}
?>