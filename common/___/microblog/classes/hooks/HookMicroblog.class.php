<?php

/**
 * Регистрация хука
 *
 */
class PluginMicroblog_HookMicroblog extends Hook {
	public function RegisterHook() {
		$this->AddHook('template_main_menu_item','Menu');
	}

	public function Menu() {
		// $aBlogs=$this->PluginMicroblog_Microblog_GetMicroblogs();
		// $this->Logger_Debug('Blogs ' + (string)$aBlogs);
		// $this->Viewer_Assign('aMicroblogsMain',$aBlogs);
		return $this->Viewer_Fetch(Plugin::GetTemplatePath(__CLASS__).'main_menu.tpl');
	}
}
?>