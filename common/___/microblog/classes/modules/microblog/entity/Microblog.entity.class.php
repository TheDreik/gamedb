<?php

class PluginMicroblog_ModuleMicroblog_EntityMicroblog extends Entity
{
	private $childs;
	
	public function getId() {
		return $this->_getDataOne('mb_id');
	}
	public function getParentId() {
		return $this->_getDataOne('mb_parent_id');
	}
	public function getUserId() {
		return $this->_getDataOne('mb_user_id');
	}
	public function getText() {
		return $this->_getDataOne('mb_text');
	}
	public function getDate() {
		return $this->_getDataOne('mb_date_add');
	}
	public function getChilds() {
		return $this->childs;
	}


	public function setId($data) {
		$this->_aData['mb_id']=$data;
	}
	public function setParentId($data) {
		$this->_aData['mb_parent_id']=$data;
	}
	public function setUserId($data) {
		$this->_aData['mb_user_id']=$data;
	}
	public function setText($data) {
		$this->_aData['mb_text']=$data;
	}
	public function setDate($data) {
		$this->_aData['mb_date_add']=$data;
	}
	public function setChilds($data) {
		return $this->childs = $data;
	}
}
?>