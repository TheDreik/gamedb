<?php

class PluginMicroblog_ModuleMicroblog_MapperMicroblog extends Mapper {
	
	public function AddPost(PluginMicroblog_ModuleMicroblog_EntityMicroblog $oPost) {
		$sql = "INSERT INTO ".Config::Get('plugin.microblog.table.microblog')." 
			(mb_parent_id,
			mb_user_id,
			mb_text,
			mb_date_add
			)
			VALUES(?, ?, ?, ?)
		";
		if ($oPost->getText() == "") return false;
		if ($iId=$this->oDb->query($sql,$oPost->getParentId(),$oPost->getUserId(),$oPost->getText(),$oPost->getDate()))
		{
			return $iId;
		}
		return false;
	}
	
	public function GetMicroblogs($iSkip) {
		$sql = "SELECT 
					*,					
					mb_id as ARRAY_KEY
				FROM 
					".Config::Get('plugin.microblog.table.microblog')." 
				WHERE
					mb_parent_id is NULL and 1=1
				ORDER by 
					mb_date_add desc
				LIMIT ".$iSkip.", 10;	
					";
		if ($aRows=$this->oDb->select($sql)) {
			return $aRows;
		}
		return null;
	}
	
	public function GetMicroblog($iId) {
		$sql = "SELECT 
					*,					
					mb_id as ARRAY_KEY
				FROM 
					".Config::Get('plugin.microblog.table.microblog')." 
				WHERE
					mb_id=$iId and 1=1
				";
		if ($aRows=$this->oDb->select($sql)) {
			return $aRows;
		}
		return null;
	}
	
	public function GetChildMicroblogs($iId, $iChildCount){
		if ($iChildCount){
			$sql = "(SELECT 
					*,					
					mb_id as ARRAY_KEY
				FROM 
					".Config::Get('plugin.microblog.table.microblog')." 
				WHERE
					mb_parent_id = '$iId' and 
					1=1
				ORDER by
					mb_date_add desc
				LIMIT 
					$iChildCount)
				ORDER by
					mb_date_add asc
				;";
		}else{
			$sql = "SELECT 
					*,					
					mb_id as ARRAY_KEY
				FROM 
					".Config::Get('plugin.microblog.table.microblog')." 
				WHERE
					mb_parent_id = '$iId' and 
					1=1
				ORDER by
					mb_date_add asc
				;";
		}
		if ($aRows=$this->oDb->select($sql)) {
			return $aRows;
		}
		return null;
	}
	
	public function GetCount() {
		$sql = "SELECT count(*) as count FROM ".Config::Get('plugin.microblog.table.microblog')." WHERE mb_parent_id is NULL";
		if ($aRow=$this->oDb->selectRow($sql)) {
			return $aRow['count'];
		}
		return null;
	}
}
?>