<?php
/**
 * Запрещаем напрямую через браузер обращение к этому файлу.
 */
if (!class_exists('Plugin')) {
	die('Hacking attempt!');
}

class PluginMicroblog extends Plugin {

	protected $aInherits = array(
		'module' => array(
		),
	);


	/**
	 * Активация плагина.
	 * Создание таблицы в базе данных при ее отсутствии.
	 */
	public function Activate() {
		if (!$this->isTableExists('prefix_microblog')) {
			/**
			 * При активации выполняем SQL дамп
			 */
			$this->ExportSQL(dirname(__FILE__).'/dump.sql');
			$sql = "ALTER TABLE prefix_microblog MODIFY COLUMN mb_text varchar(1024) NOT NULL;";
			$this->ExportSQLQuery($sql);
		} else {
			$sql = "ALTER TABLE prefix_microblog MODIFY COLUMN mb_text varchar(1024) NOT NULL;";
			$this->ExportSQLQuery($sql);
		}
		return true;
	}

	public function Init() {
		parent::Init();
	}
}
?>
