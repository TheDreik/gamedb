CREATE TABLE IF NOT EXISTS `prefix_microblog` (
  `mb_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mb_parent_id` int(11) unsigned DEFAULT NULL,
  `mb_user_id` int(11) unsigned NOT NULL,
  `mb_text` varchar(1024) NOT NULL,
  `mb_date_add` datetime NOT NULL,
  PRIMARY KEY (`mb_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;
