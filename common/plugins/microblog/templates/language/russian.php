<?php

return array(
	'name' => 'Микроблоги',
	'add' => 'Добавить',
	'create_text' => 'Введите текст',
	'see_post' => 'Просмотреть полностью',
	'link' => 'Ссылка',
	'create_submit_save' => 'Отправить'
);

?>