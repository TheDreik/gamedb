{extends file="_index.tpl"}

{block name="content-body"}
<!-- <script type="text/javascript" src="{$aTemplateWebPathPlugin.banneroid}js/banneroid.js"></script> -->

<div class="b-wbox-header">
    <h3 class="b-wbox-header-title">{$aLang.plugin.gamedb.name}</h3>
</div>

<div class="span6">
    <div class="b-wbox">
        <div class="b-wbox-header">
            <h3 class="b-wbox-header-title">{$aLang.plugin.gamedb.platforms}</h3>
        </div>
        <div class="b-wbox-content">
            <a id="add_platform">{$aLang.plugin.gamedb.add}</a></br>
            {if $aPlatformsList}
            <table class="table table-people table-talk">
                <thead>
                    <tr>
                        <td class="center">{$aLang.plugin.gamedb.field_title}</td>
                        <td class="center">{$aLang.plugin.gamedb.field_options}</td>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$aPlatformsList item=oPlatform}
                    <tr>
                        <td class="center">{$oPlatform->getName()}</td>
                        <td class="center"><a href="javascript:if(confirm('{$aLang.plugin.gamedb.delete}?'))window.location.href='{router page='admin'}gamedb/platform/delete/{$oPlatform->getId()}/';"> <i class="icon icon-trash"></i> </a></td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>
            {else}
            {$aLang.plugin.gamedb.empty}
            {/if}
        </div>
    </div>
</div>

<div class="span6">
    <div class="b-wbox">
        <div class="b-wbox-header">
            <h3 class="b-wbox-header-title">{$aLang.plugin.gamedb.genres}</h3>
        </div>
        <div class="b-wbox-content">
            <a id="add_genre">{$aLang.plugin.gamedb.add}</a></br>
            {if $aGenresList}
            <table class="table table-people table-talk">
                <thead>
                    <tr>
                        <td class="center">{$aLang.plugin.gamedb.field_title}</td>
                        <td class="center">{$aLang.plugin.gamedb.field_options}</td>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$aGenresList item=oGenre}
                    <tr>
                        <td class="center">{$oGenre->getName()}</td>
                        <td class="center"><a href="javascript:if(confirm('{$aLang.plugin.gamedb.delete}?'))window.location.href='{router page='admin'}gamedb/genre/delete/{$oGenre->getId()}/';"> <i class="icon icon-trash"></i> </a></td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>
            {else}
            {$aLang.plugin.gamedb.empty}
            {/if}
        </div>
    </div>
</div>

<div class="span6">
    <div class="b-wbox">
        <div class="b-wbox-header">
            <h3 class="b-wbox-header-title">{$aLang.plugin.gamedb.tags}</h3>
        </div>
        <div class="b-wbox-content">
            <a id="add_tag">{$aLang.plugin.gamedb.add}</a></br>
            {if $aTagsList}
            <table class="table table-people table-talk">
                <thead>
                    <tr>
                        <td class="center">{$aLang.plugin.gamedb.field_title}</td>
                        <td class="center">{$aLang.plugin.gamedb.field_options}</td>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$aTagsList item=oTag}
                    <tr>
                        <td class="center">{$oTag->getName()}</td>
                        <td class="center"><a href="javascript:if(confirm('{$aLang.plugin.gamedb.delete}?'))window.location.href='{router page='admin'}gamedb/tag/delete/{$oTag->getId()}/';"> <i class="icon icon-trash"></i> </a></td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>
            {else}
            {$aLang.plugin.gamedb.empty}
            {/if}
        </div>
    </div>
</div>

<div class="span6">
    <div class="b-wbox">
        <div class="b-wbox-header">
            <h3 class="b-wbox-header-title">{$aLang.plugin.gamedb.engines}</h3>
        </div>
        <div class="b-wbox-content">
            <a id="add_engine">{$aLang.plugin.gamedb.add}</a></br>
            {if $aEnginesList}
            <table class="table table-people table-talk">
                <thead>
                    <tr>
                        <td class="center">{$aLang.plugin.gamedb.field_title}</td>
                        <td class="center">{$aLang.plugin.gamedb.field_options}</td>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$aEnginesList item=oEngine}
                    <tr>
                        <td class="center">{$oEngine->getName()}</td>
                        <td class="center"><a href="javascript:if(confirm('{$aLang.plugin.gamedb.delete}?'))window.location.href='{router page='admin'}gamedb/engine/delete/{$oEngine->getId()}/';"> <i class="icon icon-trash"></i> </a></td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>
            {else}
            {$aLang.plugin.gamedb.empty}
            {/if}
        </div>
    </div>
</div>

{include_once file="modals/modal.add_platform.tpl"}
{include_once file="modals/modal.add_tag.tpl"}
{include_once file="modals/modal.add_genre.tpl"}
{include_once file="modals/modal.add_engine.tpl"}

<script>
	var admin = admin || { };
	(function($) {
		var modalPl = $('#modal_add_platform');
		$('#add_platform').on('click', function() {
			modalPl.modal('show');
		});
		var modalTag = $('#modal_add_tag');
		$('#add_tag').on('click', function() {
			modalTag.modal('show');
		});
		var modalGenre = $('#modal_add_genre');
		$('#add_genre').on('click', function() {
			modalGenre.modal('show');
		});
		var modalEngine = $('#modal_add_engine');
		$('#add_engine').on('click', function() {
			modalEngine.modal('show');
		});
	})(jQuery);

</script>
{/block}