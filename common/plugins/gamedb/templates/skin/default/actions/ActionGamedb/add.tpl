{extends file="_index.tpl"}

{block name="layout_vars"}
{$noSidebar=true}
{/block}
{block name="layout_content"}

<style>
	.form-group label {
		width: 300px;
	}
</style>

<div class="span12">
    <div class="panel panel-default panel-table raised">

        <div class="panel-body">

            <div class="panel-header">
                <h2> {$aLang.plugin.gamedb.add_game} </h2>
            </div>
            <hr/>
            {if $oUserCurrent}
            <form method="POST" action="/gamedb/add/post/" name="gameadd" enctype="multipart/form-data" class="uniform">
                <input type="hidden" name="security_key" value="{$ALTO_SECURITY_KEY}"/>

                <div class="b-wbox">

                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#base">{$aLang.plugin.gamedb.base_info}</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#add_info">{$aLang.plugin.gamedb.additional_info}</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#requirments">{$aLang.plugin.gamedb.requirments}</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#violence">{$aLang.plugin.gamedb.violence}</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tags">{$aLang.plugin.gamedb.tags}</a>
                        </li>
                    </ul>

                    <div class="tab-content">

                        <div id="base" class="tab-pane fade in active">

                            <div class="form-group">
                                <label for="title">{$aLang.plugin.gamedb.field_title}</label>
                                <input type="text" placeholder="{$aLang.plugin.gamedb.field_title}" class="form-control" id="title" name="title"/>
                            </div>

                            <div class="form-group">
                                <label for="title_aka">{$aLang.plugin.gamedb.field_title_aka}</label>
                                <input type="text" placeholder="{$aLang.plugin.gamedb.field_title_aka}" class="form-control" id="title_aka" name="title_aka"/>
                            </div>

                            <div class="form-group">
                                <label for="cover">{$aLang.plugin.gamedb.field_cover}</label>
                                <input class="w40p text" type="file" id="cover" name="cover"   /><br />
                            </div>

                            <div class="form-group">
                                <label for="short_description">{$aLang.plugin.gamedb.field_short_description}</label>
                                <input type="text" placeholder="{$aLang.plugin.gamedb.field_short_description}" class="form-control" id="short_description" name="short_description"/>
                            </div>

                            <div class="form-group">
                                <label for="description">{$aLang.plugin.gamedb.field_description}</label>
                                <input type="text" placeholder="{$aLang.plugin.gamedb.field_description}" class="form-control" id="description" name="description"/>
                            </div>

                            <div class="form-group">
                                <label for="site">{$aLang.plugin.gamedb.field_site}</label>
                                <input type="text" placeholder="{$aLang.plugin.gamedb.field_site}" class="form-control" id="site" name="site"/>
                            </div>

                            <div class="form-group">
                                <label for="download_link">{$aLang.plugin.gamedb.field_download_link}</label>
                                <input type="text" placeholder="{$aLang.plugin.gamedb.field_download_link}" class="form-control" id="download_link" name="download_link"/>
                            </div>

                            <div class="form-group">
                                <label for="released">{$aLang.plugin.gamedb.field_released}</label>
                                <input type="date" class="form-control" id="released" name="released"/>
                            </div>

                            <div class="form-group">
                                <label for="version">{$aLang.plugin.gamedb.field_version}</label>
                                <input type="text" placeholder="{$aLang.plugin.gamedb.field_version}" class="form-control" id="version" name="version"/>
                            </div>
                        </div>

                        <div id="add_info" class="tab-pane fade">

                            <div class="form-group">
                                <label for="engine">{$aLang.plugin.gamedb.field_engine}</label>
                                <select name="engine">
                                	{foreach from=$aEnginesList item=oEngine}
				                    <option value="{$oEngine->getId()}">{$oEngine->getName()}</option>
				                    {/foreach}
                                </select>
                            </div>

                            <div class="form-group">
                                <label>{$aLang.plugin.gamedb.field_game_modes}</label>
                                <br/>
                                <label for="has_story_mode">{$aLang.plugin.gamedb.field_has_story_mode}</label>
                                <input type="checkbox" id="has_story_mode" name="has_story_mode"/><br/>
                                
                                <label for="has_single">{$aLang.plugin.gamedb.field_has_single}</label>
                                <input type="checkbox" id="has_single" name="has_single"/><br/>
                                
                                <label for="has_multi">{$aLang.plugin.gamedb.field_has_multi}</label>
                                <input type="checkbox" id="has_multi" name="has_multi"/><br/>
                                
                                <label for="is_mmo">{$aLang.plugin.gamedb.field_is_mmo}</label>
                                <input type="checkbox" id="is_mmo" name="is_mmo"/><br/>
                                
                                <label for="split_screen">{$aLang.plugin.gamedb.field_split_screen}</label>
                                <input type="checkbox" id="split_screen" name="split_screen"/><br/>
                                
                                <label for="hot_seat">{$aLang.plugin.gamedb.field_hot_seat}</label>
                                <input type="checkbox" id="hot_seat" name="hot_seat"/><br/>
                                
                                <label for="pvp">{$aLang.plugin.gamedb.field_pvp}</label>
                                <input type="checkbox" id="pvp" name="pvp"/><br/>
                                
                                <label for="pve">{$aLang.plugin.gamedb.field_pve}</label>
                                <input type="checkbox" id="pve" name="pve"/><br/>
                                
                                <label for="has_coop">{$aLang.plugin.gamedb.field_has_coop}</label>
                                <input type="checkbox" id="has_coop" name="has_coop"/><br/>
                                
                            </div>

                            <div class="form-group">
                                <label for="has_achievements">{$aLang.plugin.gamedb.field_has_achievements}</label>
                                <input type="checkbox" placeholder="{$aLang.plugin.gamedb.field_has_achievements}" id="has_achievements" name="has_achievements"/>
                            </div>

                            <div class="form-group">
                                <label for="has_scoreboard">{$aLang.plugin.gamedb.field_has_scoreboard}</label>
                                <input type="checkbox" placeholder="{$aLang.plugin.gamedb.field_has_scoreboard}" id="has_scoreboard" name="has_scoreboard"/>
                            </div>

                            <div class="form-group">
                                <label for="has_game_replay_system">{$aLang.plugin.gamedb.field_has_game_replay_system}</label>
                                <input type="checkbox" placeholder="{$aLang.plugin.gamedb.field_has_game_replay_system}" id="has_game_replay_system" name="has_game_replay_system"/>
                            </div>

                            <div class="form-group">
                                <label for="is_moddable">{$aLang.plugin.gamedb.field_is_moddable}</label>
                                <input type="checkbox" placeholder="{$aLang.plugin.gamedb.field_is_moddable}" id="is_moddable" name="is_moddable"/>
                            </div>
                            
                            <div class="form-group">
                                <label for="has_map_editor">{$aLang.plugin.gamedb.field_has_map_editor}</label>
                                <input type="checkbox" placeholder="{$aLang.plugin.gamedb.field_has_map_editor}" id="has_map_editor" name="has_map_editor"/>
                            </div>

                        </div>

                        <div id="requirments" class="tab-pane fade">

							<div class="form-group">
                                <label>{$aLang.plugin.gamedb.field_platforms}</label><br/>
                            	{foreach from=$aPlatformsList item=oPlatform}
                            	<label for="{$oPlatform->getName()}">{$oPlatform->getName()}</label>
                            	<input type="checkbox" id="platform-{$oPlatform->getId()}" name="platform-{$oPlatform->getId()}"/><br/>
			                    {/foreach}
                            </div>

                            <div class="form-group">
                                <label for="cpu">{$aLang.plugin.gamedb.field_cpu}</label>
                                <input type="number" placeholder="{$aLang.plugin.gamedb.field_cpu}" id="cpu" name="cpu"/>
                            </div>

                            <div class="form-group">
                                <label for="ram">{$aLang.plugin.gamedb.field_ram}</label>
                                <input type="number" placeholder="{$aLang.plugin.gamedb.field_ram}" id="ram" name="ram"/>
                            </div>

                            <div class="form-group">
                                <label for="hdd">{$aLang.plugin.gamedb.field_hdd}</label>
                                <input type="number" placeholder="{$aLang.plugin.gamedb.field_hdd}" id="hdd" name="hdd"/>
                            </div>

                            <div class="form-group">
                                <label for="gpu">{$aLang.plugin.gamedb.field_gpu}</label>
                                <input type="number" placeholder="{$aLang.plugin.gamedb.field_ages_from}" id="gpu" name="gpu"/>
                            </div>

                            <div class="form-group">
                                <label for="controller">{$aLang.plugin.gamedb.field_controller}</label>
                                <input type="text" placeholder="{$aLang.plugin.gamedb.field_controller}" class="form-control" id="controller" name="controller"/>
                            </div>

                        </div>

                        <div id="violence" class="tab-pane fade">

                            <div class="form-group">
                                <label for="smoke">{$aLang.plugin.gamedb.field_smoke}</label>
                                <input type="checkbox" placeholder="{$aLang.plugin.gamedb.field_smoke}" id="smoke" name="smoke"/>
                            </div>

                            <div class="form-group">
                                <label for="drugs">{$aLang.plugin.gamedb.field_drugs}</label>
                                <input type="checkbox" placeholder="{$aLang.plugin.gamedb.field_drugs}" id="drugs" name="drugs"/>
                            </div>

                            <div class="form-group">
                                <label for="alcohol">{$aLang.plugin.gamedb.field_alcohol}</label>
                                <input type="checkbox" placeholder="{$aLang.plugin.gamedb.field_alcohol}" id="alcohol" name="alcohol"/>
                            </div>

                            <div class="form-group">
                                <label for="violence">{$aLang.plugin.gamedb.field_violence}</label>
                                <input type="checkbox" placeholder="{$aLang.plugin.gamedb.field_violence}"  id="violence" name="violence"/>
                            </div>

                            <div class="form-group">
                                <label for="nsfw">{$aLang.plugin.gamedb.field_nsfw}</label>
                                <input type="checkbox" placeholder="{$aLang.plugin.gamedb.field_nsfw}"  id="nsfw" name="nsfw"/>
                            </div>

                            <div class="form-group">
                                <label for="violent_language">{$aLang.plugin.gamedb.field_violent_language}</label>
                                <input type="checkbox" placeholder="{$aLang.plugin.gamedb.field_violent_language}"  id="violent_language" name="violent_language"/>
                            </div>

                            <div class="form-group">
                                <label for="ages_from">{$aLang.plugin.gamedb.field_ages_from}</label>
                                <input type="number" placeholder="{$aLang.plugin.gamedb.field_ages_from}" id="ages_from" name="ages_from" min="1" max="100"/>
                            </div>
                        </div>
                        
                        <div id="tags" class="tab-pane fade">
                        	<div class="form-group">
                                <label>{$aLang.plugin.gamedb.field_tags}</label><br/>
                            	{foreach from=$aTagsList item=oTag}
                            	<label for="{$oTag->getName()}">{$oTag->getName()}</label>
                            	<input type="checkbox" id="tag-{$oTag->getId()}" name="tag-{$oTag->getId()}"/><br/>
			                    {/foreach}
                            </div>
                    	</div>
                        
                    </div>

                    <div class="b-wbox-content nopadding">
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary" name="submit_category_add">
                                {$aLang.plugin.gamedb.add_game}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            {else}
            {$aLang.plugin.gamedb.error}
            {/if}
        </div>
    </div>
</div>
{/block}
