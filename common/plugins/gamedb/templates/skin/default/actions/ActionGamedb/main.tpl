{extends file="_index.tpl"}

{block name="layout_vars"}
{$noSidebar=true}
{/block}
{block name="layout_content"}
<div class="span12">
    {if $oUserCurrent}
    <a href="/gamedb/add/">{$aLang.plugin.gamedb.add_game}</a>
    <br/>
    {/if}

    <div id="filters_toggle">
        {$aLang.plugin.gamedb.sorting_n_filters}
    </div>
    <div id="filters">
        filtets here
    </div>

    <div class="panel panel-default panel-table raised" id="games">
        <div class="panel-body">
            {if $aGamesList}
            <table class="games-table">
                <thead>
                    <tr>
                        <td class="cover">{$aLang.plugin.gamedb.field_cover}</td>
                        <td>{$aLang.plugin.gamedb.field_title_description}</td>
                        <td>{$aLang.plugin.gamedb.field_rating}</td>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$aGamesList item=oGame}
                    <tr>
                        <td class="cover"><img src="{$oGame->getCover()}"/></td>
                        <td class="center"> {$oGame->getName()}
                        <span class="info">
                            {$oGame->getReleased()}
                            {foreach from=$oGame->platforms item=oPlatform}
                            {$oPlatform->getName()}
                            {/foreach} 
                        </span>
                        <span class="tags">
                            {foreach from=$oGame->tags item=oTag}
                            {$oTag->getName()}
                            {/foreach} 
                        </span>
                        <br/>
                        <div style="color: #999;">
                            {$oGame->getNameAka()}
                        </div>
                        <div style="margin-top: 16px;">
                            {$oGame->getShortDescription()}
                        </div></td>
                        <td>Рейтинг</td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>
            {else}
            {$aLang.plugin.gamedb.empty}
            {/if}
        </div>
    </div>

</div>

<script>
	$("#filters").slideUp(0);

	$("#filters_toggle").click(function() {
		$("#filters").slideToggle("slow", function() {
		});
	}); 
</script>

{/block}
