{if E::IsUser()}
<div class="modal fade in" id="modal_add_tag">
    <div class="modal-dialog">
        <div class="modal-content">

            <header class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">{$aLang.plugin.gamedb.add_tag}</h4>
            </header>

            <form action="/admin/gamedb/tag/add" method="POST" class="uniform">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="title" class="control-label">{$aLang.plugin.gamedb.field_title}:</label>
                        <textarea id="title" class="form-control input-text input-wide" name="title" autofocus="autofocus"></textarea>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="security_key" value="{$ALTO_SECURITY_KEY}"/>
                    <input type="hidden" name="return-path" value="{Router::Url('link')}"/>
                    <button type="submit" class="btn btn-primary">
                        {$aLang.plugin.gamedb.add}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
{/if}
