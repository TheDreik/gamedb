<?php
/*
 * russian.php
 * 
 * Project: GameDb
 * 
 * Copyright 2015 veloc1 <pavel@veloc1.me>
 */

return array(
	'name' => 'Игробаза',
    
    'add' => 'Добавить',
    'delete' => 'Удалить',
    'add_game' => 'Добавить игру',
    'add_platform' => 'Добавить платформу',
    'add_tag' => 'Добавить тег',
    'add_engine' => 'Добавить движок',
    'add_genre' => 'Добавить жанр',
    
	'sorting_n_filters' => 'Сортировка и фильтры',
    
	'platform' => 'Платформа',
    'platforms' => 'Платформы',
    'genre' => 'Жанр',
    'genres' => 'Жанры',
    'tag' => 'Тег',
    'tags' => 'Теги',
    'engines' => 'Движки',
    'base_info' => 'Основная информация',
    'additional_info' => 'Дополнительная информация',
    'requirments' => 'Системные требования',
    'violence' => 'Насилие',
    
	'field_title' => 'Название',
	'field_description' => 'Название и описание',
	'field_title_aka' => 'Известен как',
	'field_short_description' => 'Краткое описание',
	'field_description' => 'Описание',
	'field_site' => 'Адрес',
	'field_download_link' => 'Ссылка на скачивание',
	'field_released' => 'Дата релиза',
	'field_version' => 'Версия',
	'field_cover' => 'Обложка',
	'field_rating' => 'Оценка',
	'field_options' => 'Опции',
	'field_smoke' => 'Курение',
	'field_drugs' => 'Наркотики',
	'field_alcohol' => 'Алкоголь',
	'field_violence' => 'Жестокость',
	'field_nsfw' => 'Обнаженка',
	'field_violent_language' => 'Мат',
	'field_ages_from' => 'Возрастное ограничение',
	'field_cpu' => 'Процессор, Ггц',
	'field_ram' => 'Оперативная память, Мб',
	'field_hdd' => 'Жесткий диск, Мб',
	'field_gpu' => 'Размер видеопамяти, Мб',
	'field_controller' => 'Контроллер',
	'field_has_achievements' => 'Есть достижения',
	'field_has_scoreboard' => 'Есть доска лидеров',
	'field_has_game_replay_system' => 'Есть режим повторов',
	'field_engine' => 'Движок',
	'field_game_modes' => 'Режимы игры',
	'field_is_moddable' => 'Можно модифицировать',
	'field_has_map_editor' => 'Есть редактор карт',
	'field_platforms' => 'Платформы',
	'field_tags' => 'Теги',
	
	'field_has_story_mode' => 'Режим истории',
	'field_has_single' => 'Одиночная игра',
	'field_has_multi' => 'Мультиплеер',
	'field_is_mmo' => 'ММО',
	'field_split_screen' => 'Сплит-скрин',
	'field_hot_seat' => 'Горячий стул',
	'field_pvp' => 'PVP',
	'field_pve' => 'PVE',
	'field_has_coop' => 'Кооперативный режим',	
    
    'error' => 'Ошибка',
    'empty' => 'Пусто',
);

?>
