<?php
/*
 * config.php
 * 
 * Project: GameDb
 * 
 * Copyright 2015 veloc1 <pavel@veloc1.me>
 */
 
$config=array();

//~ $config['table']['gamedb']                = '___db.table.prefix___games';

Config::Set('router.page.gamedb', 'PluginGamedb_ActionGamedb');
Config::Set('plugin.gamedb.upload_dir', Config::Get('path.root.server') . Config::Get('path.uploads.root') . '/gamedb/');
Config::Set('plugin.gamedb.images_dir', Config::Get('path.root.web') . Config::Get('path.uploads.root') . '/gamedb/');

return $config;
?>
