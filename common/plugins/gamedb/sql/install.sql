-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Июл 06 2015 г., 19:17
-- Версия сервера: 5.6.21
-- Версия PHP: 5.6.3
-- Кароч, орм не поддерживает нижние подчеркивания, поэтому в именах каша

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `kolenka`
--

-- --------------------------------------------------------
-- --------------------------------------------------------

--
-- Структура таблицы `addon`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_addon` (
  `game_id` int(11) NOT NULL,
  ` name` text NOT NULL,
  `url` text NOT NULL,
  `compatible_with_version` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `developer`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_developer` (
`id` int(11) NOT NULL,
  `name` text NOT NULL,
  `year_start` int(11) NOT NULL,
  `year_end` int(11) NOT NULL,
  `url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `engine`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_engine` (
`id` int(11) NOT NULL,
  `name` text NOT NULL,
  `url` text NOT NULL,
  `description` text NOT NULL,
  `icon` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_item`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_galleryitem` (
`id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `game`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_game` (
`id` int(11) NOT NULL,
  `name` text NOT NULL,
  `cover` text NOT NULL,
  `name_aka` text NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `site` text NULL,
  `download_link` text NULL,
  `released` date NULL,
  `latest_version` text NULL,
  `smoke` tinyint(1) NOT NULL DEFAULT '0',
  `drugs` tinyint(1) NOT NULL DEFAULT '0',
  `alcohol` tinyint(1) NOT NULL DEFAULT '0',
  `violence` tinyint(1) NOT NULL DEFAULT '0',
  `nsfw` tinyint(1) NOT NULL DEFAULT '0',
  `violent_language` tinyint(1) NOT NULL DEFAULT '0',
  `ages_from` int(11) NULL DEFAULT '0',
  `cpu` text NULL,
  `ram` text NULL,
  `hdd` text NULL,
  `gpu` text NULL,
  `controller` text NULL DEFAULT '',
  `engine_id` int(11) NULL,
  `has_story_mode` tinyint(1) NOT NULL DEFAULT '0',
  `has_single` tinyint(1) NOT NULL DEFAULT '0',
  `has_multi` tinyint(1) NOT NULL DEFAULT '0',
  `is_mmo` tinyint(1) NOT NULL DEFAULT '0',
  `split_screen` tinyint(1) NOT NULL DEFAULT '0',
  `hot_seat` tinyint(1) NOT NULL DEFAULT '0',
  `pvp` tinyint(1) NOT NULL DEFAULT '0',
  `pve` tinyint(1) NOT NULL DEFAULT '0',
  `has_coop` tinyint(1) NOT NULL DEFAULT '0',
  `is_moddable` tinyint(1) NOT NULL DEFAULT '0',
  `has_map_editor` tinyint(1) NOT NULL DEFAULT '0',
  `has_achievement_system` tinyint(1) NOT NULL DEFAULT '0',
  `has_scoring_system` tinyint(1) NOT NULL DEFAULT '0',
  `has_game_replay_system` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `game_addon`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_gameaddon` (
  `game_id` int(11) NOT NULL,
  `addon_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `game_document`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_gamedocument` (
  `game_id` int(11) NOT NULL,
  `name_url_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `game_language`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_gamelanguage` (
`id` int(11) NOT NULL,
  `text_localized` tinyint(1) NOT NULL DEFAULT '0',
  `sound_localized` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `game_language_language`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_gamelanguagelanguage` (
  `game_language_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
-- --------------------------------------------------------

--
-- Структура таблицы `game_platform`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_gameplatform` (
  `game_id` int(11) NOT NULL,
  `platform_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `game_rating`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_gamerating` (
  `game_id` int(11) NOT NULL,
  `rating_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `game_to_developer`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_gamedeveloper` (
  `game_id` int(11) NOT NULL,
  `developer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `game_to_game_language`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_gamegamelanguage` (
  `game_id` int(11) NOT NULL,
  `game_language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `game_to_genre`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_gamegenre` (
  `game_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `game_to_press`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_gamepress` (
  `game_id` int(11) NOT NULL,
  `press_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `game_to_publisher`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_gamepublisher` (
  `game_id` int(11) NOT NULL,
  `publisher_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `genre`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_genre` (
`id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `language`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_language` (
`id` int(11) NOT NULL,
  `name` text NOT NULL,
  `icon` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `name_url`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_nameurl` (
`id` int(11) NOT NULL,
  `name` text CHARACTER SET latin1 NOT NULL,
  `url` text CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ost_item`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_ostitem` (
`id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `platform`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_platform` (
`id` int(11) NOT NULL,
  `name` text NOT NULL,
  `icon` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `press_review`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_pressreview` (
`id` int(11) NOT NULL,
  `press_title` text NOT NULL,
  `press_short_review` text NOT NULL,
  `link_to_full_review` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Структура таблицы `user_rating`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_userrating` (
`id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `overall` int(11) NOT NULL,
  `sound` int(11) NOT NULL,
  `graphic` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `video_gallery_item`
--

CREATE TABLE IF NOT EXISTS `prefix_gamedb_videogalleryitem` (
`id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------


CREATE TABLE IF NOT EXISTS `prefix_gamedb_tag` (
`id` int(11) NOT NULL,
`name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `prefix_gamedb_gametag` (
  `game_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `developer`
--
ALTER TABLE `prefix_gamedb_developer`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `engine`
--
ALTER TABLE `prefix_gamedb_engine`
 ADD PRIMARY KEY (`id`);


--
-- Индексы таблицы `gallery_item`
--
ALTER TABLE `prefix_gamedb_galleryitem`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `game`
--
ALTER TABLE `prefix_gamedb_game`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `game_language`
--
ALTER TABLE `prefix_gamedb_gamelanguage`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `genre`
--
ALTER TABLE `prefix_gamedb_genre`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `language`
--
ALTER TABLE `prefix_gamedb_language`
 ADD PRIMARY KEY (`id`);


--
-- Индексы таблицы `name_url`
--
ALTER TABLE `prefix_gamedb_nameurl`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ost_item`
--
ALTER TABLE `prefix_gamedb_ostitem`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `platform`
--
ALTER TABLE `prefix_gamedb_platform`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `press_review`
--
ALTER TABLE `prefix_gamedb_pressreview`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_rating`
--
ALTER TABLE `prefix_gamedb_userrating`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `video_gallery_item`
--
ALTER TABLE `prefix_gamedb_videogalleryitem`
 ADD PRIMARY KEY (`id`);
 
 ALTER TABLE `prefix_gamedb_tag`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `developer`
--
ALTER TABLE `prefix_gamedb_developer`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `engine`
--
ALTER TABLE `prefix_gamedb_engine`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `gallery_item`
--
ALTER TABLE `prefix_gamedb_galleryitem`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `game`
--
ALTER TABLE `prefix_gamedb_game`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `game_language`
--
ALTER TABLE `prefix_gamedb_gamelanguage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `genre`
--
ALTER TABLE `prefix_gamedb_genre`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `language`
--
ALTER TABLE `prefix_gamedb_language`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `name_url`
--
ALTER TABLE `prefix_gamedb_nameurl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ost_item`
--
ALTER TABLE `prefix_gamedb_ostitem`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `platform`
--
ALTER TABLE `prefix_gamedb_platform`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `press_review`
--
ALTER TABLE `prefix_gamedb_pressreview`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `user_rating`
--
ALTER TABLE `prefix_userrating`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `video_gallery_item`
--
ALTER TABLE `prefix_gamedb_videogalleryitem`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `prefix_gamedb_tag`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
