<?php
/*
 * GameDbPlugin.class.php
 *
 * Project: GameDb
 *
 * Copyright 2015 veloc1 <pavel@veloc1.me>
 */

if (!class_exists('Plugin')) {
	die('Hacking attempt!');
}

class PluginGamedb extends Plugin {

	public function Activate() {
		if (!$this -> isTableExists('prefix_gamedb_game')) {
			$this -> ExportSQL(dirname(__FILE__) . '/sql/install.sql');
		}
		$sDir = Config::Get('path.root.server') . Config::Get('path.uploads.root') . '/gamedb/';
        if (!file_exists($sDir)) {
            @mkdir($sDir);
            @chmod($sDir, 0777);
        }
		return TRUE;
	}

	protected $aInherits = array('actions' => array('ActionAdmin', ), );

	public function Init() {
		parent::Init();

		//~ $this->Viewer_AppendScript(Plugin::GetTemplateWebPath(__CLASS__).'js/voter.js');
		$this -> Viewer_AppendStyle(Plugin::GetTemplateWebPath(__CLASS__) . 'css/style.css');
	}

}
