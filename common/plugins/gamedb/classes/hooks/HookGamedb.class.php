<?php

/**
 * Регистрация хука
 *
 */
class PluginGamedb_HookGamedb extends Hook {
	public function RegisterHook() {
		$this->AddHook('template_main_menu_item','Menu');
	}

	public function Menu() {
		return $this->Viewer_Fetch(Plugin::GetTemplatePath(__CLASS__).'main_menu.tpl');
	}
}
?>
