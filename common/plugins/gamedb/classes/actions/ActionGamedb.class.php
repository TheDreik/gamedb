<?php
/*
 * ActionGameDb.class.php
 *
 * Project: GameDb
 *
 * Copyright 2015 veloc1 <pavel@veloc1.me>
 */

class PluginGamedb_ActionGamedb extends ActionPlugin {

	public function Init() {
	}

	protected function RegisterEvent() {
		$this -> AddEventPreg('/^add$/i', '/^post$/i', 'EventAddGame');
		$this -> AddEventPreg('/^add$/i', 'EventAdd');
		$this -> AddEvent('index', 'EventMain');
	}

	protected function EventMain() {
		$params = $this -> getParams();

		$this -> SetTemplateAction('main');
		$aGames = Engine::GetInstance() -> PluginGamedb_Gamedb_GetItemsByFilter(array(), "PluginGamedb_ModuleGamedb_EntityGame");
		foreach ($aGames as $oGame) {
			$oGame -> platforms = array();
			$aJoins = Engine::GetInstance() -> PluginGamedb_Gamedb_GetItemsByFilter(array('game_id' => $oGame -> getId()), "PluginGamedb_ModuleGamedb_EntityGameplatform");
			foreach ($aJoins as $oJoin) {
				array_push($oGame -> platforms, Engine::GetInstance() -> PluginGamedb_Gamedb_GetPlatformById($oJoin->getPlatformId()));
			}
			
			$oGame -> tags = array();
			$aJoins = Engine::GetInstance() -> PluginGamedb_Gamedb_GetItemsByFilter(array('game_id' => $oGame -> getId()), "PluginGamedb_ModuleGamedb_EntityGametag");
			foreach ($aJoins as $oJoin) {
				array_push($oGame -> tags, Engine::GetInstance() -> PluginGamedb_Gamedb_GetTagById($oJoin->getTagId()));
			}
		}
		$this -> Viewer_Assign('aGamesList', $aGames);
	}

	protected function EventAdd() {
		$params = $this -> getParams();
		$this -> Viewer_Assign('aEnginesList', Engine::GetInstance() -> PluginGamedb_Gamedb_GetItemsByFilter(array(), "PluginGamedb_ModuleGamedb_EntityEngine"));
		$this -> Viewer_Assign('aPlatformsList', Engine::GetInstance() -> PluginGamedb_Gamedb_GetItemsByFilter(array(), "PluginGamedb_ModuleGamedb_EntityPlatform"));
		$this -> Viewer_Assign('aTagsList', Engine::GetInstance() -> PluginGamedb_Gamedb_GetItemsByFilter(array(), "PluginGamedb_ModuleGamedb_EntityTag"));
		$this -> SetTemplateAction('add');
	}

	protected function EventAddGame() {
		$oGame = Engine::GetEntity('PluginGamedb_ModuleGamedb_EntityGame');

		$this -> saveBaseInfo($oGame);
		$this -> saveAdditionalInfo($oGame);
		$this -> saveRequirments($oGame);
		$this -> saveViolence($oGame);

		$oGame -> Add();

		$this -> savePlatforms($oGame);
		$this -> saveTags($oGame);

		Router::Location('gamedb');
	}

	private function saveBaseInfo($oGame) {
		$oGame -> setName(getRequest('title'));
		$this -> saveCover($oGame);
		$oGame -> setNameAka(getRequest('title_aka'));
		$oGame -> setShortDescription(getRequest('short_description'));
		$oGame -> setDescription(getRequest('description'));
		$oGame -> setSite(getRequest('site'));
		$oGame -> setDownloadLink(getRequest('download_link'));
		$oGame -> setReleased(getRequest('released'));
		$oGame -> setLatestVersion(getRequest('version'));
	}

	private function saveAdditionalInfo($oGame) {
		$oGame -> setEngineId(getRequest('engine'));

		$oGame -> setHasStoryMode(getRequest('has_story_mode') == "on" ? 1 : 0);
		$oGame -> setHasSingle(getRequest('has_single') == "on" ? 1 : 0);
		$oGame -> setHasMulti(getRequest('has_multi') == "on" ? 1 : 0);
		$oGame -> setIsMmo(getRequest('is_mmo') == "on" ? 1 : 0);
		$oGame -> setSplitScreen(getRequest('split_screen') == "on" ? 1 : 0);
		$oGame -> setHotSeat(getRequest('hot_seat') == "on" ? 1 : 0);
		$oGame -> setPvp(getRequest('pvp') == "on" ? 1 : 0);
		$oGame -> setPve(getRequest('pve') == "on" ? 1 : 0);
		$oGame -> setHasCoop(getRequest('has_coop') == "on" ? 1 : 0);

		$oGame -> setHasAchievementSystem(getRequest('has_achievements') == "on" ? 1 : 0);
		$oGame -> setHasScoringSystem(getRequest('has_scoreboard') == "on" ? 1 : 0);
		$oGame -> setHasGameReplaySystem(getRequest('has_game_replay_system') == "on" ? 1 : 0);
		$oGame -> setIsModdable(getRequest('is_moddable') == "on" ? 1 : 0);
		$oGame -> setHasMapEditor(getRequest('has_map_editor') == "on" ? 1 : 0);
	}

	private function saveRequirments($oGame) {
		$oGame -> setCpu(getRequest('cpu'));
		$oGame -> setRam(getRequest('ram'));
		$oGame -> setHdd(getRequest('hdd'));
		$oGame -> setGpu(getRequest('gpu'));
		$oGame -> setController(getRequest('controller'));
	}

	private function saveViolence($oGame) {
		$oGame -> setSmoke(getRequest('smoke') == "on" ? 1 : 0);
		$oGame -> setDrugs(getRequest('drugs') == "on" ? 1 : 0);
		$oGame -> setAlcohol(getRequest('alcohol') == "on" ? 1 : 0);
		$oGame -> setViolence(getRequest('violence') == "on" ? 1 : 0);
		$oGame -> setNsfw(getRequest('nsfw') == "on" ? 1 : 0);
		$oGame -> setViolentLanguage(getRequest('violent_language') == "on" ? 1 : 0);
		$oGame -> setAgesFrom(getRequest('ages_from'));
	}

	private function saveCover($oGame) {
		if (isset($_FILES["cover"]) && $_FILES["cover"]["error"] == 0) {
			$aImageFile = $_FILES["cover"];

			$aSize = @getimagesize($aImageFile["tmp_name"]);
			if (!in_array($aSize['mime'], array('image/jpeg', 'image/gif', 'image/png'))) {
				$this -> Logger_Error("mime error");
				// $this->Message_AddError(
				// $this->Lang_Get("plugin.ab.banneroid_error_image_extension"),
				// $this->Lang_Get('plugin.ab.banneroid_error'));
				// $iOk = 0;
			} else if (!$this -> UploadImage($aImageFile, $oGame)) {
				$this -> Logger_Error("upload error");
				// $this->Message_AddError(
				// $this->Lang_Get("plugin.ab.banneroid_error_unable_to_upload_image"),
				// $this->Lang_Get('plugin.ab.banneroid_error'));
				// $iOk = 0;
			}
		}
	}

	private function savePlatforms($oGame) {
		// $oItem = Engine::GetInstance() -> PluginGamedb_Gamedb_GetGameplatformByGameId($oGame -> getId());
		// $oItem -> Delete();

		$aPlatforms = Engine::GetInstance() -> PluginGamedb_Gamedb_GetItemsByFilter(array(), "PluginGamedb_ModuleGamedb_EntityPlatform");
		foreach ($aPlatforms as $oPlatform) {
			if (getRequest("platform-" . $oPlatform -> getId()) != null) {
				$oGamePlat = Engine::GetEntity('PluginGamedb_ModuleGamedb_EntityGameplatform', array());
				$oGamePlat -> setGameId($oGame -> getId());
				$oGamePlat -> setPlatformId($oPlatform -> getId());
				$oGamePlat -> Add();
			}
		}
	}

	private function saveTags($oGame) {
		// $oItem = Engine::GetInstance() -> PluginGamedb_Gamedb_GetGameplatformByGameId($oGame -> getId());
		// $oItem -> Delete();

		$aTags = Engine::GetInstance() -> PluginGamedb_Gamedb_GetItemsByFilter(array(), "PluginGamedb_ModuleGamedb_EntityTag");
		foreach ($aTags as $oTag) {
			if (getRequest("tag-" . $oTag -> getId()) != null) {
				$oGameTag = Engine::GetEntity('PluginGamedb_ModuleGamedb_EntityGametag', array());
				$oGameTag -> setGameId($oGame -> getId());
				$oGameTag -> setTagId($oTag -> getId());
				$oGameTag -> Add();
			}
		}
	}

	private function UploadImage($aImageFile, $oGame) {
		$sFileTmp = $aImageFile['tmp_name'];

		if (is_uploaded_file($sFileTmp)) {
			$this -> Logger_Error("file uploaded");
			if (strlen(@$oGame -> getCover())) {//remove old image file
				@unlink(Config::Get("plugin.gamedb.upload_dir") . $oBanner -> getCover());
			}

			$sFileName = func_generator();
			//gen new file name

			$aFileInfo = pathinfo($aImageFile['name']);
			$sFileName .= "." . $aFileInfo['extension'];

			if (!@move_uploaded_file($sFileTmp, Config::Get("plugin.gamedb.upload_dir") . $sFileName)) {
				$this -> Logger_Error("cant move uploaded file");
				return false;
			}

			$oGame -> setCover(Config::Get("plugin.gamedb.images_dir") . $sFileName);
			return true;
		}
		return false;
	}

}
