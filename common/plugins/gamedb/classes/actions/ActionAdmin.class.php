<?php

class PluginGamedb_ActionAdmin extends PluginGamedb_Inherit_ActionAdmin {

	protected function RegisterEvent() {
		parent::RegisterEvent();

		$this -> AddEventPreg('/^gamedb$/i', '/^platform$/i', '/^delete$/i', '/^(\d+)/i', 'EventAdminGamedbPlatformDelete');
		$this -> AddEventPreg('/^gamedb$/i', '/^platform$/i', '/^add$/i', 'EventAdminGamedbPlatformAdd');

		$this -> AddEventPreg('/^gamedb$/i', '/^tag$/i', '/^delete$/i', '/^(\d+)/i', 'EventAdminGamedbTagDelete');
		$this -> AddEventPreg('/^gamedb$/i', '/^tag$/i', '/^add$/i', 'EventAdminGamedbTagAdd');

		$this -> AddEventPreg('/^gamedb$/i', '/^genre$/i', '/^delete$/i', '/^(\d+)/i', 'EventAdminGamedbGenreDelete');
		$this -> AddEventPreg('/^gamedb$/i', '/^genre$/i', '/^add$/i', 'EventAdminGamedbGenreAdd');

		$this -> AddEventPreg('/^gamedb$/i', '/^engine$/i', '/^delete$/i', '/^(\d+)/i', 'EventAdminGamedbEngineDelete');
		$this -> AddEventPreg('/^gamedb$/i', '/^engine$/i', '/^add$/i', 'EventAdminGamedbEngineAdd');

		$this -> AddEventPreg('/^gamedb$/i', '/^$/i', 'EventAdminGamedb');
	}

	protected function EventAdminGamedb() {
		$this -> sMainMenuItem = 'content';

		$this -> Viewer_Assign('aPlatformsList', Engine::GetInstance() -> PluginGamedb_Gamedb_GetItemsByFilter(array(), "PluginGamedb_ModuleGamedb_EntityPlatform"));
		$this -> Viewer_Assign('aTagsList', Engine::GetInstance() -> PluginGamedb_Gamedb_GetItemsByFilter(array(), "PluginGamedb_ModuleGamedb_EntityTag"));
		$this -> Viewer_Assign('aGenresList', Engine::GetInstance() -> PluginGamedb_Gamedb_GetItemsByFilter(array(), "PluginGamedb_ModuleGamedb_EntityGenre"));
		$this -> Viewer_Assign('aEnginesList', Engine::GetInstance() -> PluginGamedb_Gamedb_GetItemsByFilter(array(), "PluginGamedb_ModuleGamedb_EntityEngine"));
		$this -> _setTitle($this -> Lang_Get('plugin.gamedb.name'));
	}

	protected function EventAdminGamedbPlatformAdd() {
		$this -> Security_ValidateSendForm();

		$this -> SetTemplate(false);

		$oPlatform = Engine::GetEntity('PluginGamedb_ModuleGamedb_EntityPlatform', array('id' => 0));
		$oPlatform -> setName(getRequest('title'));
		$oPlatform -> Add();

		Router::Location('admin/gamedb');
	}

	protected function EventAdminGamedbPlatformDelete() {
		$this -> SetTemplate(false);
		$params = $this -> getParams();
		$oItem = Engine::GetInstance() -> PluginGamedb_Gamedb_GetPlatformById($params[2]);
		$oItem -> Delete();

		Router::Location('admin/gamedb');
	}

	protected function EventAdminGamedbTagAdd() {
		$this -> Security_ValidateSendForm();

		$this -> SetTemplate(false);

		$oTag = Engine::GetEntity('PluginGamedb_ModuleGamedb_EntityTag', array('id' => 0));
		$oTag -> setName(getRequest('title'));
		$oTag -> Add();

		Router::Location('admin/gamedb');
	}

	protected function EventAdminGamedbTagDelete() {
		$this -> SetTemplate(false);
		$params = $this -> getParams();
		$oItem = Engine::GetInstance() -> PluginGamedb_Gamedb_GetTagById($params[2]);
		$oItem -> Delete();

		Router::Location('admin/gamedb');
	}

	protected function EventAdminGamedbGenreAdd() {
		$this -> Security_ValidateSendForm();

		$this -> SetTemplate(false);

		$oGenre = Engine::GetEntity('PluginGamedb_ModuleGamedb_EntityGenre', array('id' => 0));
		$oGenre -> setName(getRequest('title'));
		$oGenre -> Add();

		Router::Location('admin/gamedb');
	}

	protected function EventAdminGamedbGenreDelete() {
		$this -> SetTemplate(false);
		$params = $this -> getParams();
		$oItem = Engine::GetInstance() -> PluginGamedb_Gamedb_GetGenreById($params[2]);
		$oItem -> Delete();

		Router::Location('admin/gamedb');
	}

	protected function EventAdminGamedbEngineAdd() {
		$this -> Security_ValidateSendForm();

		$this -> SetTemplate(false);

		$oEngine = Engine::GetEntity('PluginGamedb_ModuleGamedb_EntityEngine', array('id' => 0));
		$oEngine -> setName(getRequest('title'));
		$oEngine -> setUrl(getRequest('url'));
		$oEngine -> setDescription("");
		$oEngine -> setIcon("");
		$oEngine -> Add();

		Router::Location('admin/gamedb');
	}

	protected function EventAdminGamedbEngineDelete() {
		$this -> SetTemplate(false);
		$params = $this -> getParams();
		$oItem = Engine::GetInstance() -> PluginGamedb_Gamedb_GetEngineById($params[2]);
		$oItem -> Delete();

		Router::Location('admin/gamedb');
	}

}
?>