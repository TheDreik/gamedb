<?php
/*
 * ActionPTags.class.php
 *
 * Project: PTags
 *
 * Copyright 2015 veloc1 <pavel@veloc1.me>
 */

class PluginPtags_ActionAjax extends ActionAjax {

	public function Init() {
        parent::Init();
    }

	protected function RegisterEvent() {
		$this -> AddEvent('index', 'EventMain');
	}

	protected function EventMain() {
		$params = $this -> getParams();

		$aTags = E::ModuleTopic()->GetTopicTags(1000);
		
		$aResult = array();
		
		foreach ($aTags as $oTag){
			$aResult[$oTag->getText()] = 0;
			//~ E::ModuleTopic()->GetCountTopicsByFilter(array(
				//~ 'topic_publish' => 1, 
				//~ 'period' => date('Y-m-d H:00:00', time() - Config::Get('module.topic.new_time')),
				//~ 'tags'
				//~ );
		}
		// GetCountTopicsByFilter
		$aLastTopics = E::ModuleTopic()->GetTopicsByFilter(array('topic_publish' => 1,
			'topic_new' => date('Y-m-d H:00:00', time() - Config::Get('module.topic.new_time')),
			), 1, 16);
		//~ var_dump($aLastTopics);
		//~ var_dump($aLastTopics["collection"]);
		//~ var_dump($aLastTopics["collection"][3]->getTagsArray());
		foreach ($aLastTopics["collection"] as $oTopic){
			$aTopicTags = $oTopic -> getTagsArray();
			foreach ($aTopicTags as $oTag){
				$aResult[$oTag] = $aResult[$oTag] + 1;
			}
		}
		
		$this->Viewer_SetResponseAjax('json');
		$this->Viewer_AssignAjax('tags', $aResult);
	}

}
