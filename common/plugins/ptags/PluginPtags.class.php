<?php
/*
 * PluginPtags.class.php
 *
 * Copyright 2015 veloc1 <pavel@veloc1.me>
 */

if (!class_exists('Plugin')) {
	die('Hacking attempt!');
}

class PluginPtags extends Plugin {

	public function Activate() {
		return True;
	}
	
	//~ protected $aInherits = array(
        //~ 'action' => array(
            //~ 'ActionAjax',
        //~ ),
    //~ );

	// protected $aInherits = array('actions' => array('ActionAdmin', ), );

	public function Init() {
		parent::Init();
	}

}
