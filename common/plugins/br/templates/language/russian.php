<?php
/**
 * russian.php
 * Файл локализации плагина Br
 *
 * @author      Андрей Воронов <andreyv@gladcode.ru>
 * @copyrights  Copyright © 2014, Андрей Воронов
 *              Является частью плагина Br
 * @version     0.0.1 от 03.11.2014 01:59
 */

return array(

    // Виджет брендирования блога
    'branding'                     => 'Брендирование',
    'branding_save_notice'         => 'Брендирование блога возможно только после его сохранения',
    'blog_photo_change'            => 'Изменить фон блога',
    'blog_photo_upload'            => 'Загрузить фон для блога',
    'blog_background'              => 'Фон блога',
    'blog_photo_default'           => 'Фон блога по умолчанию',
    'blog_color_only'              => 'Использовать только цвет фона',
    'blog_both'                    => 'Использовать картинку и цвет фона',
    'blog_photo_delete'            => 'Вернуть фон блога по умолчанию',
    'error_blog_photo'             => 'Ошибка при загрузке фона блога',
    'blog_resize_window_title'     => 'Изменение размера',
    'blog_resize_window_help'      => 'Выберите область изображения, которая будет установлена в качестве фона. Если обрезать фото не нужно, просто нажмите кнопку "Применить"',
    'blog_upload_success'          => 'Фон блога успешно загружен',
    'branding_save_blog'           => 'Сохранить',
    'blog_font_color'              => 'Цвет шрифта блога',
    'blog_header_color'            => 'Цвет заголовков',

    'error_upload_image'           => 'Ошибка при загрузке изображения',
    'uploader_upload_success'      => 'Файл успешно загружен',

    'blog_background_opacity'      => 'Непрозрачность фона',
    'blog_image_loading_good'      => 'Загружено новое изображение фона блога',
    'blog_opacity_loading_good'    => 'Изменена прозрачность фона блога',
    'blog_image_loading_revert'    => 'Фон блога сброшен в первоначальное состояние',
    'blog_background_color'        => 'Цвет фона блога',
    'blog_saved'                   => 'Изменения сохранены',
    'scroll_to_intensive'          => 'Испоьзуйте скролл для выбора интенсивности цвета',
    'blog_header_step'             => 'Отступ от шапки сайта',
    'blog_background_type'         => 'Положение',
    'blog_background_type_0'       => 'Замостить',
    'blog_background_type_1'       => 'Сверху по центру',
    'blog_background_type_2'       => 'Сверху слева',
    'blog_background_type_3'       => 'Сверху справа',
    'blog_background_type_4'       => 'По центру',
    'blog_background_type_5'       => 'Внизу по центру',
    'blog_background_type_6'       => 'Внизу слева',
    'blog_background_type_7'       => 'Внизу справа',
    'blog_background_type_8'       => 'Растянуть',

    'admin_title'                  => 'Брендирование',
    'cancel'                       => 'Отменить',
    'save'                         => 'Сохранить',
    'admin_use_list'               => 'Использовать следующие виды брендирования для <strong>блога</strong>:',

    'admin_allow_background'       => 'Фоновое изображение страниц блога',
    'admin_allow_font'             => 'Цвет шрифта страниц блога',
    'admin_allow_header'           => 'Цвет заголовков h1, h2, h3, h4, h5, h6 на страницах блога',
    'admin_allow_step'             => 'Отступ содержимого страницы от шапки',

    'remove_branding'              => 'Не использовать брендинг',
    'blog_removed'                 => 'Брендинг больше не используется',
    'blog_branding_removed'        => 'Брендинг этого блога больше не используется',

);