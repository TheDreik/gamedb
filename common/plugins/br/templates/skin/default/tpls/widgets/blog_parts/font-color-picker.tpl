<div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">
            {$aLang.plugin.br.blog_font_color}
        </span>
        <input id="blog-font-color-picker" class="js-branding-blog-font-color-picker input-text form-control" autocomplete="off"  name="color"/>
    </div>
</div>
