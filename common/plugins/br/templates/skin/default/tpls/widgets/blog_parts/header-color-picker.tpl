<div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">
            {$aLang.plugin.br.blog_header_color}
        </span>
        <input id="blog-header-color-picker" class="js-branding-blog-header-color-picker input-text form-control" autocomplete="off"  name="color"/>
    </div>
</div>
