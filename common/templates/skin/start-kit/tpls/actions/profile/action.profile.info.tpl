{extends file="_profile.tpl"}

{block name="layout_vars" append}
    {$sMenuItemSelect="profile"}
{/block}

{block name="layout_profile_content"}

{if $oUserProfile->getProfileAbout()}
    <div class="well profile-info-about">
        <h3>{$aLang.profile_about}</h3>
        {$oUserProfile->getProfileAbout()}
    </div>
{/if}

<div class="profile-content">

<div class="row">
<div class="col-lg-9">
            <h4>Награды</h4>
        {if $oUserProfile->getDisplayName() == 'Hrenzerg'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik1_first_place.png" title="За победу в Наколеннике №01">{/if}
        {if $oUserProfile->getDisplayName() == 'Hrenzerg'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik1.png" title="За участие в Наколеннике №01">{/if}
        {if $oUserProfile->getDisplayName() == 'Snuux'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik1.png" title="За участие в Наколеннике №01">{/if}
        {if $oUserProfile->getDisplayName() == 'Tetsuwan'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik1.png" title="За участие в Наколеннике №01">{/if}
        {if $oUserProfile->getDisplayName() == 'TheDreik'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik1.png" title="За участие в Наколеннике №01">{/if}
        {if $oUserProfile->getDisplayName() == 'Мурка'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik1.png" title="За участие в Наколеннике №01">{/if}
        {if $oUserProfile->getDisplayName() == 'Arlekin'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik2_first_place.png" title="За победу в Наколеннике №02">{/if}
        {if $oUserProfile->getDisplayName() == 'injir'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik2_first_place.png" title="За победу в Наколеннике №02">{/if}
        {if $oUserProfile->getDisplayName() == 'Arlekin'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik2.png" title="За участие в Наколеннике №02">{/if}
        {if $oUserProfile->getDisplayName() == 'injir'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik2.png" title="За участие в Наколеннике №02">{/if}
        {if $oUserProfile->getDisplayName() == 'AndreyMust19'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik2.png" title="За участие в Наколеннике №02">{/if}
        {if $oUserProfile->getDisplayName() == 'DarkDes'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik2.png" title="За участие в Наколеннике №02">{/if}
        {if $oUserProfile->getDisplayName() == 'Rat_on_psilocybin'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik2.png" title="За участие в Наколеннике №02">{/if}
        {if $oUserProfile->getDisplayName() == 'razzle_dazzle'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik2.png" title="За участие в Наколеннике №02">{/if}
        {if $oUserProfile->getDisplayName() == 'Praron'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik2.png" title="За участие в Наколеннике №02">{/if}
        {if $oUserProfile->getDisplayName() == 'Kot211'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik2.png" title="За участие в Наколеннике №02">{/if}
        {if $oUserProfile->getDisplayName() == 'DarkDes'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik2.png" title="За участие в Наколеннике №02">{/if}
        {if $oUserProfile->getDisplayName() == 'Xitilon'}<img src="/common/templates/skin/start-kit/assets/images/achi_nakolennik2.png" title="За участие в Наколеннике №02">{/if}
        {if $oUserProfile->getDisplayName() == 'DarkDes'}<img src="/common/templates/skin/start-kit/assets/images/achi_barraсuda.png" title='За участие в "BARRACUDA — Конкурс чит-кодов!"'>{/if}
        {if $oUserProfile->getDisplayName() == 'Xitilon'}<img src="/common/templates/skin/start-kit/assets/images/achi_barraсuda.png" title='За участие в "BARRACUDA — Конкурс чит-кодов!"'>{/if}
        {if $oUserProfile->getDisplayName() == 'Raseri'}<img src="/common/templates/skin/start-kit/assets/images/achi_barraсuda.png" title='За участие в "BARRACUDA — Конкурс чит-кодов!"'>{/if}
        {if $oUserProfile->getDisplayName() == 'Kozinaka'}<img src="/common/templates/skin/start-kit/assets/images/achi_barraсuda.png" title='За участие в "BARRACUDA — Конкурс чит-кодов!"'>{/if}
        {if $oUserProfile->getDisplayName() == 'Raseri'}<img src="/common/templates/skin/start-kit/assets/images/achi_barraсuda_first_place.png" title='За победу в "BARRACUDA — Конкурс чит-кодов!"'>{/if}
        {if $oUserProfile->getDisplayName() == 'stray_stoat'}<a href="http://kolenka.su/blog/ps/kuda-ne-uvodyat-mechty.html"><img src="/uploads/images/00/00/13/2015/09/10/0u5b8b2db3-54c46949-283dd710.png" title='Просто слова: лауреат'></a>{/if}
        {if $oUserProfile->getDisplayName() == 'yeo'}<a href="http://kolenka.su/blog/ps/kuda-uvodyat-mechty-dzhekson.html/"><img src="/uploads/images/00/00/13/2015/09/10/0u5b8b2db3-54c46949-283dd710.png" title='Просто слова: лауреат'></a>{/if}
        {if $oUserProfile->getDisplayName() == 'Cotton'}<a href="http://kolenka.su/blog/ps/i-may-not-be-the-right-one.html"><img src="/uploads/images/00/00/13/2015/09/10/0u5b8b2db3-54c46949-283dd710.png" title='Просто слова: лауреат'></a>{/if}
        {if $oUserProfile->getDisplayName() == 'Raseri'}<a href="http://kolenka.su/blog/ps/nastoyashhaya-mechta-dolzhna-byt-neosushhestvimoj-da.html"><img src="/uploads/images/00/00/13/2015/09/10/0u5b8b2db3-54c46949-283dd710.png" title='Просто слова: лауреат'></a>{/if}
        {if $oUserProfile->getDisplayName() == 'Kozinaka'}<a href="http://kolenka.su/blog/ps/kogda-ya-v-pervyj-raz-popal-pod-mashinu.html"><img src="/uploads/images/00/00/13/2015/09/10/0u5b8b2db3-54c46949-283dd710.png" title='Просто слова: лауреат'></a>{/if}
        {if $oUserProfile->getDisplayName() == 'mylittlekafka'}<a href="http://kolenka.su/blog/ps/dotdotdot.html"><img src="/uploads/images/00/00/13/2015/09/10/0u6b865ba9-54e240da-7508832e.png" title='Просто слова: участник'></a>{/if}
        {if $oUserProfile->getDisplayName() == 'DarkDes'}<a href="http://kolenka.su/blog/ps/karl-lavrekin.html"><img src="/uploads/images/00/00/13/2015/09/10/0u6b865ba9-54e240da-7508832e.png" title='Просто слова: участник'></a>{/if}
        {if $oUserProfile->getDisplayName() == 'MekaGem'}<a href="http://kolenka.su/blog/ps/muzhik-i-d6.html"><img src="/uploads/images/00/00/13/2015/09/10/0u6b865ba9-54e240da-7508832e.png" title='Просто слова: участник'></a>{/if}
        {if $oUserProfile->getDisplayName() == 'NoPlaceForYoungAzat'}<a href="http://kolenka.su/blog/ps/tosty-s-klubnichnym-dzhemom-ili-goluboj-rassvet-na-zolotom-saturne.html"><img src="/uploads/images/00/00/13/2015/09/10/0u6b865ba9-54e240da-7508832e.png" title='Просто слова: участник'></a>{/if}
        {if $oUserProfile->getDisplayName() == 'Hrenzerg'}<a href="http://kolenka.su/blog/ps/pyatyj-sentyabr.html"><img src="/uploads/images/00/00/13/2015/09/10/0u6b865ba9-54e240da-7508832e.png" title='Просто слова: участник'></a>{/if}
        {if $oUserProfile->getDisplayName() == 'Xitilon'}<a href="http://kolenka.su/blog/ps/fantazyor-dolzhen-sidet-v-mirke-verno.html"><img src="/uploads/images/00/00/13/2015/09/10/0u6b865ba9-54e240da-7508832e.png" title='Просто слова: участник'></a>{/if}
        {if $oUserProfile->getDisplayName() == 'Raseri'}<a href="http://kolenka.su/blog/ps/mechta-programmista.html"><img src="/uploads/images/00/00/13/2015/09/10/0u2144adc3-60623fea-247a4656.png" title='Просто слова: выдающийся графоман'></a>{/if}
</div><div class="col-lg-9">&nbsp;</div>
    <div class="col-lg-6">
        {$aUserFieldValues=$oUserProfile->getUserFieldValues(true)}
        {if $oUserProfile->getProfileSex()!='other' OR $oUserProfile->getProfileBirthday() OR $oGeoTarget OR $oUserProfile->getProfileAbout() OR count($aUserFieldValues)}
            <h4>{$aLang.profile_privat}</h4>
            <table class="table table-profile-info">
                {if $oUserProfile->getProfileSex()!='other'}
                    <tr>
                        <td class="text-muted cell-label">{$aLang.profile_sex}:</td>
                        <td>
                            {if $oUserProfile->getProfileSex()=='man'}
                                {$aLang.profile_sex_man}
                            {else}
                                {$aLang.profile_sex_woman}
                            {/if}
                        </td>
                    </tr>
                {/if}


                {if $oUserProfile->getProfileBirthday()}
                    <tr>
                        <td class="text-muted cell-label">{$aLang.profile_birthday}:</td>
                        <td>{date_format date=$oUserProfile->getProfileBirthday() format="j F Y"}</td>
                    </tr>
                {/if}


                {if $oGeoTarget}
                    <tr>
                        <td class="text-muted cell-label">{$aLang.profile_place}:</td>
                        <td itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
                            {if $oGeoTarget->getCountryId()}
                                <a href="{router page='people'}country/{$oGeoTarget->getCountryId()}/"
                                   itemprop="country-name">{$oUserProfile->getProfileCountry()|escape:'html'}</a>{if $oGeoTarget->getCityId()},{/if}
                            {/if}

                            {if $oGeoTarget->getCityId()}
                                <a href="{router page='people'}city/{$oGeoTarget->getCityId()}/"
                                   itemprop="locality">{$oUserProfile->getProfileCity()|escape:'html'}</a>
                            {/if}
                        </td>
                    </tr>
                {/if}

                {hook run='profile_whois_privat_item' oUserProfile=$oUserProfile}
            </table>
        {/if}

        {hook run='profile_whois_item_after_privat' oUserProfile=$oUserProfile}
    </div>

    <div class="col-lg-6">
        {$aUserFieldContactValues=$oUserProfile->getUserFieldValues(true, array('contact'))}
        {if $aUserFieldContactValues}
            <h4>{$aLang.profile_contacts}</h4>
            <table class="table table-profile-info">
                {foreach $aUserFieldContactValues as $oField}
                    <tr>
                        <td class="text-muted cell-label">
                            <span class="icon-contact icon-contact-{$oField->getName()}"></span>
                            {$oField->getTitle()|escape:'html'}:
                        </td>
                        <td>{$oField->getValue(true,true)}</td>
                    </tr>
                {/foreach}
            </table>
        {/if}

        {$aUserFieldContactValues=$oUserProfile->getUserFieldValues(true, array('social'))}
        {if $aUserFieldContactValues}
            <h4>{$aLang.profile_social}</h4>
            <table class="table table-profile-info">
                {foreach $aUserFieldContactValues as $oField}
                    <tr>
                        <td class="text-muted cell-label">
                            <span class="icon-contact icon-contact-{$oField->getName()}"></span>
                            {$oField->getTitle()|escape:'html'}:
                        </td>
                        <td>{$oField->getValue(true,true)}</td>
                    </tr>
                {/foreach}
            </table>
        {/if}
    </div>

</div>

{hook run='profile_whois_item' oUserProfile=$oUserProfile}

<h4>{$aLang.profile_activity}</h4>

<table class="table table-profile-info">

    {if Config::Get('general.reg.invite') AND $oUserInviteFrom}
        <tr>
            <td class="text-muted cell-label">{$aLang.profile_invite_from}:</td>
            <td>
                <a href="{$oUserInviteFrom->getProfileUrl()}">{$oUserInviteFrom->getDisplayName()}</a>&nbsp;
            </td>
        </tr>
    {/if}

    {if Config::Get('general.reg.invite') AND $aUsersInvite}
        <tr>
            <td class="text-muted cell-label">{$aLang.profile_invite_to}:</td>
            <td>
                {foreach $aUsersInvite as $oUserInvite}
                    <a href="{$oUserInvite->getProfileUrl()}">{$oUserInvite->getDisplayName()}</a>
                    &nbsp;
                {/foreach}
            </td>
        </tr>
    {/if}

    {if $aBlogsOwner}
        <tr>
            <td class="text-muted cell-label">{$aLang.profile_blogs_self}:</td>
            <td>
                {foreach $aBlogsOwner as $oBlog}
                    <a href="{$oBlog->getUrlFull()}">{$oBlog->getTitle()|escape:'html'}</a>{if !$oBlog@last}, {/if}
                {/foreach}
            </td>
        </tr>
    {/if}

    {if $aBlogAdministrators}
        <tr>
            <td class="text-muted cell-label">{$aLang.profile_blogs_administration}:</td>
            <td>
                {foreach $aBlogAdministrators as $oBlogUser}
                    {$oBlog=$oBlogUser->getBlog()}
                    <a href="{$oBlog->getUrlFull()}">{$oBlog->getTitle()|escape:'html'}</a>{if !$oBlogUser@last}, {/if}
                {/foreach}
            </td>
        </tr>
    {/if}

    {if $aBlogModerators}
        <tr>
            <td class="text-muted cell-label">{$aLang.profile_blogs_moderation}:</td>
            <td>
                {foreach $aBlogModerators as $oBlogUser}
                    {$oBlog=$oBlogUser->getBlog()}
                    <a href="{$oBlog->getUrlFull()}">{$oBlog->getTitle()|escape:'html'}</a>{if !$oBlogUser@last}, {/if}
                {/foreach}
            </td>
        </tr>
    {/if}

    {if $aBlogUsers}
        <tr>
            <td class="text-muted cell-label">{$aLang.profile_blogs_join}:</td>
            <td>
                {foreach $aBlogUsers as $oBlogUser}
                    {$oBlog=$oBlogUser->getBlog()}
                    <a href="{$oBlog->getUrlFull()}">{$oBlog->getTitle()|escape:'html'}</a>{if !$oBlogUser@last}, {/if}
                {/foreach}
            </td>
        </tr>
    {/if}

    {hook run='profile_whois_activity_item' oUserProfile=$oUserProfile}

    <tr>
        <td class="text-muted cell-label">{$aLang.profile_date_registration}:</td>
        <td>{date_format date=$oUserProfile->getDateRegister()}</td>
    </tr>

    {if $oSession}
        <tr>
            <td class="text-muted cell-label">{$aLang.profile_date_last}:</td>
            <td>{date_format date=$oSession->getDateLast()}</td>
        </tr>
    {/if}
</table>

{if $aUsersFriend}
    <h4><a href="{$oUserProfile->getProfileUrl()}friends/" class="user-friends">{$aLang.profile_friends}</a>
        <span class="text-muted">({$iCountFriendsUser})<span></h4>
    {include file='commons/common.user_list_avatar.tpl' aUsersList=$aUsersFriend}
{/if}

{hook run='profile_whois_item_end' oUserProfile=$oUserProfile}

</div>
{/block}
