{if count($aTopics)>0}
    {wgroup_add group='toolbar' name='toolbar_topic.tpl' iCountTopic=count($aTopics)}
    <form enctype="multipart/form-data" onsubmit="return false;" class="comment-reply" method="post" style="display: block;">
        <div>
            <textarea style="width:300px;height:60px;"></textarea>
        </div>
        <button id="send-micropost" class="btn btn-success js-button-submit" name="submit_comment" type="submit">добавить</button>
        <script>
        $(document).ready(function() {
            $("#send-micropost").click(function() {
                $.post(
                  "/content/topic/add/",
                  {
                    submit_topic_publish: "param1"
                  },
                  onSendMicropostSuccess
                );
                 
                function onSendMicropostSuccess(data)
                {
                  // Здесь мы получаем данные, отправленные сервером и выводим их на экран.
                  console.log(data);
                }
            });
        });
        </script>
    </form>
    {foreach $aTopics as $oTopic}
        {if E::Topic_IsAllowTopicType($oTopic->getType())}
            {$sTopicTemplateName=$oTopic->getTopicTypeTemplate('list')}
            {include file="topics/micro$sTopicTemplateName" bTopicList=true}
        {/if}
    {/foreach}

    {include file='commons/common.pagination.tpl' aPaging=$aPaging}
{else}
    <div class="alert alert-info">
        {$aLang.blog_no_topic}
    </div>
{/if}